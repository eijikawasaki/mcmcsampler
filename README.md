# mcmcsampler

## Description

This library provides Markov Chain Monte Carlo samplers.
The ```mcmcsampler``` folder contains tests and code.
See [index.md](src/mcmcsampler/index.md).

The documentation is available at: https://eijikawasaki.gitlab.io/mcmcsampler

## Installation

Install static library:
```shell
pip install git+https://gitlab.com/eijikawasaki/mcmcsampler.git
```

Remove library:
```shell
pip uninstall mcmcsampler
```

Install full repository:
```shell
git clone https://gitlab.com/eijikawasaki/mcmcsampler.git
cd mcmcsampler
pip install -U pip wheel setuptools
pip install -r requirements.txt
```

## Usage

Example:

```python
from mcmcsampler.physical_models.numpy_models import IsingModel
from mcmcsampler.lattices import SquareLattice
from mcmcsampler.monte_carlo_samplers.numpy_samplers import WolffSampler

ising_model = IsingModel()
initial_configuration = ising_model.initialize_random_configuration(configuration_shape=(3, 3))

engine = WolffSampler(configuration=initial_configuration,
                      physical_model=ising_model,
                      lattice=SquareLattice())
print(engine.metadata_dict)
engine.sample()
```

## Author

Eiji Kawasaki

## License

CeCILL-C (see [LICENSE](http://www.cecill.info/))
