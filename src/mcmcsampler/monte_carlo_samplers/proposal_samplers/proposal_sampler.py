from typing import Callable, List, Optional, Union

import numpy as np
import torch

from ...physical_models import AbstractPhysicalModel
from ..abstract_monte_carlo_sampler import AbstractMonteCarloSampler


class ProposalSampler(AbstractMonteCarloSampler):
    def __init__(
        self,
        configuration: Union[np.ndarray, torch.nn.Module],
        physical_model: AbstractPhysicalModel,
        proposal_function: Optional[Callable] = None,
        lattice=None,
        is_progress_bar_disable: bool = False,
    ):
        """
        Requires that the physical_model either has a compute_total_energy method or a compute_local_energy method.

        Args:
            configuration: configuration
            physical_model: physical_model
            proposal_function: proposal function
        """
        super().__init__(
            configuration=configuration,
            physical_model=physical_model,
            lattice=lattice,
            is_progress_bar_disable=is_progress_bar_disable,
        )
        if proposal_function is None:
            proposal_function = self._default_proposal_function
        self._proposal_function = proposal_function
        self.random_numbers: List[float] = []
        self.suggested_configurations: Union[np.ndarray, torch.nn.Module] = np.asarray([])
        self.current_local_energy = self.physical_model.compute_total_energy(self.configuration)

    @staticmethod
    def _default_proposal_function(
        configuration: Union[np.ndarray, torch.nn.Module],
        suggested_configuration: Union[np.ndarray, torch.nn.Module],
    ) -> float:
        return 1.0

    def sample(
        self,
        temperature: float = 1,
        record_step: int = 1,
        suggested_configurations: np.ndarray = np.asarray([]),
    ) -> np.ndarray:
        """
        Args:
            suggested_configurations: suggested configurations
            temperature: temperature
            record_step:

        Returns:
            samples: samples
        """
        self.random_numbers = self._create_list_random_numbers(len(suggested_configurations))
        self.suggested_configurations = suggested_configurations
        total_number_iterations = len(suggested_configurations)
        return self._create_samples(
            temperature,
            record_step,
            total_number_iterations,
        )

    def _sample_single_step(self, temperature: float, **kwargs):
        """
        Metropolis-Hastings:
            https://en.wikipedia.org/wiki/Metropolis%E2%80%93Hastings_algorithm

            - proposal_probability: $g(x|x')/g(x'|x)$
            - _compute_exponential: $p(x')/p(x)$
        """
        random_number = self.random_numbers.pop()
        suggested_configuration = self.get_suggested_configuration()
        proposal_probability = self.compute_proposal_probability(suggested_configuration)
        suggested_local_energy = self.compute_suggested_configuration_energy(suggested_configuration)
        energy_difference = suggested_local_energy - self.current_local_energy
        if (
            energy_difference < 0
            or random_number
            < ProposalSampler._compute_exponential(energy_difference, temperature) * proposal_probability
        ):
            self.configuration = suggested_configuration
            self.current_local_energy = suggested_local_energy
            self.acceptance += 1

    def compute_suggested_configuration_energy(self, suggested_configuration):
        return self.physical_model.compute_total_energy(suggested_configuration)

    def compute_proposal_probability(self, suggested_configuration):
        return self._proposal_function(self.configuration, suggested_configuration)

    def get_suggested_configuration(self):
        suggested_configuration, self.suggested_configurations = (
            self.suggested_configurations[-1],
            self.suggested_configurations[:-1],
        )
        return suggested_configuration

    @staticmethod
    def _create_list_random_numbers(total_number_iterations: int) -> list:
        # @jit(nopython=True) removed because it messes with random seeds
        random_list = np.random.rand(total_number_iterations)
        return list(random_list)
