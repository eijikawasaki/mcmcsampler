import numpy as np
import pytest

from mcmcsampler.monte_carlo_samplers.proposal_samplers import ProposalSampler
from mcmcsampler.physical_models import AbstractPhysicalModel
from mcmcsampler.physical_models.numpy_models import IsingModel


class PhysicalModelTestClass(AbstractPhysicalModel):
    @staticmethod
    def compute_total_energy(model, **kwargs):
        return 1.0

    @classmethod
    def compute_local_energy(cls, local_configuration: float, neighbors_configurations: tuple) -> float:
        return np.inf

    @staticmethod
    def initialize_random_configuration(configuration_shape: tuple) -> np.ndarray:
        return np.asarray([])


@pytest.mark.parametrize("minus_one_probability, plus_one_probability", [(0.1, 0.9), (0.8, 0.2), (0.6, 0.4)])
def test_proposal_function(minus_one_probability, plus_one_probability):
    configuration_shape = (1,)
    assert minus_one_probability + plus_one_probability == 1.0

    def proposal_function(current_state, suggested_state):
        current_state_probability = minus_one_probability if current_state == -1 else plus_one_probability
        suggested_state_probability = minus_one_probability if suggested_state == -1 else plus_one_probability
        return current_state_probability / suggested_state_probability

    metrop = ProposalSampler(
        configuration=IsingModel.initialize_random_configuration(configuration_shape),
        physical_model=PhysicalModelTestClass(),
        proposal_function=proposal_function,
    )

    suggested_configurations = np.random.choice(
        2, size=(10**4, *configuration_shape), p=[minus_one_probability, plus_one_probability]
    )
    suggested_configurations[suggested_configurations == 0] = int(-1)
    samples = metrop.sample(suggested_configurations=suggested_configurations, temperature=np.inf)
    assert abs(np.mean(samples)) < 0.2
