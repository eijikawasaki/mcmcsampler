import numpy as np

from mcmcsampler.lattices import SquareLattice
from mcmcsampler.monte_carlo_samplers.numpy_samplers import GlobalEnergySampler
from mcmcsampler.monte_carlo_samplers.proposal_samplers import ProposalSampler
from mcmcsampler.physical_models.numpy_models import Phi4Model


class TestProposalIntegration:
    @staticmethod
    def test_perfect_proposal():
        square_lattice = SquareLattice()
        phi4_model = Phi4Model(
            chemical_potential=-0.4, interaction_strength=0.01, space_dimensions=2, lattice=square_lattice
        )
        initial_configuration = phi4_model.initialize_random_configuration(configuration_shape=(3, 3))

        def _proposal_function(current_state, suggested_state):
            return np.exp(
                engine.physical_model.compute_total_energy(suggested_state)
                - engine.physical_model.compute_total_energy(current_state)
            )

        engine = GlobalEnergySampler(
            physical_model=phi4_model,
            lattice=square_lattice,
            configuration=initial_configuration,
        )
        mc_samples = engine.sample(total_number_iterations=100)

        engine = ProposalSampler(
            physical_model=phi4_model,
            configuration=initial_configuration,
            proposal_function=_proposal_function,
            lattice=square_lattice,
        )
        samples = engine.sample(suggested_configurations=mc_samples)

        mc_samples_energies = [engine.physical_model.compute_total_energy(config) for config in mc_samples]
        samples_energies = [engine.physical_model.compute_total_energy(config) for config in samples]

        # from matplotlib import pyplot as plt
        # plt.hist(mc_samples_energies, density=True)
        # plt.hist(samples_energies, density=True)
        # plt.show()

        np.testing.assert_almost_equal(np.mean(samples_energies), np.mean(mc_samples_energies))
        np.testing.assert_almost_equal(np.var(samples_energies), np.var(mc_samples_energies))
        assert engine.acceptance == 1

    @staticmethod
    def test_uniform_proposal():
        square_lattice = SquareLattice()
        phi4_model = Phi4Model(
            chemical_potential=-0.4, interaction_strength=0.01, space_dimensions=2, lattice=square_lattice
        )
        initial_configuration = phi4_model.initialize_random_configuration(configuration_shape=(3, 3))

        engine = GlobalEnergySampler(
            physical_model=phi4_model,
            lattice=square_lattice,
            configuration=initial_configuration,
        )

        for _ in range(3):
            mc_samples = engine.sample(total_number_iterations=10**3)
        mc_samples = engine.sample(total_number_iterations=10**4, record_step=20)
        mc_samples = np.asarray(mc_samples)

        engine = ProposalSampler(
            physical_model=phi4_model,
            lattice=square_lattice,
            configuration=initial_configuration,
        )
        samples = engine.sample(
            suggested_configurations=np.random.uniform(
                min(mc_samples.flatten()), max(mc_samples.flatten()), (10**5, 3, 3)
            )
        )
        # print(min(mc_samples.flatten()))
        # print(max(mc_samples.flatten()))

        mc_samples_energies = [engine.physical_model.compute_total_energy(config) for config in mc_samples]
        samples_energies = [engine.physical_model.compute_total_energy(config) for config in samples]

        # from matplotlib import pyplot as plt
        # plt.hist(mc_samples_energies, density=True, label=['mc_sampled_energies'], alpha=0.5)
        # plt.hist(samples_energies, density=True, label=['uniform_sampled_energies'], alpha=0.5)
        # plt.legend(loc='best')
        # plt.show()

        bias = abs(np.mean(samples_energies) - np.mean(mc_samples_energies)) / abs(
            np.mean(samples_energies) + np.mean(mc_samples_energies)
        )

        assert bias < 0.5
        # assert np.var(samples_energies) == np.var(mc_samples_energies)
        assert engine.acceptance < 0.5
