# Proposal Monte Carlo

```mermaid
classDiagram
    class ProposalSampler{
      +proposal_function
      +sample()
      +_sample_single_step()
    }
```

The `ProposalSampler` class allows sampling according to a physical model from a list of given samples. No local
update is required in this case, as the samples suggested to the Marlov Chain are provided by the user and not by the
MCMC algorithm. See the formal derivation of the Metropolis–Hastings
algorithm <https://en.wikipedia.org/wiki/Metropolis%E2%80%93Hastings_algorithm>

```python
import numpy as np
from mcmcsampler.physical_models.numpy_models import IsingModel
from mcmcsampler.monte_carlo_samplers.proposal_samplers import ProposalSampler
from mcmcsampler.lattices import SquareLattice

configuration_shape = (10, 10)

metrop = ProposalSampler(configuration=IsingModel.initialize_random_configuration(configuration_shape),
                         physical_model=IsingModel(lattice=SquareLattice()))

suggested_configurations = np.asarray([IsingModel.initialize_random_configuration(configuration_shape) for _ in range(10 ** 3)])

metrop.sample(suggested_configurations=suggested_configurations)
```

If the `suggested_configurations` do not share the same probability (uniformly distributed), a `proposal_function` must be
provided to the `ProposalSampler`. Here is an example in the case of a Normally distributed samples:

```python
import numpy as np
from scipy import stats


def _proposal_function(sample_1, sample_2) -> float:
    """
    return p(sample_1)/p(sample_2)
    """
    log_prob_1 = stats.norm(sample_1)
    log_prob_2 = stats.norm(sample_2)
    probability = np.exp(log_prob_1 - log_prob_2)
    return probability
```

::: mcmcsampler.monte_carlo_samplers.proposal_samplers.ProposalSampler
    handler: python
    rendering:
      show_root_heading: true
      show_source: false
      members_order: 'source'
      show_signature: false
