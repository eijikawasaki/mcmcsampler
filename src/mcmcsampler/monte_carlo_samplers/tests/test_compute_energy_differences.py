import copy

import numpy as np
import pytest

from mcmcsampler.lattices import SquareLattice
from mcmcsampler.monte_carlo_samplers.numpy_samplers import GlobalEnergySampler, LocalEnergySampler
from mcmcsampler.observables import NumpyObservables
from mcmcsampler.physical_models.numpy_models import Phi4Model


def compute_total_energy(configuration):
    return NumpyObservables.compute_total_energy(
        lattice=SquareLattice(),
        configuration=configuration,
        compute_local_energy_function=Phi4Model().compute_local_energy,
    )


@pytest.mark.parametrize(
    "configuration_shape, coordinates, random_update, initial_configuration",
    [
        ((10,), (0,), -0.13, Phi4Model.initialize_random_configuration((10,))),
        ((10,), (9,), +0.45, Phi4Model.initialize_random_configuration((10,))),
        ((10,), (5,), -1, Phi4Model.initialize_random_configuration((10,))),
        ((3, 3), (0, 0), -0.45, Phi4Model.initialize_random_configuration((3, 3))),
        ((3, 3), (1, 1), -2, Phi4Model.initialize_random_configuration((3, 3))),
    ],
)
def test_compute_energy_difference(configuration_shape, coordinates, random_update, initial_configuration):
    configuration_check = copy.deepcopy(initial_configuration)
    square_lattice = SquareLattice()
    metrop = GlobalEnergySampler(
        configuration=initial_configuration, physical_model=Phi4Model(lattice=square_lattice), lattice=square_lattice
    )
    global_energy_difference = metrop._compute_energy_difference(coordinates=coordinates, random_update=random_update)

    np.testing.assert_array_almost_equal(configuration_check, initial_configuration)
    metrop = LocalEnergySampler(
        configuration=initial_configuration, physical_model=Phi4Model(lattice=square_lattice), lattice=square_lattice
    )
    neighbors = metrop.lattice.compute_closest_neighbors(metrop.configuration_shape, coordinates)
    local_energy_difference = metrop._compute_energy_difference(
        coordinates=coordinates, neighbors=neighbors, random_update=random_update
    )
    assert abs(local_energy_difference - global_energy_difference) < 1e-5
