import numpy as np

from mcmcsampler.lattices import SquareLattice
from mcmcsampler.monte_carlo_samplers.numpy_samplers import LocalEnergySampler, MirrorSymmetricLocalEnergySampler
from mcmcsampler.physical_models.numpy_models import Phi4Model


def test_phi4_mirror_symmetric_local_metropolis():
    square_lattice = SquareLattice()
    configuration_shape = (10, 10)
    initial_configuration = Phi4Model.initialize_random_configuration(configuration_shape)
    phi4_model = Phi4Model(chemical_potential=-2, interaction_strength=1.0)
    metrop = MirrorSymmetricLocalEnergySampler(
        configuration=initial_configuration, physical_model=phi4_model, lattice=square_lattice
    )
    samples = metrop.sample(total_number_iterations=10**4)
    symmetric_absolute_value = abs(np.mean(samples))

    metrop = LocalEnergySampler(configuration=initial_configuration, physical_model=phi4_model, lattice=square_lattice)
    samples = metrop.sample(total_number_iterations=10**4)
    samples = metrop.sample(total_number_iterations=10**4)
    absolute_value = abs(np.mean(samples))

    assert absolute_value > symmetric_absolute_value
