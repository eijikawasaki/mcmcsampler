import copy
from typing import Union

import numpy as np

from mcmcsampler.physical_models.torch_models import NeuralNetworkMiniBatchModel, NeuralNetworkModel


class NeuralNetworkLangevinProposal:
    def __init__(self, physical_model: Union[NeuralNetworkModel, NeuralNetworkMiniBatchModel]):
        self.physical_model = physical_model

    def proposal_function(
        self,
        sample_1: dict,
        sample_2: dict,
        noises: list,
        eta: float,
    ) -> float:
        sample_1_negative_log_likelihood = self.compute_current_sample_negative_log_likelihood(
            sample_1, sample_2, noises, eta
        )
        sample_2_negative_log_likelihood = (
            NeuralNetworkLangevinProposal.compute_proposed_sample_negative_log_likelihood(noises)
        )
        proposal_probability = np.exp(sample_2_negative_log_likelihood - sample_1_negative_log_likelihood)
        return proposal_probability

    @staticmethod
    def compute_proposed_sample_negative_log_likelihood(noises: list):
        sample_2_likelihood = sum([noise.norm(2).pow(2.0).sum() for noise in noises])
        sample_2_likelihood /= 2.0
        return sample_2_likelihood

    def compute_current_sample_negative_log_likelihood(
        self, sample_1: dict, sample_2: dict, noises: list, eta: float
    ) -> float:
        sample_1_likelihood = 0.0
        for parameter1, parameter2, noise in zip(
            self.create_sample_parameters_generator(sample_1),
            self.create_sample_parameters_generator(sample_2),
            noises,
        ):
            single_dimension_likelihood = (
                eta * (parameter1.grad.detach().data + parameter2.grad.detach().data) - np.sqrt(2 * eta) * noise
            )
            sample_1_likelihood += single_dimension_likelihood.norm(2).item() ** 2
        sample_1_likelihood /= 4.0 * eta
        return sample_1_likelihood

    def create_sample_parameters_generator(self, sample):
        temp_physical_model = copy.deepcopy(self.physical_model)
        temp_physical_model.load_configration_into_module(sample)
        temp_physical_model.compute_module_gradient_based_on_configuration(sample)
        sample_parameters_generator = temp_physical_model.get_module_parameters_reference()
        return sample_parameters_generator
