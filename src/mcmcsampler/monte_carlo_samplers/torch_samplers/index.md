```mermaid
classDiagram
    AbstractNeuralNetworkSampler <|-- NeuralNetworkRandomWalkSampler
    AbstractNeuralNetworkSampler <|-- NeuralNetworkLangevinSampler
    NeuralNetworkLangevinSampler *-- NeuralNetworkLangevinProposal: composition
    class NeuralNetworkLangevinSampler{
      +proposal
      +current_local_energy
      +sample()
      +suggest_new_configuration()
      +_sample_single_step()
    }
    class NeuralNetworkLangevinProposal{
      +physical_model
      +proposal_function()
      +compute_proposed_sample_negative_log_likelihood()
      +compute_current_sample_negative_log_likelihood()
      +compute_module_gradient_based_on_configuration()
    }
    class NeuralNetworkRandomWalkSampler{
      +sample()
      +_sample_single_step()
    }
```

::: mcmcsampler.monte_carlo_samplers.torch_samplers.NeuralNetworkRandomWalkSampler
    handler: python
    rendering:
      show_root_heading: true
      show_source: false
      members_order: 'source'
      show_signature: false

::: mcmcsampler.monte_carlo_samplers.torch_samplers.NeuralNetworkLangevinSampler
    handler: python
    rendering:
      show_root_heading: true
      show_source: false
      members_order: 'source'
      show_signature: false

::: mcmcsampler.monte_carlo_samplers.torch_samplers.NeuralNetworkPenaltySampler
    handler: python
    rendering:
      show_root_heading: true
      show_source: false
      members_order: 'source'
      show_signature: false
