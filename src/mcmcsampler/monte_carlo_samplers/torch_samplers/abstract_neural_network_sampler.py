import json
from abc import ABC

import numpy as np

from mcmcsampler.monte_carlo_samplers.abstract_monte_carlo_sampler import AbstractMonteCarloSampler


class AbstractNeuralNetworkSampler(AbstractMonteCarloSampler, ABC):
    def __repr__(self) -> str:
        dico = json.loads(super().__repr__())
        dico.update({"configuration_architecture": list(self.configuration.keys())})
        return json.dumps(dico)

    @staticmethod
    def _compute_exponential(energy_difference: float, temperature: float) -> float:
        return np.exp(-energy_difference / temperature)
