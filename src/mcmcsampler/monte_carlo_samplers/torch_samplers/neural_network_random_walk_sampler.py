import json
from typing import List, Union

import numpy as np
from numba import jit

from mcmcsampler.lattices import NeuralNetworkLattice
from mcmcsampler.monte_carlo_samplers.torch_samplers.abstract_neural_network_sampler import AbstractNeuralNetworkSampler
from mcmcsampler.physical_models.torch_models import NeuralNetworkMiniBatchModel, NeuralNetworkModel


class NeuralNetworkRandomWalkSampler(AbstractNeuralNetworkSampler):
    """
    Random walk over a pytorch NN.
    """

    def __init__(
        self,
        configuration: dict,
        physical_model: Union[NeuralNetworkModel, NeuralNetworkMiniBatchModel],
        lattice: NeuralNetworkLattice,
        is_progress_bar_disable: bool = False,
    ):
        """
        Args:
            configuration: torch.nn.module.state_dict()
            physical_model: NeuralNetworkModel
            lattice: NeuralNetworkLattice
        """
        self.lattice = lattice
        super().__init__(
            configuration=configuration,
            physical_model=physical_model,
            lattice=lattice,
            is_progress_bar_disable=is_progress_bar_disable,
        )

        self.random_coordinates: List[tuple] = []
        self.random_numbers: List[float] = []
        self.random_updates: List[float] = []
        self.current_energy = np.inf

    def __repr__(self) -> str:
        dico = json.loads(super().__repr__())
        dico.update(json.loads(repr(self.lattice)))
        return json.dumps(dico)

    def sample(
        self,
        temperature: float = 1.0,
        record_step: int = 1,
        total_number_iterations: int = 10,
        random_walk_step_size: float = 1.0,
    ) -> np.ndarray:
        """
        Args:
            total_number_iterations: total number of iterations
            temperature: temperature
            record_step: records the configuration every ```record_step```
            random_walk_step_size: random walk step size

        Returns:
            samples: samples
        """
        self.random_numbers = self._create_list_random_numbers(total_number_iterations)
        self.random_updates = self._create_list_random_updates(total_number_iterations, random_walk_step_size)
        self.random_coordinates = self._create_list_random_coordinates(total_number_iterations)
        return self._create_samples(
            temperature,
            record_step,
            total_number_iterations,
        )

    def _sample_single_step(self, temperature: float, **kwargs):
        random_update = self.random_updates.pop()
        random_coordinate = self.random_coordinates.pop()
        random_number = self.random_numbers.pop()
        energy_difference, new_energy = self.compute_new_energy_difference(random_coordinate, random_update)
        if energy_difference < 0 or random_number < self._compute_exponential(energy_difference, temperature):
            NeuralNetworkRandomWalkSampler._update_single_weight(self.configuration, random_coordinate, random_update)
            self.current_energy = new_energy
            self.acceptance += 1.0

    def compute_new_energy_difference(self, random_coordinate, random_update):
        NeuralNetworkRandomWalkSampler._update_single_weight(self.configuration, random_coordinate, random_update)
        new_energy = self.physical_model.compute_total_energy(self.configuration)
        NeuralNetworkRandomWalkSampler._update_single_weight(self.configuration, random_coordinate, -random_update)
        # restores self.configuration to the previous value
        energy_difference = new_energy - self.current_energy
        return energy_difference, new_energy

    @staticmethod
    def _update_single_weight(configuration, random_coordinate: tuple, random_update: float):
        random_layer, random_index = random_coordinate
        if len(random_index) == 2:
            (random_index_1, random_index_2) = random_index
            configuration.get(random_layer)[random_index_1][random_index_2] += random_update
        else:
            configuration.get(random_layer)[random_index] += random_update

    def _create_list_random_coordinates(self, total_number_iterations):
        return [self.lattice.compute_random_coordinate(self.configuration) for _ in range(total_number_iterations)]

    @staticmethod
    @jit(nopython=True)
    def _create_list_random_updates(total_number_iterations: int, random_walk_step_size: float) -> list:
        random_list = random_walk_step_size * np.random.randn(total_number_iterations)
        return list(random_list)
