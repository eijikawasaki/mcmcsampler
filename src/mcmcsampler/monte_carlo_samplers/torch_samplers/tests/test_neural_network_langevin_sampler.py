import copy

import numpy as np
import pytest
import torch
from torch.utils.data import DataLoader, TensorDataset

from mcmcsampler.monte_carlo_samplers.torch_samplers import NeuralNetworkLangevinSampler
from mcmcsampler.observables import TorchObservables
from mcmcsampler.physical_models.torch_models import NeuralNetworkModel
from mcmcsampler.tests.mlp import MLPTrainer


class TestNeuralNetworkLangevin:
    @staticmethod
    def test_create_noise(mlp):
        configuration = mlp.state_dict()
        noises = NeuralNetworkLangevinSampler._create_noise(configuration)
        for layer_name, noise_values in zip(configuration, noises):
            assert configuration.get(layer_name).shape == noise_values.shape

    @staticmethod
    def test_create_optimizer(mlp):
        optimizer = NeuralNetworkLangevinSampler.create_optimizer(mlp)
        assert isinstance(optimizer, torch.optim.SGD)

    @staticmethod
    def test_suggest_new_configuration(mlp, loader):
        nn_model = NeuralNetworkModel(loader, module=mlp)
        metrop = NeuralNetworkLangevinSampler(configuration=mlp.state_dict(), physical_model=nn_model)
        noises = metrop._create_noise(mlp.state_dict())
        new_configuration = metrop.suggest_new_configuration(noises=noises, random_walk_step_size=1.0)
        assert isinstance(new_configuration, dict)
        assert str(new_configuration) != str(metrop.configuration)

    @staticmethod
    def test_suggest_new_configuration_noise_trick(init, update_module_by_single_gradient_step):
        (
            metrop,
            nn_model,
            initial_configuration,
            noises,
            proposed_configuration,
            random_walk_step_size,
        ) = init
        start_parameters = TorchObservables.turn_parameters_configuration_into_numpy_vector(
            nn_model.module, initial_configuration
        )
        proposed_sample_parameters = TorchObservables.turn_parameters_configuration_into_numpy_vector(
            nn_model.module, proposed_configuration
        )
        nn_model.load_configration_into_module(initial_configuration)
        update_module_by_single_gradient_step(initial_configuration, metrop, nn_model, random_walk_step_size)
        new_configuration_parameters = TorchObservables.turn_parameters_configuration_into_numpy_vector(
            nn_model.module, nn_model.get_module_configuration_copy()
        )
        with pytest.raises(AssertionError):
            np.testing.assert_array_almost_equal(new_configuration_parameters, start_parameters)
            np.testing.assert_array_almost_equal(proposed_sample_parameters, start_parameters)
        np.testing.assert_array_almost_equal(
            proposed_sample_parameters,
            new_configuration_parameters + np.sqrt(2.0 * random_walk_step_size) * torch.Tensor(noises).numpy(),
        )

    @staticmethod
    def test_samples_are_not_memory_linked(mlp, loader):
        initial_configuration = mlp.state_dict()
        nn_model = NeuralNetworkModel(loader, mlp)
        metrop = NeuralNetworkLangevinSampler(configuration=initial_configuration, physical_model=nn_model)
        samples = metrop.sample(total_number_iterations=10**2, random_walk_step_size=1.0)
        p1 = TorchObservables.turn_parameters_configuration_into_numpy_vector(mlp, initial_configuration)
        p2 = TorchObservables.turn_parameters_configuration_into_numpy_vector(mlp, samples[-1])
        with pytest.raises(AssertionError):
            assert np.testing.assert_allclose(p1, p2)
        p1 = TorchObservables.turn_parameters_configuration_into_numpy_vector(mlp, samples[0])
        with pytest.raises(AssertionError):
            assert np.testing.assert_allclose(p1, p2)

    @staticmethod
    def test_langevin_parameter_update_equals_gradient_step_plus_noise(init):
        (
            metrop,
            nn_model,
            initial_configuration,
            noises,
            proposed_configuration,
            random_walk_step_size,
        ) = init
        nn_model.load_configration_into_module(initial_configuration)
        nn_model.module.zero_grad()
        nn_model.module.train()
        loss = nn_model.compute_negative_log_likelihood()
        loss.backward()
        gradient_configuration_iterator = nn_model.module.parameters()
        nn_model.load_configration_into_module(initial_configuration)
        initial_configuration_iterator = copy.deepcopy(nn_model).module.parameters()
        nn_model.load_configration_into_module(proposed_configuration)
        proposed_configuration_iterator = nn_model.module.parameters()
        for p1, p1bis, p2, noise in zip(
            initial_configuration_iterator,
            gradient_configuration_iterator,
            proposed_configuration_iterator,
            noises,
        ):
            assert p2 - (p1 - p1bis.grad.detach().data + np.sqrt(2) * noise) < 1e-2

    @staticmethod
    def test_proposal_density_ratio_and_target_density_ratio_are_close(init):
        random_walk_step_size = 1e-4
        module = MLPTrainer(input_dim=1, hidden_dim=(1,), output_dim=1)
        train_input_data = torch.zeros((1, 1))
        train_output_data = torch.zeros((1, 1))
        dataset = TensorDataset(train_input_data, train_output_data)
        loader = DataLoader(dataset)
        nn_model = NeuralNetworkModel(loader, module, weight_decay=0.0)
        initial_configuration = nn_model.get_module_configuration_copy()
        metrop = NeuralNetworkLangevinSampler(configuration=initial_configuration, physical_model=nn_model)
        noises = metrop._create_noise(initial_configuration)
        proposed_configuration = metrop.suggest_new_configuration(
            noises=noises,
            random_walk_step_size=random_walk_step_size,
        )
        proposal_probability = metrop.proposal.proposal_function(
            initial_configuration, proposed_configuration, noises, random_walk_step_size
        )
        nn_model.load_configration_into_module(initial_configuration)
        current_loss = nn_model.compute_negative_log_likelihood()
        current_loss = current_loss.detach().numpy()
        nn_model.load_configration_into_module(proposed_configuration)
        proposed_loss = nn_model.compute_negative_log_likelihood()
        proposed_loss = proposed_loss.detach().numpy()
        assert abs(proposal_probability - np.exp(-proposed_loss + current_loss)) < 1e-1
