import random

import numpy as np
import pytest
import torch
from torch.utils.data import DataLoader, TensorDataset

from mcmcsampler.monte_carlo_samplers.torch_samplers import NeuralNetworkLangevinSampler
from mcmcsampler.physical_models.torch_models import NeuralNetworkModel
from mcmcsampler.tests.mlp import MLPTrainer


@pytest.fixture(scope="module")
def init():
    seed = 0
    np.random.seed(seed)
    torch.manual_seed(seed)
    random.seed(seed)

    random_walk_step_size = 1.0
    module = MLPTrainer(input_dim=1, hidden_dim=(1,), output_dim=1)
    train_input_data = torch.zeros((1, 1))
    train_output_data = torch.zeros((1, 1))
    dataset = TensorDataset(train_input_data, train_output_data)
    loader = DataLoader(dataset)
    nn_model = NeuralNetworkModel(loader, module, weight_decay=0.0)
    initial_configuration = nn_model.get_module_configuration_copy()
    metrop = NeuralNetworkLangevinSampler(configuration=initial_configuration, physical_model=nn_model)
    noises = metrop._create_noise(initial_configuration)
    proposed_configuration = metrop.suggest_new_configuration(
        noises=noises,
        random_walk_step_size=random_walk_step_size,
    )
    return (
        metrop,
        nn_model,
        initial_configuration,
        noises,
        proposed_configuration,
        random_walk_step_size,
    )


@pytest.fixture(scope="module")
def mlp():
    return MLPTrainer(input_dim=1, hidden_dim=tuple(10 for _ in range(10)), output_dim=1)


@pytest.fixture(scope="module")
def loader():
    train_input_data = torch.randn((10, 1))
    train_output_data = torch.randn((10, 1))
    dataset = TensorDataset(train_input_data, train_output_data)
    return DataLoader(dataset, batch_size=len(train_input_data))


@pytest.fixture(scope="module")
def update_module_by_single_gradient_step():
    def wrapped_function(initial_configuration, metrop, nn_model, random_walk_step_size):
        nn_model.load_configration_into_module(initial_configuration)
        optimizer = metrop.create_optimizer(nn_model.module)
        optimizer.zero_grad()
        loss = random_walk_step_size * nn_model.compute_negative_log_likelihood()
        loss.backward()
        optimizer.step()

    return wrapped_function
