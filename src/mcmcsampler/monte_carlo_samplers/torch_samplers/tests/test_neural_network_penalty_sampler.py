import numpy as np
import pytest
import torch
from torch.utils.data import DataLoader, TensorDataset

from mcmcsampler.lattices import NeuralNetworkLattice
from mcmcsampler.monte_carlo_samplers.torch_samplers import NeuralNetworkPenaltySampler, NeuralNetworkRandomWalkSampler
from mcmcsampler.observables import TorchObservables
from mcmcsampler.physical_models.torch_models import NeuralNetworkMiniBatchModel, NeuralNetworkModel


class TestNeuralNetworkPenaltySampler:
    @staticmethod
    def test_infinite_energy_stops_markov_chain(mlp):
        initial_configuration = mlp.state_dict()
        train_input_data = torch.randn((10, 1))
        train_output_data = torch.randn((10, 1))
        train_output_data[0][0] = 1e20
        dataset = TensorDataset(train_input_data, train_output_data)
        loader = DataLoader(dataset)
        nn_model = NeuralNetworkMiniBatchModel(loader, mlp)
        nn_lattice = NeuralNetworkLattice()
        engine = NeuralNetworkPenaltySampler(
            physical_model=nn_model, lattice=nn_lattice, configuration=initial_configuration
        )
        _ = engine.sample(total_number_iterations=10**2)
        assert engine.acceptance == 0.0

    @staticmethod
    def test_acceptance_is_not_zero(mlp, loader):
        initial_configuration = mlp.state_dict()
        nn_model = NeuralNetworkMiniBatchModel(loader, mlp)
        nn_lattice = NeuralNetworkLattice()
        engine = NeuralNetworkPenaltySampler(
            physical_model=nn_model,
            lattice=nn_lattice,
            configuration=initial_configuration,
            is_energy_difference_averaged=False,
        )
        _ = engine.sample(total_number_iterations=10**2)
        assert engine.acceptance > 0.0

    @staticmethod
    @pytest.mark.parametrize(
        ("weight_decay", "batch_size"),
        [(0.0, 1), (1.0, 1), (1.0, 2)],
    )
    def test_penalty_likelihood_same_as_bnn(mlp, weight_decay, batch_size):
        initial_configuration = mlp.state_dict()
        train_input_data = torch.randn((10, 1))
        train_output_data = torch.randn((10, 1))
        dataset = TensorDataset(train_input_data, train_output_data)
        nn_lattice = NeuralNetworkLattice()

        penalty_loader = DataLoader(dataset, batch_size=batch_size)
        penalty_nn_model = NeuralNetworkMiniBatchModel(
            penalty_loader,
            module=mlp,
            weight_decay=weight_decay,
        )
        penalty_engine = NeuralNetworkPenaltySampler(
            physical_model=penalty_nn_model,
            lattice=nn_lattice,
            configuration=initial_configuration,
        )
        total_energies = penalty_engine.physical_model.compute_total_energies(penalty_engine.configuration)
        penalty_energy = np.mean(total_energies)

        loader = DataLoader(dataset, batch_size=len(train_input_data))
        nn_model = NeuralNetworkModel(loader, module=mlp, weight_decay=weight_decay)
        engine = NeuralNetworkRandomWalkSampler(
            physical_model=nn_model, lattice=nn_lattice, configuration=initial_configuration
        )
        energy = engine.physical_model.compute_total_energy(engine.configuration)
        assert TorchObservables.check_two_configurations_dict_are_equal(
            penalty_engine.configuration, engine.configuration
        )
        assert abs(penalty_energy - energy) < 1e-4
        assert np.var(total_energies) > 0.0
