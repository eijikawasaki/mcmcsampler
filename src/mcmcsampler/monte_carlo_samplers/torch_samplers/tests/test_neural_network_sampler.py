import copy

import numpy as np
import pytest
import torch
from torch.utils.data import DataLoader, TensorDataset

from mcmcsampler.lattices import NeuralNetworkLattice
from mcmcsampler.monte_carlo_samplers.torch_samplers import NeuralNetworkLangevinSampler, NeuralNetworkRandomWalkSampler
from mcmcsampler.observables import TorchObservables
from mcmcsampler.physical_models.torch_models import NeuralNetworkModel
from mcmcsampler.tests.mlp import MLPTrainer


class TestNeuralNetworkSampler:
    @staticmethod
    @pytest.fixture
    def nn_model():
        module = MLPTrainer(input_dim=1, hidden_dim=tuple(10 for _ in range(10)), output_dim=1)
        train_input_data = torch.zeros((10, 1))
        train_output_data = torch.zeros((10, 1))
        dataset = TensorDataset(train_input_data, train_output_data)
        loader = DataLoader(dataset)
        nn_model = NeuralNetworkModel(loader, module=module)
        return nn_model

    @staticmethod
    def test_neural_network_metropolis(nn_model):
        initial_configuration = nn_model.get_module_configuration_copy()
        nn_lattice = NeuralNetworkLattice()
        engine = NeuralNetworkRandomWalkSampler(
            physical_model=nn_model, lattice=nn_lattice, configuration=initial_configuration
        )
        samples = engine.sample(total_number_iterations=10**3)
        assert np.shape(samples) == (10**3,)

    @staticmethod
    def test_neural_network_langevin(nn_model):
        initial_configuration = nn_model.get_module_configuration_copy()
        engine = NeuralNetworkLangevinSampler(configuration=initial_configuration, physical_model=nn_model)
        samples = engine.sample(total_number_iterations=10**3, random_walk_step_size=1.0)
        assert np.shape(samples) == (10**3,)

    @staticmethod
    def test_optimizer_energy_is_the_same_as_physical_model(nn_model):
        initial_configuration = nn_model.get_module_configuration_copy()
        total_energy = nn_model.compute_total_energy(initial_configuration)
        engine = NeuralNetworkLangevinSampler(configuration=initial_configuration, physical_model=nn_model)
        loss = engine.physical_model.compute_total_energy(engine.configuration)
        assert loss == total_energy

    @staticmethod
    def test_optimizer_energy_is_the_same_as_physical_model_2(nn_model):
        initial_configuration = nn_model.get_module_configuration_copy()
        configuration = copy.deepcopy(initial_configuration)
        # Langevin
        engine = NeuralNetworkLangevinSampler(physical_model=nn_model, configuration=initial_configuration)
        optimizer = NeuralNetworkLangevinSampler.create_optimizer(engine.physical_model.module)
        optimizer.zero_grad()
        loss = engine.physical_model.compute_negative_log_likelihood()
        loss.backward()
        optimizer.step()
        p1 = TorchObservables.turn_parameters_configuration_into_numpy_vector(
            engine.physical_model.module, engine.physical_model.get_module_configuration_copy()
        )
        # Random Walk
        nn_model.load_configration_into_module(configuration)
        optimizer = torch.optim.SGD(
            params=nn_model.get_module_parameters_reference(),
            lr=1.0,
            momentum=0.0,
            dampening=0.0,
            weight_decay=0.0,
            nesterov=False,
        )
        optimizer.zero_grad()
        total_energy = nn_model.compute_negative_log_likelihood()
        total_energy.backward()
        optimizer.step()

        p2 = TorchObservables.turn_parameters_configuration_into_numpy_vector(
            nn_model.module, nn_model.get_module_configuration_copy()
        )
        np.testing.assert_allclose(p1, p2)
