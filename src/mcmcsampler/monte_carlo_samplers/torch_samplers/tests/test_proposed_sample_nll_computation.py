import copy

import numpy as np
import torch

from mcmcsampler.monte_carlo_samplers.torch_samplers import NeuralNetworkLangevinSampler
from mcmcsampler.observables import TorchObservables
from mcmcsampler.physical_models.torch_models import NeuralNetworkModel


class TestToto:
    @staticmethod
    def test_compute_proposed_sample_log_likelihood_is_equal_to_gaussian_nll(
        init, update_module_by_single_gradient_step
    ):
        (
            metrop,
            nn_model,
            initial_configuration,
            noises,
            proposed_configuration,
            random_walk_step_size,
        ) = init

        proposal_log_likelihood = metrop.proposal.compute_proposed_sample_negative_log_likelihood(noises)
        assert abs(proposal_log_likelihood - 2.46504942 / 2.0) < 1e-4
        # 2.46504942 computed by hand as the sum of the squared components in noises
        proposal_log_likelihood += (len(noises) / 2) * (np.log(2 * np.pi) + np.log(2))
        # Gaussian normalization with sigma=\sqrt(2*eta)

        compute_loss = torch.nn.GaussianNLLLoss(full=True, reduction="sum")
        nn_model.load_configration_into_module(proposed_configuration)
        _input = torch.nn.utils.parameters_to_vector(nn_model.module.parameters())
        # _input: gradient step + noise
        nn_model.load_configration_into_module(initial_configuration)
        update_module_by_single_gradient_step(initial_configuration, metrop, nn_model, random_walk_step_size)
        target = torch.nn.utils.parameters_to_vector(nn_model.module.parameters())
        # target: gradient step
        var = 2.0 * random_walk_step_size * torch.ones(_input.shape)
        nll = compute_loss(input=_input, target=target, var=var)
        # nll is equal to the Gaussian likelihood of _input with mean target and variance var
        nll = float(nll.detach().numpy())
        assert abs(nll - proposal_log_likelihood) < 1e-7

    @staticmethod
    def test_compute_proposed_sample_log_likelihood_is_equal_to_gaussian_nll_high_dimension(
        mlp, loader, update_module_by_single_gradient_step
    ):
        random_walk_step_size = 1.0
        initial_configuration = copy.deepcopy(mlp.state_dict())
        nn_model = NeuralNetworkModel(loader, mlp, weight_decay=0.0)
        metrop = NeuralNetworkLangevinSampler(configuration=initial_configuration, physical_model=nn_model)
        noises = metrop._create_noise(initial_configuration)
        proposed_sample = metrop.suggest_new_configuration(
            noises=noises,
            random_walk_step_size=random_walk_step_size,
        )
        proposal_log_likelihood = metrop.proposal.compute_proposed_sample_negative_log_likelihood(noises)
        k = TestToto.compute_gaussian_dimension(noises)
        proposal_log_likelihood += (k / 2) * (np.log(2 * np.pi) + np.log(2))
        compute_loss = torch.nn.GaussianNLLLoss(full=True, reduction="sum")
        nn_model.load_configration_into_module(proposed_sample)
        _input = torch.nn.utils.parameters_to_vector(nn_model.module.parameters())
        update_module_by_single_gradient_step(initial_configuration, metrop, nn_model, random_walk_step_size)
        target = torch.nn.utils.parameters_to_vector(nn_model.module.parameters())
        var = 2.0 * random_walk_step_size * torch.ones(_input.shape)
        nll = compute_loss(input=_input, target=target, var=var)
        nll = float(nll.detach().numpy())
        assert abs(nll - proposal_log_likelihood) < 1e-3

    @staticmethod
    def compute_gaussian_dimension(noises):
        k = 0
        for _ in noises:
            for __ in _:
                try:
                    for ___ in __:
                        k += 1
                except TypeError:
                    k += 1
        return k

    @staticmethod
    def test_compute_compute_current_sample_log_likelihood_is_equal_to_gaussian_nll(
        init, update_module_by_single_gradient_step
    ):
        (
            metrop,
            nn_model,
            initial_configuration,
            noises,
            proposed_configuration,
            random_walk_step_size,
        ) = copy.deepcopy(init)
        proposal_negative_log_likelihood = metrop.proposal.compute_current_sample_negative_log_likelihood(
            sample_1=initial_configuration,
            sample_2=proposed_configuration,
            noises=noises,
            eta=random_walk_step_size,
        )
        proposal_negative_log_likelihood += (len(noises) / 2) * (np.log(2 * np.pi) + np.log(2))
        (
            metrop,
            nn_model,
            initial_configuration,
            noises,
            proposed_configuration,
            random_walk_step_size,
        ) = init
        compute_loss = torch.nn.GaussianNLLLoss(full=True, reduction="sum")
        nn_model.load_configration_into_module(initial_configuration)
        _input = TorchObservables.turn_parameters_configuration_into_numpy_vector(
            nn_model.module, nn_model.get_module_configuration_copy(), is_output_numpy=False
        )
        update_module_by_single_gradient_step(proposed_configuration, metrop, nn_model, random_walk_step_size)
        target = torch.nn.utils.parameters_to_vector(nn_model.module.parameters())
        var = 2.0 * random_walk_step_size * torch.ones(_input.shape)
        nll = compute_loss(input=_input, target=target, var=var)
        nll = float(nll.detach().numpy())
        assert abs(nll - proposal_negative_log_likelihood) < 1e-6
