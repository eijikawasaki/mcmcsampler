import numpy as np
import torch

from mcmcsampler.observables import TorchObservables


class TestGaussianProposalFunctionEqualsGaussianNLL:
    @staticmethod
    def test_gaussian_proposal_function_equal_to_gaussian_nll(init, update_module_by_single_gradient_step):
        current_sample_negative_log_likelihood = (
            TestGaussianProposalFunctionEqualsGaussianNLL.compute_current_sample_likelihood(
                init, update_module_by_single_gradient_step
            )
        )
        proposed_sample_negative_log_likelihood = (
            TestGaussianProposalFunctionEqualsGaussianNLL.compute_proposed_sample_likelihood(
                init, update_module_by_single_gradient_step
            )
        )
        proposal_probability = TestGaussianProposalFunctionEqualsGaussianNLL.compute_proposal_probability(init)
        assert (
            abs(
                proposal_probability
                - np.exp(proposed_sample_negative_log_likelihood - current_sample_negative_log_likelihood)
            )
            < 1e-7
        )

    @staticmethod
    def compute_proposal_probability(init):
        (
            metrop,
            nn_model,
            initial_configuration,
            noises,
            proposed_configuration,
            random_walk_step_size,
        ) = init
        proposal_probability = metrop.proposal.proposal_function(
            initial_configuration, proposed_configuration, noises, random_walk_step_size
        )
        return proposal_probability

    @staticmethod
    def compute_proposed_sample_likelihood(init, update_module_by_single_gradient_step):
        (
            metrop,
            nn_model,
            initial_configuration,
            noises,
            proposed_configuration,
            random_walk_step_size,
        ) = init
        compute_loss = torch.nn.GaussianNLLLoss(full=True, reduction="sum")
        _input = TorchObservables.turn_parameters_configuration_into_numpy_vector(
            nn_model.module, proposed_configuration, is_output_numpy=False
        )
        update_module_by_single_gradient_step(initial_configuration, metrop, nn_model, random_walk_step_size)
        target = TorchObservables.turn_parameters_configuration_into_numpy_vector(
            nn_model.module, nn_model.get_module_configuration_copy(), is_output_numpy=False
        )
        var = 2.0 * random_walk_step_size * torch.ones(_input.shape)
        proposed_sample_negative_log_likelihood = compute_loss(input=_input, target=target, var=var)
        proposed_sample_negative_log_likelihood = float(proposed_sample_negative_log_likelihood.detach().numpy())
        return proposed_sample_negative_log_likelihood

    @staticmethod
    def compute_current_sample_likelihood(init, update_module_by_single_gradient_step):
        (
            metrop,
            nn_model,
            initial_configuration,
            noises,
            proposed_configuration,
            random_walk_step_size,
        ) = init
        compute_loss = torch.nn.GaussianNLLLoss(full=True, reduction="sum")
        _input = TorchObservables.turn_parameters_configuration_into_numpy_vector(
            nn_model.module, nn_model.get_module_configuration_copy(), is_output_numpy=False
        )
        update_module_by_single_gradient_step(proposed_configuration, metrop, nn_model, random_walk_step_size)
        target = TorchObservables.turn_parameters_configuration_into_numpy_vector(
            nn_model.module, nn_model.get_module_configuration_copy(), is_output_numpy=False
        )
        var = 2.0 * random_walk_step_size * torch.ones(_input.shape)
        current_sample_negative_log_likelihood = compute_loss(input=_input, target=target, var=var)
        current_sample_negative_log_likelihood = float(current_sample_negative_log_likelihood.detach().numpy())
        return current_sample_negative_log_likelihood
