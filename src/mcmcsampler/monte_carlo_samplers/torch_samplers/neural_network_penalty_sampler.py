import json
from typing import List, Tuple

import numpy as np

from mcmcsampler.lattices import NeuralNetworkLattice
from mcmcsampler.physical_models.torch_models import NeuralNetworkMiniBatchModel

from .neural_network_random_walk_sampler import NeuralNetworkRandomWalkSampler


class NeuralNetworkPenaltySampler(NeuralNetworkRandomWalkSampler):
    def __init__(
        self,
        configuration: dict,
        physical_model: NeuralNetworkMiniBatchModel,
        lattice: NeuralNetworkLattice,
        is_energy_difference_averaged=True,
        is_progress_bar_disable: bool = False,
    ):
        """
        Args:
            configuration: configuration
            physical_model: NeuralNetworkModel
            lattice: NeuralNetworkLattice
        """
        super().__init__(configuration, physical_model, lattice, is_progress_bar_disable=is_progress_bar_disable)
        self.is_energy_difference_averaged = is_energy_difference_averaged
        self.random_coordinates: List[tuple] = []
        self.random_numbers: List[float] = []
        self.random_updates: List[float] = []

    def __repr__(self) -> str:
        dico = json.loads(super().__repr__())
        dico.update(json.loads(repr(self.lattice)))
        dico.update({"is_energy_difference_averaged": self.is_energy_difference_averaged})
        return json.dumps(dico)

    def sample(
        self,
        temperature: float = 1.0,
        record_step: int = 1,
        total_number_iterations: int = 10,
        random_walk_step_size: float = 1,
        penalty_regularization: float = 1.0,
    ) -> np.ndarray:
        """
        Args:
            total_number_iterations: total number of iterations
            temperature: temperature
            record_step: records the configuration every ```record_step```
            random_walk_step_size: random walk step size
            penalty_regularization: penalty_regularization
        Returns:
            samples: samples
        """
        self.random_numbers = self._create_list_random_numbers(total_number_iterations)
        self.random_updates = self._create_list_random_updates(total_number_iterations, random_walk_step_size)
        self.random_coordinates = self._create_list_random_coordinates(total_number_iterations)
        return self._create_samples(
            temperature,
            record_step,
            total_number_iterations,
            penalty_regularization=penalty_regularization,
        )

    def _sample_single_step(self, temperature: float, penalty_regularization: float = 1.0, **kwargs) -> None:
        # TODO: test temperature scaling
        random_number = self.random_numbers.pop()
        random_coordinate = self.random_coordinates.pop()
        random_update = self.random_updates.pop()
        (
            estimated_energy_difference,
            var_energy_difference,
        ) = self._compute_energy_difference_and_variance(random_coordinate, random_update)
        if estimated_energy_difference is not np.inf:
            log_acceptance = estimated_energy_difference + penalty_regularization * var_energy_difference / (
                2.0 * temperature
            )
            if log_acceptance < 0 or random_number < self._compute_exponential(log_acceptance, temperature):
                NeuralNetworkRandomWalkSampler._update_single_weight(
                    self.configuration, random_coordinate, random_update
                )
                self.acceptance += 1.0

    def _compute_energy_difference_and_variance(self, random_coordinate: tuple, random_update) -> Tuple[float, float]:
        current_energies = self.physical_model.compute_total_energies(self.configuration)
        NeuralNetworkRandomWalkSampler._update_single_weight(self.configuration, random_coordinate, random_update)
        new_energies = self.physical_model.compute_total_energies(self.configuration)
        NeuralNetworkRandomWalkSampler._update_single_weight(self.configuration, random_coordinate, -random_update)
        energy_differences = new_energies - current_energies
        if self.is_energy_difference_averaged:
            return np.mean(energy_differences), np.var(energy_differences) / (len(energy_differences) - 1)
        return energy_differences[-1], np.var(energy_differences)
