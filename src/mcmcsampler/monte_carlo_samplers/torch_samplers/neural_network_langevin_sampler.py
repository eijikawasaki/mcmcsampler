from typing import Any, List, Optional, Union

import numpy as np
import torch

from mcmcsampler.monte_carlo_samplers.torch_samplers.abstract_neural_network_sampler import AbstractNeuralNetworkSampler
from mcmcsampler.physical_models.torch_models import NeuralNetworkMiniBatchModel, NeuralNetworkModel

from .neural_network_langevin_proposal import NeuralNetworkLangevinProposal


class NeuralNetworkLangevinSampler(AbstractNeuralNetworkSampler):
    """
    examples of torch optimizer:
        - https://github.com/alisiahkoohi/Langevin-dynamics/blob/master/langevin_sampling/SGLD.py
        - https://www.lyndonduong.com/sgmcmc/
        - https://colab.research.google.com/github/simeneide/blog/blob/master/_notebooks/2020-11-13-bayesian-deep
        -learning.ipynb#scrollTo=eakEqUkn2dZ2
        - https://henripal.github.io/blog/langevin
    """

    def __init__(
        self,
        configuration: dict,
        physical_model: Union[NeuralNetworkModel, NeuralNetworkMiniBatchModel],
        lattice: Optional[Any] = None,
        is_progress_bar_disable: bool = False,
    ):
        """

        Args:
            configuration: configuration's state_dict
            physical_model: NeuralNetworkModel or NeuralNetworkMiniBatchModel
            lattice: Langevin does not require a lattice (gradient update)
            is_progress_bar_disable:
        """
        super().__init__(
            configuration=configuration, physical_model=physical_model, is_progress_bar_disable=is_progress_bar_disable
        )
        self.proposal = NeuralNetworkLangevinProposal(physical_model)
        self.random_numbers: List[float] = []
        self.current_local_energy = self.physical_model.compute_total_energy(self.configuration)

    def sample(
        self,
        temperature: float = 1.0,
        record_step: int = 1,
        total_number_iterations: int = 10,
        random_walk_step_size: float = 1,
        is_metropolis_adjusted: bool = True,
    ) -> np.ndarray:
        """

        Args:
            total_number_iterations: total_number_iterations
            temperature: temperature
            record_step: record_step
            random_walk_step_size: random_walk_step_size
            is_metropolis_adjusted: if ```False``` the move is automatically accepted
        Returns:
            samples: samples
        """
        self.random_numbers = self._create_list_random_numbers(total_number_iterations)
        return self._create_samples(
            temperature,
            record_step,
            total_number_iterations,
            random_walk_step_size=random_walk_step_size,
            is_metropolis_adjusted=is_metropolis_adjusted,
        )

    def _sample_single_step(
        self, temperature: float, random_walk_step_size=np.inf, is_metropolis_adjusted: bool = True, **kwargs
    ) -> None:
        """
        Metropolis-Hastings:
            https://en.wikipedia.org/wiki/Metropolis%E2%80%93Hastings_algorithm

            - proposal_probability: $g(x|x')/g(x'|x)$
            - _compute_exponential: $p(x')/p(x)$
        """
        noises = NeuralNetworkLangevinSampler._create_noise(self.configuration)
        suggested_configuration = self.suggest_new_configuration(
            noises=noises,
            random_walk_step_size=random_walk_step_size,
        )
        random_number = self.random_numbers.pop()

        if not is_metropolis_adjusted:
            self.configuration = suggested_configuration
            self.current_local_energy = np.inf
            self.acceptance += 1
        else:
            proposal_probability = self.proposal.proposal_function(
                self.configuration, suggested_configuration, noises, random_walk_step_size
            )
            suggested_local_energy = self.physical_model.compute_total_energy(suggested_configuration)
            energy_difference = suggested_local_energy - self.current_local_energy
            if (
                energy_difference < 0
                or random_number < self._compute_exponential(energy_difference, temperature) * proposal_probability
            ):
                self.configuration = suggested_configuration
                self.current_local_energy = suggested_local_energy
                self.acceptance += 1

    def suggest_new_configuration(self, noises: list, random_walk_step_size: float) -> dict:
        r"""
        Reference:
            https://en.wikipedia.org/wiki/Metropolis-adjusted_Langevin_algorithm

        $$\theta_{t+1}=\theta_t-\eta\nabla \mathcal{L}_{\mathcal{D}}(\theta_t)+\sqrt{2\eta}\epsilon$$

        $$\epsilon\sim\mathcal{N}(0,I_d)$$

        The Gaussian noise $\epsilon$ is added as a noisy loss term $\sqrt{2\eta}\epsilon\cdot\theta_t$ where
        $\theta_t$ is the vector of parameters i.e. the current configuration.
        See ```NeuralNetworkModel``` documentation.

        Args:
            noises: noises
            random_walk_step_size: random_walk_step_size

        Returns:
            new_configuration: new_configuration
        """
        assert hasattr(self.physical_model, "module")
        assert hasattr(self.physical_model, "get_module_configuration_copy")
        assert hasattr(self.physical_model, "load_configration_into_module")
        assert hasattr(self.physical_model, "compute_noisy_loss")
        self.physical_model.load_configration_into_module(self.configuration)
        optimizer = NeuralNetworkLangevinSampler.create_optimizer(self.physical_model.module)
        optimizer.zero_grad()
        loss = random_walk_step_size * self.physical_model.compute_negative_log_likelihood()
        noisy_loss = self.physical_model.compute_noisy_loss(noises)
        loss -= np.sqrt(2 * random_walk_step_size) * noisy_loss
        loss.backward()
        optimizer.step()
        new_configuration = self.physical_model.get_module_configuration_copy()
        # copy such that new_configuration does not point to physical_model.module
        self.physical_model.load_configration_into_module(self.configuration)
        return new_configuration

    @staticmethod
    def _create_noise(configuration: dict) -> list:
        noises = []
        for layer_name in configuration:
            noise = torch.randn_like(configuration.get(layer_name))
            noises.append(noise)
        return noises

    @staticmethod
    def create_optimizer(module: torch.nn.Module):
        optimizer = torch.optim.SGD(
            params=module.parameters(),
            lr=1.0,
            momentum=0.0,
            dampening=0.0,
            weight_decay=0.0,
            nesterov=False,
        )
        return optimizer
