```mermaid
classDiagram
    AbstractMonteCarloSampler <|-- ProposalSampler
    AbstractMonteCarloSampler <|-- AbstractNumpySampler
    AbstractMonteCarloSampler <|-- AbstractNeuralNetworkSampler
    AbstractMonteCarloSampler : +configuration
    AbstractMonteCarloSampler : +acceptance
    AbstractMonteCarloSampler : +physical_model
    AbstractMonteCarloSampler : +lattice
    AbstractMonteCarloSampler: +sample()
    AbstractMonteCarloSampler: +_create_samples()
    class ProposalSampler{
      +proposal_function
      +_sample_single_step()
    }
    class AbstractNeuralNetworkSampler{
      +minimize_energy()
    }
```


::: mcmcsampler.monte_carlo_samplers.AbstractMonteCarloSampler
    handler: python
    rendering:
      show_root_heading: true
      show_source: false
      members_order: 'source'
      show_signature: false

::: mcmcsampler.metadata_handler.MetadataHandler
    handler: python
    rendering:
      show_root_heading: true
      show_source: false
      members_order: 'source'
      show_signature: false
