import pytest

from mcmcsampler.lattices import SquareLattice
from mcmcsampler.monte_carlo_samplers.numpy_samplers import WolffSampler
from mcmcsampler.observables import NumpyObservables
from mcmcsampler.physical_models.numpy_models import IsingModel


class TestWolffAlgorithm:
    @staticmethod
    def test_ising_wolff():
        ising_model = IsingModel()
        square_lattice = SquareLattice()
        initial_configuration = ising_model.initialize_random_configuration(configuration_shape=(3, 3))
        engine = WolffSampler(lattice=square_lattice, configuration=initial_configuration)
        engine.sample(total_number_iterations=10**3)

    @staticmethod
    def compute_total_energy(_configuration) -> float:
        return NumpyObservables.compute_total_energy(
            lattice=SquareLattice(),
            configuration=_configuration,
            compute_local_energy_function=IsingModel().compute_local_energy,
        )

    @staticmethod
    def compute_average_energy(_configuration) -> float:
        return TestWolffAlgorithm.compute_total_energy(_configuration=_configuration) / _configuration.size

    @staticmethod
    @pytest.fixture
    def metropolis_2d():
        configuration_shape = (10, 10)
        initial_configuration = IsingModel.initialize_random_configuration(configuration_shape)
        return WolffSampler(configuration=initial_configuration, lattice=SquareLattice(), is_progress_bar_disable=True)

    @staticmethod
    def test_minimize_energy(metropolis_2d):
        initial_energy = TestWolffAlgorithm.compute_average_energy(metropolis_2d.configuration)
        new_energy = initial_energy
        for i in range(10):
            old_energy = TestWolffAlgorithm.compute_average_energy(metropolis_2d.configuration)
            metropolis_2d.sample(
                temperature=1e-10,
                total_number_iterations=1,
            )
            new_energy = TestWolffAlgorithm.compute_average_energy(metropolis_2d.configuration)
            assert old_energy >= new_energy
        assert initial_energy > new_energy
        assert new_energy == -2.0

    @staticmethod
    def test_maximize_energy(metropolis_2d):
        metropolis_2d.sample(temperature=1e10, total_number_iterations=int(1e5))
        average_energy = 0.0
        for _ in range(10):
            metropolis_2d.sample(
                temperature=1e10,
                total_number_iterations=int(1e3),
            )
            average_energy += TestWolffAlgorithm.compute_average_energy(metropolis_2d.configuration) / 10.0
        assert round(average_energy, ndigits=0) == 0.0
