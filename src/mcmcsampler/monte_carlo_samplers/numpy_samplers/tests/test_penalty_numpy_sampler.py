import numpy as np
import pytest

from mcmcsampler.lattices import SquareLattice
from mcmcsampler.monte_carlo_samplers.numpy_samplers import LocalEnergySampler, PenaltyNumpySampler
from mcmcsampler.physical_models.numpy_models import NoisyPhi4Model, Phi4Model


class TestPenaltyNumpySampler:
    energy_noise_std = 2.0

    @staticmethod
    def test_integration_noisy_phi4_model(initial_config):
        lattice, phi4_model, initial_configuration = initial_config
        true_mean, true_var = TestPenaltyNumpySampler.compute_phi4_energies(lattice, phi4_model, initial_configuration)
        penalty_mean, penalty_var = TestPenaltyNumpySampler.compute_noisy_corrected_energies(
            lattice, initial_configuration
        )
        no_penalty_mean, no_penalty_var = TestPenaltyNumpySampler.compute_noisy_not_corrected_energies(
            lattice, initial_configuration
        )
        assert abs(true_mean - penalty_mean) < abs(true_mean - no_penalty_mean)
        assert abs(true_var - penalty_var) < abs(true_var - no_penalty_var)

    @staticmethod
    @pytest.fixture
    def initial_config():
        configuration_shape = (3, 3)
        lattice = SquareLattice()
        phi4_model = Phi4Model(lattice=lattice)
        initial_configuration = phi4_model.initialize_random_configuration(configuration_shape)
        return lattice, phi4_model, initial_configuration

    @staticmethod
    def compute_phi4_energies(lattice, phi4_model, initial_configuration):
        engine = LocalEnergySampler(
            configuration=initial_configuration,
            physical_model=phi4_model,
            lattice=lattice,
            is_progress_bar_disable=True,
        )
        for _ in range(2):
            samples = engine.sample(temperature=1.0, total_number_iterations=10**4, record_step=10**2)
        true_energies = [phi4_model.compute_total_energy(sample) for sample in samples]
        return np.mean(true_energies), np.var(true_energies)

    @classmethod
    def compute_noisy_corrected_energies(cls, lattice, initial_configuration):
        penalty_regularization = 1.0

        physical_model = NoisyPhi4Model(lattice=lattice, energy_noise_std=cls.energy_noise_std)
        engine = PenaltyNumpySampler(
            configuration=initial_configuration,
            physical_model=physical_model,
            lattice=lattice,
            is_progress_bar_disable=True,
        )
        for _ in range(2):
            samples = engine.sample(
                temperature=1.0,
                total_number_iterations=10**4,
                record_step=10**2,
                penalty_regularization=penalty_regularization,
                energy_noise_std=cls.energy_noise_std,
            )
        penalty_energies = [physical_model.compute_total_energy(sample) for sample in samples]
        return np.mean(penalty_energies), np.var(penalty_energies)

    @classmethod
    def compute_noisy_not_corrected_energies(cls, lattice, initial_configuration):
        penalty_regularization = 0.0

        physical_model = NoisyPhi4Model(lattice=lattice, energy_noise_std=cls.energy_noise_std)
        engine = PenaltyNumpySampler(
            configuration=initial_configuration,
            physical_model=physical_model,
            lattice=lattice,
            is_progress_bar_disable=True,
        )
        for _ in range(2):
            samples = engine.sample(
                temperature=1.0,
                total_number_iterations=10**4,
                record_step=10**2,
                penalty_regularization=penalty_regularization,
                energy_noise_std=cls.energy_noise_std,
            )
        no_penalty_energies = [physical_model.compute_total_energy(sample) for sample in samples]
        return np.mean(no_penalty_energies), np.var(no_penalty_energies)
