from mcmcsampler.lattices import SquareLattice
from mcmcsampler.monte_carlo_samplers.numpy_samplers import GlobalEnergySampler
from mcmcsampler.physical_models.numpy_models import Phi4Model


class TestGlobalEnergySampler:
    @staticmethod
    def test_phi4_metropolis():
        square_lattice = SquareLattice()
        phi4_model = Phi4Model(lattice=square_lattice)
        initial_configuration = phi4_model.initialize_random_configuration(configuration_shape=(3, 3))
        engine = GlobalEnergySampler(
            physical_model=phi4_model,
            lattice=square_lattice,
            configuration=initial_configuration,
        )
        engine.sample(total_number_iterations=10**3)
        assert 1.0 > engine.acceptance > 0.0
