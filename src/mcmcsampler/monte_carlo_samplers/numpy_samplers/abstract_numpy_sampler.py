import json
from abc import ABC, abstractmethod

import numpy as np

from mcmcsampler.lattices import SquareLattice
from mcmcsampler.monte_carlo_samplers.abstract_monte_carlo_sampler import AbstractMonteCarloSampler
from mcmcsampler.physical_models import AbstractPhysicalModel


class AbstractNumpySampler(AbstractMonteCarloSampler, ABC):
    def __init__(
        self,
        configuration: np.ndarray,
        physical_model: AbstractPhysicalModel,
        lattice: SquareLattice = SquareLattice(),
        is_progress_bar_disable: bool = False,
    ):
        super().__init__(configuration, physical_model, lattice, is_progress_bar_disable=is_progress_bar_disable)
        self.lattice = lattice
        self.configuration_shape = configuration.shape

    def __repr__(self) -> str:
        dico = json.loads(super().__repr__())
        dico.update(json.loads(repr(self.lattice)))
        dico.update({"configuration_shape": self.configuration_shape})
        return json.dumps(dico)

    @property
    def _indices(self) -> list:
        return SquareLattice.get_indices(self.configuration_shape)

    def _create_list_random_coordinates(self, total_number_iterations: int) -> list:
        return [
            self.lattice.compute_random_coordinate(self.configuration.size, self._indices)
            for _ in range(total_number_iterations)
        ]

    @abstractmethod
    def sample(
        self,
        temperature: float = 1,
        record_step: int = 1,
        total_number_iterations: int = 10,
    ):
        self._create_samples(temperature, record_step, total_number_iterations)
