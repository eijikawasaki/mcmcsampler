from typing import Optional, Tuple

import numpy as np

from .global_energy_sampler import GlobalEnergySampler


class PenaltyNumpySampler(GlobalEnergySampler):
    def sample(
        self,
        temperature: float = 1,
        record_step: int = 1,
        total_number_iterations: int = 10,
        penalty_regularization: float = 1.0,
        energy_noise_std: Optional[float] = None,
    ) -> np.ndarray:
        """
        Args:
            total_number_iterations: total number of iterations
            temperature: temperature
            record_step: records the configuration every ```record_step```
            penalty_regularization:
            energy_noise_std:

        Returns:
            samples: samples
        """
        self.random_coordinates = self._create_list_random_coordinates(total_number_iterations)
        self.random_numbers = self._create_list_random_numbers(total_number_iterations)
        self.random_updates = self._create_list_random_updates(total_number_iterations)
        return self._create_samples(
            temperature,
            record_step,
            total_number_iterations,
            penalty_regularization=penalty_regularization,
            energy_noise_std=energy_noise_std,
        )

    def _sample_single_step(
        self, temperature: float, penalty_regularization=1.0, energy_noise_std: Optional[float] = None, **kwargs
    ):
        random_update = self.random_updates.pop()
        coordinates = self.random_coordinates.pop()
        random_number = self.random_numbers.pop()

        estimated_energy_difference, var_energy_difference = self._compute_energy_difference_and_variance(
            coordinates, random_update, energy_noise_std
        )
        log_acceptance = estimated_energy_difference + penalty_regularization * var_energy_difference / (
            2.0 * temperature
        )
        if log_acceptance < 0 or random_number < self._compute_exponential(log_acceptance, temperature):
            self.configuration[coordinates] += random_update
            self.acceptance += 1.0

    def _compute_energy_difference(self, coordinates: tuple, random_update: float) -> float:
        current_energy = self.physical_model.compute_total_energy(self.configuration)
        self.configuration[coordinates] += random_update
        suggested_energy = self.physical_model.compute_total_energy(self.configuration)
        energy_difference = suggested_energy - current_energy
        self.configuration[coordinates] -= random_update
        return energy_difference

    def _compute_energy_difference_and_variance(
        self, coordinates: tuple, random_update, energy_noise_std: Optional[float]
    ) -> Tuple[float, float]:
        current_energies = self.physical_model.compute_total_energies(self.configuration)
        self.configuration[coordinates] += random_update
        new_energies = self.physical_model.compute_total_energies(self.configuration)
        self.configuration[coordinates] -= random_update
        energy_differences = new_energies - current_energies
        if energy_noise_std is None:
            return float(np.mean(energy_differences)), float(np.var(energy_differences) / (len(energy_differences) - 1))
        return float(np.mean(energy_differences)), 2.0 * energy_noise_std**2
        # factor 2 on the variance because the noise is added independently on each energy
