```mermaid
classDiagram
    AbstractNumpySampler <|-- GlobalNumpySampler
    AbstractNumpySampler <|-- GlobalEnergySampler
    AbstractNumpySampler <|-- LocalEnergySampler
    LocalEnergySampler <|-- MirrorSymmetricLocalEnergySampler
    AbstractNumpySampler <|-- PenaltyNumpySampler
    AbstractNumpySampler <|-- RBMSampler
    AbstractNumpySampler <|-- WolffSampler
```


::: mcmcsampler.monte_carlo_samplers.numpy_samplers.LocalEnergySampler
    handler: python
    rendering:
      show_root_heading: true
      show_source: false
      members_order: 'source'
      show_signature: false

::: mcmcsampler.monte_carlo_samplers.numpy_samplers.MirrorSymmetricLocalEnergySampler
    handler: python
    rendering:
      show_root_heading: true
      show_source: false
      members_order: 'source'
      show_signature: false

::: mcmcsampler.monte_carlo_samplers.numpy_samplers.GlobalEnergySampler
    handler: python
    rendering:
      show_root_heading: true
      show_source: false
      members_order: 'source'
      show_signature: false

::: mcmcsampler.monte_carlo_samplers.numpy_samplers.GlobalNumpySampler
    handler: python
    rendering:
      show_root_heading: true
      show_source: false
      members_order: 'source'
      show_signature: false

::: mcmcsampler.monte_carlo_samplers.numpy_samplers.WolffSampler
    handler: python
    rendering:
      show_root_heading: true
      show_source: false
      members_order: 'source'
      show_signature: false

::: mcmcsampler.monte_carlo_samplers.numpy_samplers.RBMSampler
    handler: python
    rendering:
      show_root_heading: true
      show_source: false
      members_order: 'source'
      show_signature: false

::: mcmcsampler.monte_carlo_samplers.numpy_samplers.PenaltyNumpySampler
    handler: python
    rendering:
      show_root_heading: true
      show_source: false
      members_order: 'source'
      show_signature: false
