import functools
import random
import sys
from typing import List

import numpy as np

from mcmcsampler.lattices import SquareLattice
from mcmcsampler.monte_carlo_samplers.numpy_samplers.abstract_numpy_sampler import AbstractNumpySampler
from mcmcsampler.physical_models.numpy_models import IsingModel


class WolffSampler(AbstractNumpySampler):
    """
    References:
        https://en.wikipedia.org/wiki/Wolff_algorithm
    """

    _spin_flip_values = {-1: 1, 1: -1}

    def __init__(
        self,
        configuration: np.ndarray,
        lattice: SquareLattice,
        physical_model=IsingModel(),
        is_progress_bar_disable: bool = False,
    ):
        """
        Args:
            configuration: configuration
            lattice: lattice
        """
        super().__init__(
            configuration,
            physical_model=physical_model,
            lattice=lattice,
            is_progress_bar_disable=is_progress_bar_disable,
        )
        self.random_coordinates: List[tuple] = []

    def sample(
        self,
        temperature: float = 2.269185314,
        record_step: int = 1,
        total_number_iterations: int = 10,
    ) -> np.ndarray:
        """

        Args:
            total_number_iterations: total number of iterations
            temperature: temperature is set to critical temperature
            record_step: records the configuration every ```record_step```

        Returns:
            samples: samples
        """
        self.random_coordinates = self._create_list_random_coordinates(total_number_iterations)
        return self._create_samples(
            temperature,
            record_step,
            total_number_iterations,
        )

    def _sample_single_step(self, temperature: float, **kwargs):
        coordinates = self.random_coordinates.pop()
        cluster = [coordinates]
        sys.setrecursionlimit(10**6)
        cluster = self._recursive_compute_cluster_from_coordinate(temperature, cluster, coordinates)
        self._flip_spins_in_cluster(cluster)
        self.acceptance += 1

    def _compute_cluster_from_coordinate(self, temperature: float, cluster: list, to_be_visited: list):
        while to_be_visited:
            current_site = random.choice(to_be_visited)
            for neighbor in self.lattice.compute_closest_neighbors(self.configuration_shape, current_site):
                if (
                    neighbor not in cluster
                    and self.configuration[neighbor] == self.configuration[current_site]
                    and np.random.rand() < self._compute_magic_probability(temperature)
                ):
                    to_be_visited.append(neighbor)
                    cluster.append(neighbor)
            to_be_visited.remove(current_site)
        return cluster, to_be_visited

    def _recursive_compute_cluster_from_coordinate(self, temperature: float, cluster: list, coordinate: tuple):
        for neighbor in self.lattice.compute_closest_neighbors(self.configuration_shape, coordinate):
            if (
                neighbor not in cluster
                and self.configuration[coordinate] == self.configuration[neighbor]
                and self._compute_magic_probability(temperature) > np.random.rand()
            ):
                cluster.append(neighbor)
                cluster = self._recursive_compute_cluster_from_coordinate(temperature, cluster, neighbor)
        return cluster

    def _flip_spins_in_cluster(self, cluster: list):
        [self._flip_spin(coordinate) for coordinate in cluster]

    def _flip_spin(self, coordinates: tuple):
        self.configuration[coordinates] = self._spin_flip_values.get(self.configuration[coordinates])

    @staticmethod
    @functools.lru_cache()
    def _compute_magic_probability(temperature: float) -> float:
        return 1.0 - np.exp(-2.0 / temperature)
