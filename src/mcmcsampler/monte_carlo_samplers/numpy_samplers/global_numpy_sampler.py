from typing import List

import numpy as np
from numba import jit

from ...lattices import SquareLattice
from ...physical_models import AbstractPhysicalModel
from .abstract_numpy_sampler import AbstractNumpySampler


class GlobalNumpySampler(AbstractNumpySampler):
    """
    Random walk with global energy computation and non-local moves.
    """

    # TODO: write tests

    def __init__(
        self,
        configuration: np.ndarray,
        physical_model: AbstractPhysicalModel,
        lattice: SquareLattice = SquareLattice(),
        is_progress_bar_disable: bool = False,
    ):
        super().__init__(configuration, physical_model, lattice, is_progress_bar_disable=is_progress_bar_disable)
        self.random_numbers: List[float] = []
        self.random_updates: List[float] = []

    def sample(
        self,
        temperature: float = 1,
        record_step: int = 1,
        total_number_iterations: int = 10,
        random_walk_step_size: float = 1.0,
    ) -> np.ndarray:
        """
        Args:
            total_number_iterations: total number of iterations
            temperature: temperature
            record_step: records the configuration every ```record_step```
            random_walk_step_size

        Returns:
            samples: samples
        """
        self.random_numbers = self._create_list_random_numbers(total_number_iterations)
        self.random_updates = self._create_list_random_updates(
            total_number_iterations, self.configuration_shape, random_walk_step_size
        )
        return self._create_samples(
            temperature,
            record_step,
            total_number_iterations,
        )

    def _sample_single_step(self, temperature: float, **kwargs):
        random_update = self.random_updates.pop()
        random_number = self.random_numbers.pop()
        energy_difference = self._compute_energy_difference(random_update)
        if energy_difference < 0 or random_number < GlobalNumpySampler._compute_exponential(
            energy_difference, temperature
        ):
            self.configuration += random_update
            self.acceptance += 1

    def _compute_energy_difference(self, random_update: float) -> float:
        current_energy = self.physical_model.compute_total_energy(self.configuration)
        self.configuration += random_update
        suggested_energy = self.physical_model.compute_total_energy(self.configuration)
        energy_difference = suggested_energy - current_energy
        self.configuration -= random_update
        return energy_difference

    @staticmethod
    @jit(nopython=True)
    def _create_list_random_updates(total_number_iterations: int, shape: tuple, random_walk_step_size: float) -> list:
        random_list = [np.random.randn(*shape) * random_walk_step_size for _ in range(total_number_iterations)]
        return random_list.copy()
