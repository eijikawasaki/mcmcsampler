import numpy as np

from .local_energy_sampler import LocalEnergySampler


class MirrorSymmetricLocalEnergySampler(LocalEnergySampler):
    r"""
    Similar to Phi4LocalMetropolis but manually ensures that the configuration has a mirror symmetry.
    $$\sum_i \psi_i \simeq 0$$
    """

    def _sample_single_step(self, temperature: float, **kwargs):
        super()._sample_single_step(temperature)
        self.configuration = np.random.choice([-1, 1]) * self.configuration
