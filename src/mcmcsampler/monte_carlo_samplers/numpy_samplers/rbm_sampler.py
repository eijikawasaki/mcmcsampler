import functools
from typing import List

import numpy as np

from mcmcsampler.lattices import SquareLattice
from mcmcsampler.monte_carlo_samplers.numpy_samplers.abstract_numpy_sampler import AbstractNumpySampler
from mcmcsampler.physical_models.numpy_models import RBM


class RBMSampler(AbstractNumpySampler):
    """
    Random walk for a Restricted Boltzmann Machine. The possible states for each neurone are $0$ or $1$.
    """

    # TODO: write tests
    _neuron_flip_values = {0: 1, 1: 0}

    def __init__(
        self,
        configuration: np.ndarray,
        physical_model: RBM,
        lattice: SquareLattice = SquareLattice(),
        is_progress_bar_disable: bool = False,
    ):
        super().__init__(configuration, physical_model, lattice, is_progress_bar_disable=is_progress_bar_disable)
        self.random_coordinates: List[tuple] = []
        self.random_numbers: List[float] = []

    def sample(
        self,
        temperature: float = 1.0,
        record_step: int = 1,
        total_number_iterations: int = 10,
    ) -> np.ndarray:
        """
        Args:
            total_number_iterations: total number of iterations
            temperature: temperature
            record_step: records the configuration every ```record_step```

        Returns:
            samples: samples
        """
        self.random_coordinates = self._create_list_random_coordinates(total_number_iterations)
        self.random_numbers = self._create_list_random_numbers(total_number_iterations)
        return self._create_samples(
            temperature,
            record_step,
            total_number_iterations,
        )

    def _sample_single_step(self, temperature: float, **kwargs):
        coordinates = self.random_coordinates.pop()
        random_number = self.random_numbers.pop()
        energy_difference = self._compute_energy_difference(coordinates)
        if energy_difference < 0 or random_number < self._compute_exponential(energy_difference, temperature):
            self._flip_neuron_state(coordinates)

    def _compute_energy_difference(self, coordinates) -> float:
        current_energy = self.physical_model.compute_total_energy(self.configuration)
        self._flip_neuron_state(coordinates)  # flips neuron state to compute new suggested energy
        new_energy = self.physical_model.compute_total_energy(self.configuration)
        self._flip_neuron_state(coordinates)  # flips neuron to restore it to the previous state
        return new_energy - current_energy

    def _flip_neuron_state(self, coordinates: tuple):
        self.configuration[coordinates] = self._neuron_flip_values[self.configuration[coordinates]]

    @staticmethod
    @functools.lru_cache()
    def _compute_exponential(energy_difference: float, temperature: float) -> float:
        return np.exp(-energy_difference / temperature)
