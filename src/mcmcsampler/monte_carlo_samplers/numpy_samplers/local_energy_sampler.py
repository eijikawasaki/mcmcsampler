from typing import List

import numpy as np
from numba import jit

from mcmcsampler.monte_carlo_samplers.numpy_samplers.abstract_numpy_sampler import AbstractNumpySampler

from ...lattices import SquareLattice
from ...observables import NumpyObservables
from ...physical_models import AbstractPhysicalModel


class LocalEnergySampler(AbstractNumpySampler):
    """
    Random walk with local energy.
    """

    def __init__(
        self,
        configuration: np.ndarray,
        physical_model: AbstractPhysicalModel,
        lattice: SquareLattice = SquareLattice(),
        is_progress_bar_disable: bool = False,
    ):
        super().__init__(configuration, physical_model, lattice, is_progress_bar_disable=is_progress_bar_disable)
        self.random_coordinates: List[tuple] = []
        self.random_numbers: List[float] = []
        self.random_updates: List[float] = []
        self.neighbors_list: List[frozenset] = []

    def sample(
        self,
        temperature: float = 1,
        record_step: int = 1,
        total_number_iterations: int = 10,
    ) -> np.ndarray:
        """
        Args:
            total_number_iterations: total number of iterations
            temperature: temperature
            record_step: records the configuration every ```record_step```

        Returns:
            samples: samples
        """
        self.random_coordinates = self._create_list_random_coordinates(total_number_iterations)
        self.random_numbers = self._create_list_random_numbers(total_number_iterations)
        self.random_updates = self._create_list_random_updates(total_number_iterations)
        self.neighbors_list = [
            self.lattice.compute_closest_neighbors(self.configuration_shape, coordinates)
            for coordinates in self.random_coordinates
        ]
        return self._create_samples(
            temperature,
            record_step,
            total_number_iterations,
        )

    def _sample_single_step(self, temperature: float, **kwargs):
        random_update = self.random_updates.pop()
        coordinates = self.random_coordinates.pop()
        random_number = self.random_numbers.pop()
        neighbors = self.neighbors_list.pop()
        energy_difference = self._compute_energy_difference(coordinates, neighbors, random_update)

        if energy_difference < 0 or random_number < LocalEnergySampler._compute_exponential(
            energy_difference, temperature
        ):
            self.configuration[coordinates] += random_update
            self.acceptance += 1.0

    def _compute_energy_difference(self, coordinates: tuple, neighbors: frozenset, random_update: float) -> float:
        current_local_energy = self.compute_area_energy(coordinates, neighbors)
        self.configuration[coordinates] += random_update
        suggested_local_energy = self.compute_area_energy(coordinates, neighbors)
        self.configuration[coordinates] -= random_update
        energy_difference = suggested_local_energy - current_local_energy
        return energy_difference

    def compute_area_energy(self, coordinates: tuple, neighbors: frozenset) -> float:
        neighbors_configurations = NumpyObservables.get_neighbors_configurations(self.configuration, neighbors)
        current_local_energy = self.physical_model.compute_local_energy(
            local_configuration=self.configuration[coordinates], neighbors_configurations=neighbors_configurations
        )
        for neighbors_coordinates in neighbors:
            neighbors_neighbors = self.lattice.compute_closest_neighbors(
                self.configuration_shape, neighbors_coordinates
            )
            neighbors_configurations = NumpyObservables.get_neighbors_configurations(
                self.configuration, neighbors_neighbors
            )
            current_local_energy += self.physical_model.compute_local_energy(
                local_configuration=self.configuration[neighbors_coordinates],
                neighbors_configurations=neighbors_configurations,
            )
        # TODO: check 0.5 factor
        return 0.5 * current_local_energy

    @staticmethod
    @jit(nopython=True)
    def _create_list_random_updates(total_number_iterations: int) -> list:
        random_list = np.random.rand(total_number_iterations) * 2.0 - 1.0
        return list(random_list)
