from typing import List

import numpy as np
from numba import jit

from mcmcsampler.lattices import SquareLattice
from mcmcsampler.physical_models import AbstractPhysicalModel

from .abstract_numpy_sampler import AbstractNumpySampler


class GlobalEnergySampler(AbstractNumpySampler):
    """
    Random walk with global energy computation.
    """

    # TODO: write tests

    def __init__(
        self,
        configuration: np.ndarray,
        physical_model: AbstractPhysicalModel,
        lattice: SquareLattice = SquareLattice(),
        is_progress_bar_disable: bool = False,
    ):
        super().__init__(configuration, physical_model, lattice, is_progress_bar_disable=is_progress_bar_disable)
        self.random_coordinates: List[tuple] = []
        self.random_numbers: List[float] = []
        self.random_updates: List[float] = []

    def sample(
        self,
        temperature: float = 1,
        record_step: int = 1,
        total_number_iterations: int = 10,
    ) -> np.ndarray:
        """
        Args:
            total_number_iterations: total number of iterations
            temperature: temperature
            record_step: records the configuration every ```record_step```

        Returns:
            samples: samples
        """
        self.random_coordinates = self._create_list_random_coordinates(total_number_iterations)
        self.random_numbers = self._create_list_random_numbers(total_number_iterations)
        self.random_updates = self._create_list_random_updates(total_number_iterations)
        return self._create_samples(
            temperature,
            record_step,
            total_number_iterations,
        )

    def _sample_single_step(self, temperature: float, **kwargs):
        random_update = self.random_updates.pop()
        coordinates = self.random_coordinates.pop()
        random_number = self.random_numbers.pop()
        energy_difference = self._compute_energy_difference(coordinates, random_update)
        if energy_difference < 0 or random_number < GlobalEnergySampler._compute_exponential(
            energy_difference, temperature
        ):
            self.configuration[coordinates] += random_update
            self.acceptance += 1.0

    def _compute_energy_difference(self, coordinates: tuple, random_update: float) -> float:
        current_energy = self.physical_model.compute_total_energy(self.configuration)
        self.configuration[coordinates] += random_update
        suggested_energy = self.physical_model.compute_total_energy(self.configuration)
        energy_difference = suggested_energy - current_energy
        self.configuration[coordinates] -= random_update
        return energy_difference

    @staticmethod
    @jit(nopython=True)
    def _create_list_random_updates(total_number_iterations: int) -> list:
        random_list = np.random.rand(total_number_iterations) * 2.0 - 1.0
        return list(random_list)
