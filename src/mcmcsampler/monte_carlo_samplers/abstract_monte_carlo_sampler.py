import copy
import json
from abc import ABC, abstractmethod
from tempfile import NamedTemporaryFile
from typing import Any, Optional

import numpy as np
import torch
from numba import jit
from tqdm import trange

from mcmcsampler.lattices import AbstractLattice
from mcmcsampler.metadata_handler import MetadataHandler
from mcmcsampler.physical_models import AbstractPhysicalModel


class AbstractMonteCarloSampler(ABC, MetadataHandler):
    def __init__(
        self,
        configuration: Any,
        physical_model: AbstractPhysicalModel,
        lattice: Optional[AbstractLattice] = None,
        is_progress_bar_disable: bool = False,
    ):
        """
        Args:
            configuration: configuration
            physical_model: physical_model
            lattice: lattice
            is_progress_bar_disable: is_progress_bar_disable
        """
        self.configuration = copy.deepcopy(configuration)
        self.physical_model = copy.deepcopy(physical_model)
        self.is_progress_bar_disable = is_progress_bar_disable

        self.acceptance = np.inf
        temporary_file = NamedTemporaryFile(delete=False)
        temporary_file_name = temporary_file.name + ".pt"
        self.temporary_file_name = temporary_file_name

    def __repr__(self) -> str:
        dico = {"simulation": type(self).__name__}
        dico.update(json.loads(repr(self.physical_model)))
        return json.dumps(dico)

    @abstractmethod
    def sample(self, temperature: float = 1, record_step: int = 1):
        """

        Args:
            temperature:
            record_step:

        Returns:

        """
        self._create_samples(temperature, record_step, total_number_iterations=0)

    def _create_samples(
        self,
        temperature: float,
        record_step: int,
        total_number_iterations: int,
        **kwargs,
    ):
        """

        Args:
            total_number_iterations:
            record_step:
            is_progress_bar_disable:
            temperature:
            **kwargs:

        Returns:

        """
        samples = []
        progress_bar = self.create_progress_bar(total_number_iterations, record_step)
        self.acceptance = 0
        for i in progress_bar:
            for _ in range(record_step):
                self._sample_single_step(temperature, **kwargs)
            samples.append(copy.deepcopy(self.configuration))
            progress_bar.set_postfix({"current acceptance": self.acceptance / (record_step * (i + 1))})
        self.acceptance /= total_number_iterations
        return np.asarray(samples)

    def sample_with_swap_memory(self, *args, **kwargs):
        """

        Args:
            *args:
            **kwargs:

        Returns:

        """
        samples = self.sample(*args, **kwargs)
        torch.save(samples, self.temporary_file_name)
        return self

    @staticmethod
    @jit(nopython=True)
    def _create_list_random_numbers(total_number_iterations: int) -> list:
        random_list = np.random.rand(total_number_iterations)
        return list(random_list)

    @staticmethod
    @jit(nopython=True)
    def _copy_is_required(step, record_step):
        return step % record_step == 0

    @staticmethod
    @jit(nopython=True)
    def _compute_exponential(energy_difference: float, temperature: float) -> float:
        return np.exp(-energy_difference / temperature)

    def create_progress_bar(self, total_number_iterations, record_step):
        progress_bar = trange(
            int(total_number_iterations // record_step),
            disable=self.is_progress_bar_disable,
            desc=type(self).__name__,
        )
        return progress_bar

    @abstractmethod
    def _sample_single_step(self, temperature, **kwargs):
        """
        Single step: configuration update and acceptance update

        Args:
            temperature:
            **kwargs:

        Returns:

        """
        pass
