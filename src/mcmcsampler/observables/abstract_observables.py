from abc import ABC, abstractmethod


class AbstractObservables(ABC):
    @staticmethod
    @abstractmethod
    def compute_average_density(*args, **kwargs) -> float:
        pass

    @staticmethod
    @abstractmethod
    def compute_average_configuration(*args, **kwargs) -> float:
        pass
