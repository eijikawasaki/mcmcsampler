from .numpy_observables import NumpyObservables
from .torch_observables import TorchObservables
