import copy
from typing import Union

import numpy as np
import torch
from torch.func import functional_call

from .abstract_observables import AbstractObservables


class TorchObservables(AbstractObservables):
    @staticmethod
    def compute_average_density(model) -> float:
        density = 0.0
        with torch.no_grad():
            for _, param in model.named_parameters():
                density += np.sum(np.square(param.numpy()))
        density /= TorchObservables.get_number_of_parameters(model)
        return density

    @staticmethod
    def compute_average_configuration(model) -> float:
        average_configuration = 0.0
        with torch.no_grad():
            for _, param in model.named_parameters():
                average_configuration += np.sum(param.numpy())
        average_configuration /= TorchObservables.get_number_of_parameters(model)
        return average_configuration

    @staticmethod
    def get_number_of_parameters(model: dict):
        number_of_parameters = np.sum([layer_content.numel() for layer_content in model.values()])
        return number_of_parameters

    @staticmethod
    def predict_from_samples(module, samples, input_data, output_data):
        predictions = functional_call(
            module=module,
            parameter_and_buffer_dicts=samples,
            args=(input_data, output_data),
            tie_weights=True,
            strict=True,
        )
        return predictions

    @staticmethod
    def turn_parameters_configuration_into_numpy_vector(
        module: torch.nn.Module, configuration: dict, is_output_numpy: bool = True
    ) -> Union[np.ndarray, torch.Tensor]:
        temp_module = copy.deepcopy(module)
        temp_module.load_state_dict(configuration)
        parameters = torch.nn.utils.parameters_to_vector(temp_module.parameters()).detach()
        if is_output_numpy:
            return parameters.numpy()
        return parameters

    @staticmethod
    def check_two_configurations_dict_are_equal(configuration_1, configuration_2):
        return str(configuration_1) == str(configuration_2)
