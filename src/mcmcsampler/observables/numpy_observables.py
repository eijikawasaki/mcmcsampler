import itertools
from typing import Callable, Type

import numpy as np
from numba import jit

from ..lattices import SquareLattice
from .abstract_observables import AbstractObservables


class NumpyObservables(AbstractObservables):
    @staticmethod
    def get_configuration_copy(_configuration: np.ndarray) -> np.ndarray:
        return np.copy(_configuration)

    @staticmethod
    def get_local_configuration(configuration: np.ndarray, coordinates: tuple) -> int:
        return configuration[coordinates]

    @staticmethod
    def get_neighbors_configurations(configuration: np.ndarray, neighbors: frozenset) -> tuple:
        return tuple(configuration[neighbors_coordinates] for neighbors_coordinates in neighbors)

    @staticmethod
    def compute_total_energy(
        lattice: SquareLattice,
        configuration: np.ndarray,
        compute_local_energy_function: Callable,
    ) -> float:
        configuration_shape = configuration.shape
        all_neighbors = [
            lattice.compute_closest_neighbors(configuration_shape, coordinates)
            for coordinates in SquareLattice.get_indices(configuration_shape)
        ]
        all_local_configurations = [
            configuration[coordinates] for coordinates in SquareLattice.get_indices(configuration_shape)
        ]
        all_neighbors_configurations = [
            NumpyObservables.get_neighbors_configurations(configuration, neighbors) for neighbors in all_neighbors
        ]
        total_energy = sum(
            [
                compute_local_energy_function(local_configuration, neighbors_configurations)
                for local_configuration, neighbors_configurations in zip(
                    all_local_configurations, all_neighbors_configurations
                )
            ]
        )
        return 0.5 * total_energy

    @staticmethod
    @jit(nopython=True)
    def compute_average_magnetization(configuration: np.ndarray) -> float:
        return configuration.sum() / configuration.size

    @staticmethod
    def compute_average_absolute_magnetization(configuration: np.ndarray) -> float:
        return abs(configuration.sum() / abs(configuration).sum())

    @staticmethod
    def compute_average_ising_correlation(configuration: np.ndarray, final_configuration: np.ndarray) -> float:
        indices = SquareLattice.get_indices(configuration.shape)
        average_correlation = sum(
            [configuration[coordinates] * final_configuration[coordinates] for coordinates in indices]
        )
        average_correlation /= configuration.size
        return average_correlation

    @staticmethod
    @jit(nopython=True)
    def compute_average_density(configuration: np.ndarray) -> float:
        return np.square(configuration).mean()

    @staticmethod
    @jit(nopython=True)
    def compute_average_configuration(configuration: np.ndarray) -> float:
        return configuration.mean()

    @staticmethod
    def compute_two_points_correlation_function(lattice: Type[SquareLattice], configuration: np.ndarray):
        """\frac{<s_is_i+j>-<s_i><s_i>}{<s_i^2>-<s_i><s_i>}
        <s_i> corresponds to the average magnetization
        <s_i^2> corresponds to the average variance of the magnetization
        averages are taken over a single configuration $1/L sum_i=1^L$ with L the system size
        """
        coordinates = lattice.get_indices(configuration.shape)
        two_points_correlations = NumpyObservables._compute_all_two_points_correlations(
            coordinates, configuration, lattice
        )
        average_magnetization = np.mean(configuration)
        two_points_correlations = NumpyObservables._normalize_and_average_two_points_correlations(
            two_points_correlations, average_magnetization
        )
        return two_points_correlations

    @staticmethod
    def _normalize_and_average_two_points_correlations(two_points_correlations, average_magnetization):
        for distance in two_points_correlations:
            two_points_correlations[distance] = np.mean(two_points_correlations[distance])
        # We want two_points_correlations[0.] = 1. by definition
        normalization = two_points_correlations[0.0] - average_magnetization**2
        if normalization != 0:
            two_points_correlations = {
                key: (value - average_magnetization**2) / normalization
                for key, value in two_points_correlations.items()
            }
        else:
            two_points_correlations = {
                key: value / two_points_correlations[0.0] for key, value in two_points_correlations.items()
            }
        return two_points_correlations

    @staticmethod
    def _compute_all_two_points_correlations(coordinates, configuration, lattice):
        two_points_correlations = {}
        for (first_coordinate, second_coordinate) in itertools.product(coordinates, coordinates):
            distance = lattice.compute_distance(configuration.shape, first_coordinate, second_coordinate)
            if distance not in two_points_correlations:
                two_points_correlations.update({distance: []})
            correlation = configuration[first_coordinate] * configuration[second_coordinate]
            two_points_correlations[distance].append(correlation)
        return two_points_correlations
