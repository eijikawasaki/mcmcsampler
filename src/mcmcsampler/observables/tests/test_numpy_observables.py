import numpy as np
import pytest

from mcmcsampler.lattices import SquareLattice
from mcmcsampler.observables import NumpyObservables
from mcmcsampler.physical_models.numpy_models import IsingModel, Phi4Model


class TestNumpyObservables:
    @staticmethod
    def test_compute_ising_correlation():
        configuration = IsingModel.initialize_random_configuration(configuration_shape=(int(1e6),))
        different_configuration = IsingModel.initialize_random_configuration(configuration_shape=(int(1e6),))
        assert (
            round(
                NumpyObservables.compute_average_ising_correlation(configuration, different_configuration),
                ndigits=2,
            )
            == 0.0
        )
        assert (
            NumpyObservables.compute_average_ising_correlation(
                configuration, NumpyObservables.get_configuration_copy(configuration)
            )
            == 1.0
        )

    @staticmethod
    @pytest.mark.parametrize(
        "local_energy_function",
        [Phi4Model().compute_local_energy, IsingModel().compute_local_energy],
    )
    def test_compute_total_energy(local_energy_function):
        configuration_shape = (10, 10)
        up_low_energy_state = np.ones(configuration_shape)
        up_low_energy = NumpyObservables.compute_total_energy(
            SquareLattice(), up_low_energy_state, local_energy_function
        )

        down_low_energy_state = -np.ones(configuration_shape)
        down_low_energy = NumpyObservables.compute_total_energy(
            SquareLattice(), down_low_energy_state, local_energy_function
        )

        random_high_energy_state = np.random.choice(2, configuration_shape)
        random_high_energy_state[random_high_energy_state == 0] = -1.0
        random_high_energy = NumpyObservables.compute_total_energy(
            SquareLattice(), random_high_energy_state, local_energy_function
        )

        first_excitation_low_energy_state = np.ones(configuration_shape)
        first_excitation_low_energy_state[0][0] = -1.0
        first_excitation_low_energy = NumpyObservables.compute_total_energy(
            SquareLattice(), first_excitation_low_energy_state, local_energy_function
        )

        assert up_low_energy == down_low_energy
        assert random_high_energy > up_low_energy
        assert up_low_energy < first_excitation_low_energy < random_high_energy

    @staticmethod
    @pytest.mark.parametrize(
        "lattice, configuration, expected_correlations",
        [
            (SquareLattice, np.ones((1,)), {0.0: 1.0}),
            (SquareLattice, np.ones((1, 1)), {0.0: 1.0}),
            (SquareLattice, np.ones((2,)), {0.0: 1.0, 1.0: 1.0}),
            (SquareLattice, np.ones((2, 2)), {0.0: 1.0, 1.0: 1.0, 1.4142135623730951: 1.0}),
            (
                SquareLattice,
                -1 * np.ones((2, 2)),
                {0.0: 1.0, 1.0: 1.0, 1.4142135623730951: 1.0},
            ),
        ],
    )
    def test_compute_two_points_correlation_function(lattice, configuration, expected_correlations):
        assert expected_correlations == NumpyObservables.compute_two_points_correlation_function(lattice, configuration)
