```mermaid
classDiagram
    class AbstractPhysicalModel{
      +compute_total_energy()
      +initialize_random_configuration()
    }
```
::: mcmcsampler.physical_models.AbstractPhysicalModel
    handler: python
    rendering:
      show_root_heading: true
      show_source: false
      members_order: 'source'
      show_signature: false
