import pytest

from mcmcsampler.tests.mlp import MLPTrainer


@pytest.fixture(scope="module")
def flat_mlp_model():
    input_dim = 1
    hidden_dim = (1,)
    output_dim = 1
    mlp_model = MLPTrainer(input_dim, hidden_dim, output_dim)
    return mlp_model


@pytest.fixture(scope="module")
def wide_mlp_model():
    input_dim = 1
    hidden_dim = (10,)
    output_dim = 1
    mlp_model = MLPTrainer(input_dim, hidden_dim, output_dim)
    return mlp_model
