import torch
from torch.utils.data import DataLoader, TensorDataset

from mcmcsampler.monte_carlo_samplers.torch_samplers import NeuralNetworkLangevinSampler
from mcmcsampler.observables import TorchObservables
from mcmcsampler.physical_models.torch_models import NeuralNetworkModel
from mcmcsampler.tests.mlp import MLPTrainer


class TestNeuralNetworkModel:
    @staticmethod
    def nn_model(module):
        train_input_data = torch.zeros((10, 1))
        train_output_data = torch.zeros((10, 1))
        dataset = TensorDataset(train_input_data, train_output_data)
        loader = DataLoader(dataset)
        nn_model = NeuralNetworkModel(loader, weight_decay=0.0, module=module)
        return nn_model

    @staticmethod
    def test_0_weights_is_0_energy(flat_mlp_model):
        nn_model = TestNeuralNetworkModel.nn_model(flat_mlp_model)
        with torch.no_grad():
            for name, param in flat_mlp_model.named_parameters():
                param.copy_(torch.zeros(1))
        total_energy = nn_model.compute_total_energy(flat_mlp_model.state_dict())
        assert total_energy == 0.0

    @staticmethod
    def test_2_bias_is_4_energy(flat_mlp_model):
        """
        there are 2 biases set to 1
        since the input is 0 other weights do not contribute
        The prediction y_pred is 1+1 = 2
        The loss is therefore 2**2 = 4
        """
        nn_model = TestNeuralNetworkModel.nn_model(flat_mlp_model)
        with torch.no_grad():
            for name, param in flat_mlp_model.named_parameters():
                param.copy_(torch.ones(1))
        total_energy = nn_model.compute_total_energy(flat_mlp_model.state_dict())
        assert total_energy == 4.0

    @staticmethod
    def test_flat_weight_decay_energy(flat_mlp_model):
        nn_model = TestNeuralNetworkModel.nn_model(flat_mlp_model)
        number_of_parameters = 0
        with torch.no_grad():
            for name, param in flat_mlp_model.named_parameters():
                param.copy_(torch.ones(1))
                number_of_parameters += 1

        nn_model.weight_decay = 1.0
        total_energy = nn_model.compute_total_energy(flat_mlp_model.state_dict())
        assert total_energy == 4.0 + 0.5 * 1.0 * number_of_parameters

        nn_model.weight_decay = 0.5
        total_energy = nn_model.compute_total_energy(flat_mlp_model.state_dict())
        assert total_energy == 4.0 + 0.5 * 0.5 * number_of_parameters

    @staticmethod
    def test_get_number_of_parameters(wide_mlp_model):
        number_of_parameters = 0.0
        with torch.no_grad():
            for _, param in wide_mlp_model.named_parameters():
                number_of_parameters += param.numpy().size
        assert number_of_parameters == TorchObservables.get_number_of_parameters(wide_mlp_model.state_dict())

    @staticmethod
    def test_weight_decay_energy(wide_mlp_model):
        nn_model = TestNeuralNetworkModel.nn_model(wide_mlp_model)
        with torch.no_grad():
            for name, param in wide_mlp_model.named_parameters():
                param.copy_(torch.ones(1))

        nn_model.weight_decay = 1.0
        total_energy = nn_model.compute_total_energy(wide_mlp_model.state_dict())
        assert total_energy == 121.0 + 0.5 * 1.0 * TorchObservables.get_number_of_parameters(
            wide_mlp_model.state_dict()
        )

    @staticmethod
    def test_compute_negative_log_likelihood_computes_gradients(wide_mlp_model):
        nn_model = TestNeuralNetworkModel.nn_model(wide_mlp_model)
        loss = nn_model.compute_negative_log_likelihood()
        assert loss.grad_fn is not None

    @staticmethod
    def test_compute_noisy_loss():
        model = MLPTrainer(input_dim=1, hidden_dim=tuple(10 for _ in range(10)), output_dim=1)
        configuration = model.state_dict()
        nn_model = TestNeuralNetworkModel.nn_model(model)
        noises = NeuralNetworkLangevinSampler._create_noise(configuration)
        noisy_loss = nn_model.compute_noisy_loss(noises)
        assert isinstance(noisy_loss, torch.Tensor)
