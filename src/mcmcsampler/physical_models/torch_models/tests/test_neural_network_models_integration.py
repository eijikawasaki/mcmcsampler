import numpy as np
import pytest
import torch
from torch.utils.data import DataLoader, TensorDataset

from mcmcsampler.physical_models.torch_models import NeuralNetworkMiniBatchModel, NeuralNetworkModel


class TestNeuralNetworkModelsIntegration:
    @staticmethod
    @pytest.fixture
    def data():
        train_input_data = torch.randn((10, 1))
        train_output_data = torch.randn((10, 1))
        dataset = TensorDataset(train_input_data, train_output_data)
        loader = DataLoader(dataset, batch_size=len(train_input_data))
        return train_input_data, train_output_data, dataset, loader

    @staticmethod
    def test_single_and_mini_batches_have_same_total_energy(flat_mlp_model, data):
        train_input_data, train_output_data, dataset, loader = data
        nn_model = NeuralNetworkModel(loader, weight_decay=1.0, module=flat_mlp_model)
        configuration = flat_mlp_model.state_dict()
        single_batch_total_energy = nn_model.compute_total_energy(configuration)

        loader = DataLoader(dataset, batch_size=len(train_input_data))
        nn_model = NeuralNetworkMiniBatchModel(loader, weight_decay=1.0, module=flat_mlp_model)
        configuration = flat_mlp_model.state_dict()
        mini_batch_total_energy = nn_model.compute_total_energies(configuration)
        assert single_batch_total_energy == mini_batch_total_energy

    @staticmethod
    def test_single_data_batch_loader_computes_correct_total_energy(flat_mlp_model, data):
        train_input_data, train_output_data, dataset, loader = data
        nn_model = NeuralNetworkModel(loader, weight_decay=1.0, module=flat_mlp_model)
        configuration = flat_mlp_model.state_dict()
        single_batch_total_energy = nn_model.compute_total_energy(configuration)

        loader = DataLoader(dataset)
        nn_model = NeuralNetworkMiniBatchModel(loader, weight_decay=1.0, module=flat_mlp_model)
        configuration = flat_mlp_model.state_dict()
        mini_batch_total_energies = nn_model.compute_total_energies(configuration)
        mini_batch_total_energy = np.mean(mini_batch_total_energies)
        assert abs(single_batch_total_energy - mini_batch_total_energy) < 1e-6

    @staticmethod
    def test_inverse_temperature_balances_prior_and_data_likelihood(flat_mlp_model, data):
        train_input_data, train_output_data, dataset, loader = data
        nn_model = NeuralNetworkModel(loader, weight_decay=0.1, module=flat_mlp_model, inverse_temperature=1)
        configuration = flat_mlp_model.state_dict()
        single_batch_total_energy = nn_model.compute_total_energy(configuration)
        loader = DataLoader(dataset)

        nn_model = NeuralNetworkMiniBatchModel(loader, weight_decay=1.0, module=flat_mlp_model)
        configuration = flat_mlp_model.state_dict()
        mini_batch_total_energies = nn_model.compute_total_energies(configuration)
        mini_batch_total_energy = np.mean(mini_batch_total_energies)
        assert abs(10 * single_batch_total_energy - mini_batch_total_energy) < 1e-6
