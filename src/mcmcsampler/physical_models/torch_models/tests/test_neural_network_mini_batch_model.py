import numpy as np
import pytest
import torch
from torch.utils.data import DataLoader, TensorDataset

from mcmcsampler.physical_models.torch_models import NeuralNetworkMiniBatchModel


class TestNeuralNetworkNoisyModel:
    @staticmethod
    def test_train_input_data_list_shape(flat_mlp_model):
        train_input_data = torch.randn((2, 10))
        train_output_data = torch.randn((2, 10))
        dataset = TensorDataset(train_input_data, train_output_data)
        loader = DataLoader(dataset, batch_size=1)
        nn_model = NeuralNetworkMiniBatchModel(loader, flat_mlp_model)
        assert len(nn_model.train_input_data_list) == 2
        assert nn_model.train_input_data_list[0].shape == torch.Size([1, 10])

        train_input_data = torch.randn((10, 1))
        train_output_data = torch.randn((10, 1))
        dataset = TensorDataset(train_input_data, train_output_data)
        loader = DataLoader(dataset, batch_size=1)
        nn_model = NeuralNetworkMiniBatchModel(loader, flat_mlp_model)
        assert len(nn_model.train_input_data_list) == 10
        assert nn_model.train_input_data_list[0].shape == torch.Size([1, 1])

    @staticmethod
    def test_create_random_indices():
        assert NeuralNetworkMiniBatchModel.create_random_indices_without_replacement(
            list_length=1, number_of_data_batch=1
        ) == [0]
        with pytest.raises(IndexError):
            NeuralNetworkMiniBatchModel.create_random_indices_without_replacement(
                list_length=1, number_of_data_batch=2
            ) == [0, 0]
        random_indices = NeuralNetworkMiniBatchModel.create_random_indices_without_replacement(
            list_length=10, number_of_data_batch=3
        )
        assert len(random_indices) == len(set(random_indices))
        assert np.shape(random_indices) == (3,)

    @staticmethod
    @pytest.mark.parametrize("batch_size, number_of_data_batches", [(1, 1), (1, 10), (2, 5), (5, 2), (3, 3)])
    def test_number_of_total_energies_corresponds_to_number_of_data_batches(
        flat_mlp_model, batch_size, number_of_data_batches
    ):
        initial_configuration = flat_mlp_model.state_dict()
        train_input_data = torch.randn((10, 1))
        train_output_data = torch.randn((10, 1))
        dataset = TensorDataset(train_input_data, train_output_data)

        penalty_loader = DataLoader(dataset, batch_size=batch_size)
        penalty_nn_model = NeuralNetworkMiniBatchModel(
            penalty_loader, flat_mlp_model, weight_decay=1.0, number_of_data_batches=number_of_data_batches
        )
        assert len(train_input_data) == 10
        assert (
            len(penalty_nn_model.compute_total_energies(configuration=initial_configuration)) == number_of_data_batches
        )

    @staticmethod
    def test_compute_total_energies_with_replacement(flat_mlp_model):
        initial_configuration = flat_mlp_model.state_dict()
        batch_size = 5
        number_of_batches = 3
        train_input_data = torch.randn((10, 1))
        train_output_data = torch.randn((10, 1))
        dataset = TensorDataset(train_input_data, train_output_data)

        penalty_loader = DataLoader(dataset, batch_size=batch_size)
        penalty_nn_model = NeuralNetworkMiniBatchModel(
            penalty_loader,
            flat_mlp_model,
            weight_decay=1.0,
            number_of_data_batches=number_of_batches,
            with_replacement=True,
        )
        assert len(train_input_data) == 10
        assert len(penalty_nn_model.compute_total_energies(initial_configuration)) == number_of_batches

        penalty_nn_model = NeuralNetworkMiniBatchModel(
            penalty_loader,
            flat_mlp_model,
            weight_decay=1.0,
            number_of_data_batches=number_of_batches,
            with_replacement=False,
        )
        with pytest.raises(IndexError):
            assert len(penalty_nn_model.compute_total_energies(initial_configuration)) == number_of_batches
