from typing import Tuple

import numpy as np
import torch


class SampleTransformer:
    @staticmethod
    def convert_physical_to_ml(sample_array: np.ndarray, configuration_shape: tuple):
        return torch.tensor(sample_array.reshape(len(sample_array), np.prod(configuration_shape)).astype(np.float32))

    @staticmethod
    def convert_ml_to_physical(sample_array: np.ndarray, configuration_shape: tuple):
        return sample_array.reshape(len(sample_array), *configuration_shape)

    @staticmethod
    def convert_two_physical_samples_to_ml(sample_1, sample_2, configuration_shape: tuple) -> Tuple[np.array, np.array]:
        sample_1 = SampleTransformer.convert_physical_to_ml(np.asarray([sample_1]), configuration_shape)
        sample_2 = SampleTransformer.convert_physical_to_ml(np.asarray([sample_2]), configuration_shape)
        return sample_1, sample_2
