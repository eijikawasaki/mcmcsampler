import copy

import numpy as np


class EnergyTransformer:
    mean_energy = None
    std = None

    def compute_transformation_parameter(self, physical_energies):
        self.mean_energy = np.mean(physical_energies)
        self.std = np.std(physical_energies)

    def convert_physical_to_ml(self, physical_energies):
        energies = copy.copy(physical_energies)
        energies -= self.mean_energy
        return energies / (10 * self.std)

    def convert_ml_to_physical(self, ml_energies):
        energies = copy.copy(ml_energies)
        energies = energies * (10 * self.std)
        energies += self.mean_energy
        return energies
