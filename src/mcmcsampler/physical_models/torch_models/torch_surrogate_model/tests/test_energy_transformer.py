import numpy as np

from mcmcsampler.physical_models.torch_models import EnergyTransformer


class TestEnergyTransformer:
    @staticmethod
    def test_convert_physical_to_ml():
        physical_energies = 2.5 * np.random.randn(100) + 3
        transformer = EnergyTransformer()
        transformer.compute_transformation_parameter(physical_energies)
        ml_energies = transformer.convert_physical_to_ml(physical_energies)
        new_physical_energies = transformer.convert_ml_to_physical(ml_energies)
        np.testing.assert_allclose(physical_energies, new_physical_energies)
        assert abs(np.mean(ml_energies)) < 1e-5
        assert abs(np.std(ml_energies) - 0.1) < 1e-10
