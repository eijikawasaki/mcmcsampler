import numpy as np
import torch

from mcmcsampler.lattices import SquareLattice
from mcmcsampler.monte_carlo_samplers.numpy_samplers import GlobalEnergySampler, LocalEnergySampler
from mcmcsampler.multi_process_engine import MultiProcessEngine
from mcmcsampler.physical_models.numpy_models import Phi4Model
from mcmcsampler.physical_models.torch_models import SampleTransformer, TorchSurrogateModel
from mcmcsampler.tests.mlp import MLPTrainer


class TestTorchSurrogateModel:
    @staticmethod
    def test_integration_train_physical_model():
        MultiProcessEngine.set_seeds(seed=42)
        train_set_size = 1000
        configuration_shape = (3,)
        temperature = 0.5

        lattice = SquareLattice()
        physical_model = Phi4Model(lattice=lattice)
        initial_configuration = physical_model.initialize_random_configuration(configuration_shape)

        engine = LocalEnergySampler(
            configuration=initial_configuration,
            physical_model=physical_model,
            lattice=lattice,
        )
        samples = []
        for _ in range(2):
            samples = engine.sample(temperature=temperature, total_number_iterations=10**4, record_step=10**2)
        true_energies = [physical_model.compute_total_energy(sample) for sample in samples]

        outputs = []
        for _ in range(3):
            mlp = MLPTrainer(input_dim=3, hidden_dim=(10**3,), output_dim=1)
            approximate_physical_model = TorchSurrogateModel(
                energy_computer=mlp, configuration_shape=configuration_shape
            )
            engine = GlobalEnergySampler(
                configuration=initial_configuration,
                physical_model=approximate_physical_model,
                lattice=lattice,
            )
            train_set = np.asarray(
                [physical_model.initialize_random_configuration(configuration_shape) for _ in range(train_set_size)]
            )
            TestTorchSurrogateModel.train_physical_model(
                engine.physical_model, physical_model, train_set, configuration_shape
            )
            for _ in range(2):
                samples = engine.sample(temperature=temperature, total_number_iterations=10**4, record_step=10**2)
            outputs.append(samples)
        samples = MultiProcessEngine.concatenate_multiprocess_outputs(outputs)
        approximate_energies = [physical_model.compute_total_energy(sample) for sample in samples]

        true_mean, true_var = np.mean(true_energies), np.var(true_energies)
        meta_mean, meta_var = np.mean(approximate_energies), np.var(approximate_energies)
        assert abs((true_mean - meta_mean) / true_mean) < 0.5
        assert abs((true_var - meta_var) / true_var) < 0.5

    @staticmethod
    def train_physical_model(ml_model, physical_model, train_set, configuration_shape, epochs=500):
        ml_train_set = SampleTransformer.convert_physical_to_ml(train_set, configuration_shape)
        physical_energies = TestTorchSurrogateModel.compute_physical_energies(train_set, physical_model)
        ml_model.compute_transformation_parameter(physical_energies)
        ml_energies = TestTorchSurrogateModel.compute_ml_energies_from_physical(physical_energies, ml_model)
        ml_model.energy_computer.do_training(ml_train_set, ml_energies, epochs)

    @staticmethod
    def compute_physical_energies(train_set, physical_model):
        physical_energies = [[physical_model.compute_total_energy(sample)] for sample in train_set]
        return physical_energies

    @staticmethod
    def compute_ml_energies_from_physical(physical_energies, ml_model):
        ml_energies = [
            ml_model.energy_transformer.convert_physical_to_ml(physical_energy) for physical_energy in physical_energies
        ]
        ml_energies = np.asarray(ml_energies)
        ml_energies = ml_energies.astype(np.float32)
        return torch.tensor(ml_energies)
