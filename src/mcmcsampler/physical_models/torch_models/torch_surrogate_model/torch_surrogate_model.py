import numpy as np

from mcmcsampler.physical_models import AbstractPhysicalModel

from .energy_trasformer import EnergyTransformer
from .sample_transformer import SampleTransformer


class TorchSurrogateModel(AbstractPhysicalModel):
    def __init__(self, configuration_shape: tuple, energy_computer=None, energy_computers=()):
        """

        Args:
            configuration_shape:
            energy_computer:
            energy_computers:
        """
        self.energy_computer = energy_computers[-1] if energy_computer is None else energy_computer
        self.energy_computers = energy_computers
        self.energy_transformer = EnergyTransformer()
        self.configuration_shape = configuration_shape

    def compute_total_energy(self, configuration: np.ndarray) -> float:
        ml_configuration = SampleTransformer.convert_physical_to_ml(
            np.asarray([configuration]), self.configuration_shape
        )
        ml_energy = float(self.energy_computer.predict(ml_configuration))
        return self.energy_transformer.convert_ml_to_physical(ml_energy)

    def compute_transformation_parameter(self, physical_energies):
        self.energy_transformer.compute_transformation_parameter(physical_energies)

    @staticmethod
    def initialize_random_configuration(configuration_shape: tuple) -> np.ndarray:
        raise NotImplementedError

    def compute_local_energy(self, local_configuration: float, neighbors_configurations: tuple) -> float:
        raise NotImplementedError

    def compute_total_energies(self, model):
        total_energies = []
        for energy_computer in self.energy_computers:
            ml_configuration = SampleTransformer.convert_physical_to_ml(np.asarray([model]), self.configuration_shape)
            ml_energy = float(energy_computer.predict(ml_configuration))
            total_energies.append(self.energy_transformer.convert_ml_to_physical(ml_energy))
        return np.asarray(total_energies)
