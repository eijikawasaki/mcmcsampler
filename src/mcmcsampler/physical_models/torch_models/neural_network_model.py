import copy
import json
from typing import Optional

import torch
from torch.utils.data import DataLoader

from mcmcsampler.physical_models.torch_models.abstract_neural_network_model import AbstractNeuralNetworkModel


class NeuralNetworkModel(AbstractNeuralNetworkModel):
    """
    Characteristics:
        - Requires a ```module.compute_negative_log_likelihood``` method to compute the loss.
        - ```weight_decay``` corresponds to a Gaussian prior over the configuration's parameters.
    """

    def __init__(
        self,
        data_loader: DataLoader,
        module: torch.nn.Module,
        weight_decay: float = 1e-1,
        inverse_temperature: Optional[float] = None,
    ):
        """
        Args:
            data_loader: requires a torch.utils.data.DataLoader\
            the batch_size attribute should be set to len(\
            data_set_size) so that one mini batch is created containing the full data set. See:\
            https://pytorch.org/docs/stable/data.html#torch.utils.data.DataLoader
            module: useful for compute_negative_log_likelihood
            weight_decay: weight decay (L2 regularization)
            inverse_temperature: N/B with N data set size and B batch size
        """
        super().__init__(module, weight_decay)
        self.train_input_data, self.train_output_data = next(iter(data_loader))
        if inverse_temperature is None:
            inverse_temperature = float(len(self.train_input_data))
        self.inverse_temperature = inverse_temperature

    def __repr__(self) -> str:
        dico = json.loads(super().__repr__())
        dico.update({"inverse_temperature": self.inverse_temperature})
        return json.dumps(dico)

    def compute_total_energy(self, configuration: dict) -> float:
        """
        Args:
            configuration: torch configuration able to ```compute_negative_log_likelihood```

        Returns:
            total_energy: total energy
        """
        self.module.load_state_dict(configuration)
        negative_log_likelihood = self.compute_negative_log_likelihood()
        negative_log_likelihood = negative_log_likelihood.detach().numpy()
        negative_log_likelihood *= self.inverse_temperature / len(self.train_input_data)
        negative_log_prior = float(self.compute_negative_log_prior(configuration))
        return negative_log_likelihood + negative_log_prior

    def compute_negative_log_likelihood(self):
        negative_log_likelihood = self.module.compute_negative_log_likelihood(
            self.train_input_data, self.train_output_data
        )
        return negative_log_likelihood

    def load_configration_into_module(self, configuration: dict):
        """

        Args:
            configuration:

        """
        self.module.load_state_dict(configuration)

    def get_module_configuration_copy(self):
        """

        Returns:

        """
        return copy.deepcopy(self.module.state_dict())

    def compute_noisy_loss(self, noises: list):
        r"""
        Method useful for suggesting a new configuration in the Langevin sampling algorithm.

        This noisy loss term corresponds to $\sqrt{2\eta}\epsilon\cdot\theta_t$ where
        $\theta_t$ is the vector of parameters i.e. the current configuration.

        Args:
            noises:

        Returns:

        """
        loss = 0.0
        for param, noise in zip(self.module.parameters(), noises):
            loss += (noise * param).sum()
        return loss

    def compute_module_gradient_based_on_configuration(self, configuration: dict) -> None:
        self.module.load_state_dict(configuration)
        self.module.zero_grad()
        loss = self.compute_negative_log_likelihood()
        loss.backward()

    def get_module_parameters_reference(self):
        return self.module.parameters()
