import json
import random
from typing import Optional, Tuple

import numpy as np
import torch
from torch.utils.data import DataLoader

from mcmcsampler.physical_models.torch_models.abstract_neural_network_model import AbstractNeuralNetworkModel


class NeuralNetworkMiniBatchModel(AbstractNeuralNetworkModel):
    """
    Computes randomly samples batches on the fly.

    Characteristics:
        - Requires a ```configuration.compute_negative_log_likelihood``` method to compute the energy.
        - ```weight_decay``` corresponds to a Gaussian prior over the configuration's parameters.
    """

    def __init__(
        self,
        data_loader: DataLoader,
        module: torch.nn.Module,
        weight_decay: float = 0.0,
        number_of_data_batches: Optional[int] = None,
        with_replacement: bool = False,
    ):
        """

        Args:
            data_loader: https://pytorch.org/docs/stable/data.html#torch.utils.data.DataLoader
            weight_decay:
            module:
            number_of_data_batches:
            with_replacement:
        """
        super().__init__(module, weight_decay)
        self.with_replacement = with_replacement
        self.train_input_data_list: list = []
        self.train_output_data_list: list = []
        self._store_data_batches(data_loader)
        assert len(self.train_input_data_list) == len(self.train_output_data_list)
        if number_of_data_batches is None:
            number_of_data_batches = len(self.train_input_data_list)
        self.number_of_data_batches = number_of_data_batches

    def __repr__(self) -> str:
        dico = json.loads(super().__repr__())
        dico.update({"number_of_data_batches": self.number_of_data_batches})
        dico.update({"with_replacement": self.with_replacement})
        return json.dumps(dico)

    def _store_data_batches(self, data_loader):
        for train_input_data, train_output_data in iter(data_loader):
            self.train_input_data_list.append(train_input_data)
            self.train_output_data_list.append(train_output_data)

    def compute_total_energy(self, configuration: dict) -> float:
        raise NotImplementedError

    def compute_total_energies(self, configuration: dict) -> np.array:
        """
        Args:
            configuration: torch configuration able to ```compute_negative_log_likelihood```

        Returns:
            total_energies: numpy array containing total energies
        """
        self.module.load_state_dict(configuration)
        random_data_indices = NeuralNetworkMiniBatchModel.compute_random_data_indices(
            self.with_replacement, len(self.train_input_data_list), self.number_of_data_batches
        )
        total_energies = [
            self._compute_single_batch_total_energy(configuration, random_data_index)
            for random_data_index in random_data_indices
        ]
        return np.asarray(total_energies)

    @staticmethod
    def compute_random_data_indices(
        with_replacement: bool, train_input_data_list_length: int, number_of_data_batches: int
    ) -> np.array:
        # TODO: test static method
        if with_replacement:
            random_indices = NeuralNetworkMiniBatchModel.create_random_indices_with_replacement(
                train_input_data_list_length, number_of_data_batches
            )
        else:
            random_indices = NeuralNetworkMiniBatchModel.create_random_indices_without_replacement(
                train_input_data_list_length, number_of_data_batches
            )
        return random_indices

    def _compute_single_batch_total_energy(self, configuration: dict, random_data_index: int) -> float:
        negative_log_likelihood = self.compute_negative_log_likelihood(random_data_index)
        negative_log_likelihood = negative_log_likelihood.detach().numpy()
        negative_log_prior = self.compute_negative_log_prior(configuration)
        total_energy = negative_log_likelihood + negative_log_prior
        return total_energy

    def compute_negative_log_likelihood(self, random_data_index: int = 0):
        train_input_data, train_output_data = self._get_single_batch_data(random_data_index)
        negative_log_likelihood = self.module.compute_negative_log_likelihood(train_input_data, train_output_data)
        return negative_log_likelihood

    def _get_single_batch_data(self, random_index: int) -> Tuple[torch.Tensor, torch.Tensor]:
        train_input_data = self.train_input_data_list[random_index]
        train_output_data = self.train_output_data_list[random_index]
        return train_input_data, train_output_data

    @staticmethod
    def create_random_indices_with_replacement(list_length: int, number_of_data_batch: int) -> list:
        random_indices = [random.randint(0, list_length - 1) for _ in range(number_of_data_batch)]
        return random_indices

    @staticmethod
    def create_random_indices_without_replacement(list_length: int, number_of_data_batch: int) -> list:
        deck = list(range(0, list_length))
        random.shuffle(deck)
        try:
            random_indices = [deck.pop() for _ in range(number_of_data_batch)]
        except IndexError:
            raise IndexError(
                f"Cannot create {number_of_data_batch} independent batches from a {list_length} sized list."
            )
        return random_indices
