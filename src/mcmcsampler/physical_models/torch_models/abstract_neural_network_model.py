import json
from abc import ABC, abstractmethod

import numpy as np
import torch

from mcmcsampler.physical_models import AbstractPhysicalModel


class AbstractNeuralNetworkModel(AbstractPhysicalModel, ABC):
    def __init__(self, module: torch.nn.Module, weight_decay: float = 1e-1):
        self.module = module
        self.weight_decay = weight_decay

    def __repr__(self) -> str:
        dico: dict = {"weight_decay": self.weight_decay}
        return json.dumps(dico)

    def compute_negative_log_prior(self, configuration: dict):
        """
        Factor 0.5 to match torch definition weight_decay as 1/variance
        """
        negative_log_prior = np.sum([layer_content.pow(2.0).sum().numpy() for layer_content in configuration.values()])
        negative_log_prior *= 0.5 * self.weight_decay
        return negative_log_prior

    @abstractmethod
    def compute_total_energy(self, configuration: dict) -> float:
        """

        Args:
            configuration:

        Returns:

        """
        pass

    @staticmethod
    def initialize_random_configuration(configuration_shape: tuple):
        raise NotImplementedError

    def compute_local_energy(self, local_configuration: float, neighbors_configurations: tuple) -> float:
        raise NotImplementedError

    def compute_total_energies(self, configuration: dict):
        pass

    @abstractmethod
    def compute_negative_log_likelihood(self, *args, **kwargs):
        pass
