One ```configuration``` corresponds to a ```state_dict``` of a pytorch module.
One can load a ```configuration``` sampled by this library by using the pytorch
method ```load_state_dict```.

For more details see PyTorch documentation, in particular [Saving & Loading Model for Inference
](https://pytorch.org/tutorials/beginner/saving_loading_models.html)

```mermaid
classDiagram
    AbstractPhysicalModel <|-- AbstractNeuralNetworkModel
    AbstractNeuralNetworkModel <|-- NeuralNetworkModel
    AbstractNeuralNetworkModel <|-- NeuralNetworkMiniBatchModel
    AbstractPhysicalModel <|-- TorchSurrogateModel
    TorchSurrogateModel *-- EnergyTransformer
    class AbstractNeuralNetworkModel {
        +module
        +weight_decay
        +compute_negative_log_prior()
    }
    class NeuralNetworkModel {
        +data_loader
        +module
        +weight_decay
        +inverse_temperature
        +compute_total_energy()
        +compute_negative_log_likelihood_with_gradient()
    }
    class NeuralNetworkMiniBatchModel {
        +data_loader
        +module
        +number_of_data_batches
        +with_replacement
        +compute_total_energies()
    }
    class TorchSurrogateModel {
        +energy_computer
        +energy_transformer
        +compute_total_energy()
    }
    class EnergyTransformer {
        +compute_transformation_parameter()
        +convert_physical_to_ml()
        +convert_ml_to_physical()
    }
```

::: mcmcsampler.physical_models.torch_models.NeuralNetworkModel
    handler: python
    rendering:
      show_root_heading: true
      show_source: false
      members_order: 'source'
      show_signature: false

::: mcmcsampler.physical_models.torch_models.NeuralNetworkMiniBatchModel
    handler: python
    rendering:
      show_root_heading: true
      show_source: false
      members_order: 'source'
      show_signature: false

::: mcmcsampler.physical_models.torch_models.TorchSurrogateModel
    handler: python
    rendering:
      show_root_heading: true
      show_source: false
      members_order: 'source'
      show_signature: false
