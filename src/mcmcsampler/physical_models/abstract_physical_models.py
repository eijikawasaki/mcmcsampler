import json
from abc import ABC, abstractmethod
from typing import Any

import numpy as np


class AbstractPhysicalModel(ABC):
    def __repr__(self) -> str:
        dico: dict = {}
        return json.dumps(dico)

    @abstractmethod
    def compute_local_energy(self, local_configuration: float, neighbors_configurations: tuple) -> float:
        """

        Args:
            local_configuration:
            neighbors_configurations:

        Returns:

        """
        pass

    @abstractmethod
    def compute_total_energy(self, model: Any) -> float:
        """

        Args:
            model:

        Returns:

        """
        pass

    @staticmethod
    @abstractmethod
    def initialize_random_configuration(configuration_shape: tuple) -> np.ndarray:
        """

        Args:
            configuration_shape:

        Returns:

        """
        pass

    def compute_total_energies(self, model: Any):
        pass

    def compute_negative_log_likelihood(self, *args, **kwargs):
        pass
