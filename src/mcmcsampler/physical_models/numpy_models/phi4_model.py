import json
from typing import Any

import numpy as np
from numba import jit

from mcmcsampler.observables import NumpyObservables
from mcmcsampler.physical_models.abstract_physical_models import AbstractPhysicalModel


class Phi4Model(AbstractPhysicalModel):
    def __init__(
        self,
        chemical_potential: float = 1.0,
        interaction_strength: float = 0.0,
        space_dimensions: int = 2,
        magnetic_configuration: float = 0.0,
        lattice=None,
    ):
        """
        Args:
            chemical_potential: chemical_potential
            interaction_strength: interaction_strength
            space_dimensions: space_dimensions
            magnetic_configuration: magnetic_configuration
        """
        self.chemical_potential_mu = chemical_potential
        self.interaction_strength_g = interaction_strength
        self.space_dimensions = space_dimensions
        self.magnetic_configuration = magnetic_configuration
        self.lattice = lattice

    def __repr__(self) -> str:
        dico = {
            "chemical_potential": self.chemical_potential_mu,
            "interaction_strength": self.interaction_strength_g,
            "space_dimensions": self.space_dimensions,
            "magnetic_configuration": self.magnetic_configuration,
        }
        return json.dumps(dico)

    @staticmethod
    def initialize_random_configuration(configuration_shape: tuple) -> np.ndarray:
        return np.random.rand(*configuration_shape) * 2.0 - 1.0

    def compute_local_energy(self, local_configuration: float, neighbors_configurations: tuple) -> float:
        return self._compute_local_energy(
            local_configuration,
            sum(neighbors_configurations),
            self.space_dimensions,
            self.chemical_potential_mu,
            self.interaction_strength_g,
            self.magnetic_configuration,
        )

    @staticmethod
    @jit(nopython=True)
    def _compute_local_energy(
        local_configuration: float,
        neighbors_total_configuration: float,
        space_dimensions: int,
        chemical_potential_mu: float,
        interaction_strength_g: float,
        magnetic_configuration: float,
    ) -> float:
        # https://en.wikipedia.org/wiki/Finite_difference_coefficient
        local_energy = -local_configuration * neighbors_total_configuration
        local_energy += (2.0 * space_dimensions + chemical_potential_mu) * local_configuration**2
        local_energy += interaction_strength_g * local_configuration**4
        local_energy += magnetic_configuration * local_configuration
        return local_energy

    def compute_total_energy(self, model: Any) -> float:
        return NumpyObservables.compute_total_energy(
            lattice=self.lattice,
            configuration=model,
            compute_local_energy_function=self.compute_local_energy,
        )
