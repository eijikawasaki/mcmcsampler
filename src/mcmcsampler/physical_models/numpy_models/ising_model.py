from typing import Any

import numpy as np

from mcmcsampler.observables import NumpyObservables
from mcmcsampler.physical_models.abstract_physical_models import AbstractPhysicalModel


class IsingModel(AbstractPhysicalModel):
    """
    Reference:
        https://en.wikipedia.org/wiki/Ising_model
    """

    def __init__(self, lattice=None):
        """

        Args:
            lattice:
        """
        self.lattice = lattice

    @staticmethod
    def initialize_ones_configuration(configuration_shape: tuple) -> np.ndarray:
        return np.ones(configuration_shape)

    @staticmethod
    def initialize_minus_ones_configuration(configuration_shape: tuple) -> np.ndarray:
        return -1.0 * np.ones(configuration_shape)

    @staticmethod
    def initialize_random_configuration(configuration_shape: tuple) -> np.ndarray:
        """

        Args:
            configuration_shape:

        Returns:

        """
        configuration = np.random.rand(*configuration_shape) * 2.0 - 1.0
        configuration[configuration < 0] = -1
        configuration[configuration >= 0] = 1
        return configuration

    @classmethod
    def compute_local_energy(cls, local_configuration: float, neighbors_configurations: tuple) -> float:
        """

        Args:
            local_configuration:
            neighbors_configurations:

        Returns:

        """
        neighbors_total_configuration = sum(neighbors_configurations)
        return -1.0 * local_configuration * neighbors_total_configuration

    def compute_total_energy(self, model: Any) -> float:
        """

        Args:
            model:

        Returns:

        """
        return NumpyObservables.compute_total_energy(
            lattice=self.lattice,
            configuration=model,
            compute_local_energy_function=self.compute_local_energy,
        )
