from .ising_model import IsingModel
from .linear_gaussian_model import LinearGaussianModel
from .noisy_phi4_model import NoisyPhi4Model
from .phi4_model import Phi4Model
from .rbm_model import RBM
