from typing import Optional

import numpy as np
from scipy import stats

from mcmcsampler.physical_models.abstract_physical_models import AbstractPhysicalModel


class LinearGaussianModel(AbstractPhysicalModel):
    def __init__(self, data_samples: Optional[list] = None, aleatoric_std: float = 1.0, chemical_potential: float = 1):
        """

        Args:
            data_samples:
            aleatoric_std:
            chemical_potential:
        """
        self.data_samples: list = [] if data_samples is None else data_samples
        self.chemical_potential = chemical_potential
        self.aleatoric_std = aleatoric_std

    def compute_total_energy(self, model: np.array) -> float:
        """

        Args:
            model:

        Returns:

        """
        total_energy = LinearGaussianModel.compute_prior_energy(model, self.chemical_potential)
        total_energy += self.compute_negative_log_likelihood(model)
        return total_energy

    @staticmethod
    def compute_prior_energy(configuration: np.array, chemical_potential: float) -> float:
        """
        Not normalized!

        Args:
            configuration:
            chemical_potential:

        Returns:

        """
        energy = np.linalg.norm(configuration) ** 2 * chemical_potential
        return energy

    def compute_negative_log_likelihood(self, configuration: np.array) -> float:
        negative_log_likelihood = 0.0
        for x_data, y_data in self.data_samples:
            negative_log_likelihood += stats.multivariate_normal.logpdf(
                y_data, np.dot(configuration, (1, x_data)), self.aleatoric_std**2
            )
        return -negative_log_likelihood

    @staticmethod
    def initialize_random_configuration(configuration_shape: tuple) -> np.ndarray:
        """

        Args:
            configuration_shape:

        Returns:

        """
        return np.random.randn(*configuration_shape)

    def compute_local_energy(self, local_configuration: float, neighbors_configurations: tuple) -> float:
        raise NotImplementedError
