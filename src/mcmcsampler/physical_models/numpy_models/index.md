```mermaid
classDiagram
    AbstractPhysicalModel <|-- IsingModel
    AbstractPhysicalModel <|-- LinearGaussianModel
    AbstractPhysicalModel <|-- NoisyPhi4Model
    AbstractPhysicalModel <|-- Phi4Model
    AbstractPhysicalModel <|-- RBM
```

::: mcmcsampler.physical_models.numpy_models.IsingModel
    handler: python
    rendering:
      show_root_heading: true
      show_source: false
      members_order: 'source'
      show_signature: false

::: mcmcsampler.physical_models.numpy_models.Phi4Model
    handler: python
    rendering:
      show_root_heading: true
      show_source: false
      members_order: 'source'
      show_signature: false

Corresponds to the Free Energy of a Restricted Boltzmann Machine.
::: mcmcsampler.physical_models.numpy_models.RBM
    handler: python
    rendering:
      show_root_heading: true
      show_source: false
      members_order: 'source'
      show_signature: false

::: mcmcsampler.physical_models.numpy_models.LinearGaussianModel
    handler: python
    rendering:
      show_root_heading: true
      show_source: false
      members_order: 'source'
      show_signature: false
