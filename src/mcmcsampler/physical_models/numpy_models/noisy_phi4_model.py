import json

import numpy as np

from .phi4_model import Phi4Model


class NoisyPhi4Model(Phi4Model):
    def __init__(
        self,
        chemical_potential: float = 1.0,
        interaction_strength: float = 0.0,
        space_dimensions: int = 2,
        magnetic_configuration: float = 0.0,
        lattice=None,
        energy_noise_std: float = 1.0,
    ):
        super().__init__(chemical_potential, interaction_strength, space_dimensions, magnetic_configuration, lattice)
        self.energy_noise_std = energy_noise_std

    def __repr__(self) -> str:
        dico = json.loads(super().__repr__())
        dico.update({"energy_noise": self.energy_noise_std})
        return json.dumps(dico)

    def compute_total_energies(self, model, size=None):
        return self.compute_total_energy(model) + np.random.normal(loc=0.0, scale=self.energy_noise_std, size=size)
