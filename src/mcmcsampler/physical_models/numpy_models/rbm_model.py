import json

import numpy as np

from mcmcsampler.physical_models.abstract_physical_models import AbstractPhysicalModel


class RBM(AbstractPhysicalModel):
    def __init__(
        self,
        weights: np.ndarray,
        visible_biases: np.array,
        hidden_biases: np.array,
    ):
        """

        Args:
            weights:
            visible_biases:
            hidden_biases:
        """
        self.weights = weights
        self.visible_biases = visible_biases
        self.hidden_biases = hidden_biases

    def __repr__(self) -> str:
        dico = {
            "weights": self.weights,
            "visible_biases": self.visible_biases,
            "hidden_biases": self.hidden_biases,
        }
        return json.dumps(dico)

    @staticmethod
    def initialize_random_configuration(configuration_shape: tuple) -> np.ndarray:
        configuration = np.random.rand(*configuration_shape)
        configuration[configuration < 0.5] = 0
        configuration[configuration >= 0.5] = 1
        return configuration

    def compute_total_energy(self, model: np.array) -> float:
        r"""
        formula:
            $$p(x)=e^{−F(x)/Z}$$

            $$-F(x)=\sum_{i=1}^N a_i x_i + \sum_{j=1}^M \ln(1 + \exp(b_j + \sum_{i=1}^N x_i W_{ij}))$$

        extracted from:
            Huang, Li, and Lei Wang.
            “Accelerated Monte Carlo Simulations with Restricted Boltzmann Machines.”
            Physical Review B 95, no. 3 (January 4, 2017): 035105. https://doi.org/10.1103/PhysRevB.95.035105.
        """
        free_energy = -np.dot(self.visible_biases, model)
        free_energy -= sum(
            np.log(
                [
                    1 + np.exp(self.hidden_biases[i] + np.dot(self.weights[i], model))
                    for i in range(len(self.hidden_biases))
                ]
            )
        )
        return free_energy

    def compute_local_energy(self, local_configuration: float, neighbors_configurations: tuple) -> float:
        raise NotImplementedError
