import numpy as np

from mcmcsampler.physical_models.numpy_models import LinearGaussianModel


class TestLinearGaussianModel:
    @staticmethod
    def test_compute_negative_log_likelihood():
        true_weights = np.asarray([-0.3, 0.5])
        data_samples = [[1.0, 2.0]]
        model = LinearGaussianModel(data_samples=data_samples, aleatoric_std=1.0)
        energy = model.compute_negative_log_likelihood(configuration=true_weights)
        assert energy == -np.log(np.exp(-0.5 * (2.0 - (-0.3 + 0.5 * 1)) ** 2) / np.sqrt(2 * np.pi))

    @staticmethod
    def test_compute_negative_log_likelihood_2():
        true_weights = np.asarray([-0.3, 0.5])
        data_samples = [[1.0, 2.0]]
        model = LinearGaussianModel(data_samples=data_samples, aleatoric_std=2.0)
        energy = model.compute_negative_log_likelihood(configuration=true_weights)
        assert energy == -np.log(np.exp(-0.5 * ((2.0 - (-0.3 + 0.5 * 1)) / 2.0) ** 2) / (2.0 * np.sqrt(2 * np.pi)))

    @staticmethod
    def test_compute_prior_energy():
        true_weights = np.asarray([-0.3, 0.5])
        assert (
            abs(
                LinearGaussianModel.compute_prior_energy(true_weights, chemical_potential=0.7)
                - 0.7 * (0.3**2 + 0.5**2)
            )
            < 1e-10
        )

    @staticmethod
    def test_no_data_compute_prior_energy():
        true_weights = np.asarray([-0.3, 0.5])
        model = LinearGaussianModel(chemical_potential=0.7)
        assert abs(model.compute_total_energy(true_weights) - 0.7 * (0.3**2 + 0.5**2)) < 1e-10

    @staticmethod
    def test_prior_sign_assertion():
        model = LinearGaussianModel(chemical_potential=0.7)
        energy_1 = model.compute_total_energy(np.asarray([-0.3, 0.5]))
        energy_2 = model.compute_total_energy(np.asarray([-1.3, 1.5]))
        assert energy_2 > energy_1

    @staticmethod
    def test_initialize_random_configuration():
        model_size = (2, 2)
        model = LinearGaussianModel()
        random_configuration = model.initialize_random_configuration(model_size)
        assert random_configuration.shape == model_size
