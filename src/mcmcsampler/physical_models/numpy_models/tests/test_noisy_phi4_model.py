import numpy as np

from mcmcsampler.lattices import SquareLattice
from mcmcsampler.physical_models.numpy_models import NoisyPhi4Model, Phi4Model


class TestNoisyPhi4Model:
    @staticmethod
    def test_compute_total_energies():
        configuration_shape = (3, 3)
        energy_noise_std = 2.0
        lattice = SquareLattice()
        physical_model = Phi4Model(lattice=lattice)
        noisy_physical_model = NoisyPhi4Model(lattice=lattice, energy_noise_std=energy_noise_std)
        configuration = physical_model.initialize_random_configuration(configuration_shape)
        total_energies = noisy_physical_model.compute_total_energies(configuration, size=10000)
        mean_total_energy = np.mean(total_energies)
        assert abs(physical_model.compute_total_energy(configuration) - mean_total_energy) < 0.1
        std_total_energy = np.std(total_energies)
        assert abs(std_total_energy - energy_noise_std) / energy_noise_std < 0.05
