import pytest

from mcmcsampler.physical_models.numpy_models import IsingModel


@pytest.mark.parametrize(
    "size",
    [
        (10,),
        (10, 10),
        (10, 10, 10),
    ],
)
def test_ones_configuration(size: tuple):
    configuration = IsingModel.initialize_ones_configuration(configuration_shape=size)
    assert set(configuration.flatten()) == {1}
    assert configuration.shape == size


@pytest.mark.parametrize(
    "size",
    [
        (10,),
        (10, 10),
        (10, 10, 10),
    ],
)
def test_minus_ones_configuration(size: tuple):
    configuration = IsingModel.initialize_minus_ones_configuration(configuration_shape=size)
    assert set(configuration.flatten()) == {-1}
    assert configuration.shape == size


@pytest.mark.parametrize(
    "size",
    [
        (10,),
        (10, 10),
        (10, 10, 10),
    ],
)
def test_random_configuration(size: tuple):
    configuration = IsingModel.initialize_random_configuration(configuration_shape=size)
    assert set(configuration.flatten()) == {1, -1}
    assert configuration.shape == size
