import numpy as np

from mcmcsampler.physical_models.numpy_models import Phi4Model


def test_compute_local_energy():
    model = Phi4Model(
        chemical_potential=float(np.random.rand()),
        interaction_strength=0.0,
        space_dimensions=2,
    )
    assert (
        abs(
            model.chemical_potential_mu
            - model.compute_local_energy(local_configuration=1.0, neighbors_configurations=(1.0, 1.0, 1.0, 1.0))
        )
        < 1e-10
    )

    model = Phi4Model(
        chemical_potential=float(np.random.rand()),
        interaction_strength=0.0,
        space_dimensions=1,
    )
    assert (
        abs(
            model.chemical_potential_mu
            - model.compute_local_energy(local_configuration=1.0, neighbors_configurations=(1.0, 1.0))
        )
        < 1e-10
    )

    model = Phi4Model(
        chemical_potential=float(np.random.rand()),
        interaction_strength=1.0,
        space_dimensions=1,
    )
    assert (
        abs(
            1.0
            + model.chemical_potential_mu
            - model.compute_local_energy(local_configuration=1.0, neighbors_configurations=(1.0, 1.0))
        )
        < 1e-10
    )
