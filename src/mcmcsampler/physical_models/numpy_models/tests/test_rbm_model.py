import numpy as np
import pytest

from mcmcsampler.physical_models.numpy_models import RBM


@pytest.mark.parametrize(
    "configuration_configuration, weights, visible_biases, hidden_biases, expected_probability",
    [
        (
            np.zeros((10,)),
            np.identity(10),
            np.zeros((10,)),
            np.zeros((10,)),
            -10.0 * np.log(2.0),
        ),
        (
            np.ones((10,)),
            np.identity(10),
            np.ones((10,)),
            np.ones((10,)),
            -(10.0 + 10.0 * np.log(1 + np.exp(2.0))),
        ),
        (
            np.ones((10,)),
            np.ones((10, 10)),
            np.ones((10,)),
            np.ones((10,)),
            -(10.0 + 10.0 * np.log(1 + np.exp(11.0))),
        ),
        (
            np.ones((10,)),
            [[1.0 if j == 0 else 0.0 for i in range(10)] for j in range(10)],
            np.ones((10,)),
            np.ones((10,)),
            -(10.0 + np.log(1 + np.exp(11.0)) + 9.0 * np.log(1 + np.exp(1.0))),
        ),
    ],
)
def test_compute_unnormalized_probability(
    configuration_configuration, weights, visible_biases, hidden_biases, expected_probability
):
    assert round(expected_probability, ndigits=5) == round(
        RBM(weights=weights, visible_biases=visible_biases, hidden_biases=hidden_biases,).compute_total_energy(
            model=configuration_configuration,
        ),
        ndigits=5,
    )
