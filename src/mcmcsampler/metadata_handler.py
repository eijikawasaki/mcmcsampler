import json
import os
import socket
from typing import Optional


class MetadataHandler:
    @property
    def metadata_dict(self) -> dict:
        """
        Returns:
            dict: metadata dict
        """
        return json.loads(repr(self))

    @staticmethod
    def write_json_into_file(metadata: dict, file_path: str = "./", file_name: Optional[str] = None):
        """
        Args:
            metadata: dict containing information
            file_path: file path
            file_name: file name (must include '.json' extension)
        """
        if file_name is None:
            file_name = socket.gethostname() + ".json"
        file_name_path = file_path + file_name
        if not os.path.isfile(file_name_path):
            with open(file_name_path, "w") as file:
                json.dump([], file)
            print(f"{file_name_path} file created")
        with open(file_name_path, "r+") as file:
            metadata_list = json.load(file)
            metadata_list.append(metadata)
            file.seek(0)
            json.dump(metadata_list, file, indent=4, sort_keys=True)
