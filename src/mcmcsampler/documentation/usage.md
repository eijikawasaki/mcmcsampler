# Cluster Example

## Single Process

Here is an example of a script that runs a single Monte Carlo walker.

```python
from datetime import timedelta
from timeit import default_timer as timer
import numpy as np
from mcmcsampler.physical_models.numpy_models import IsingModel
from mcmcsampler.lattices import SquareLattice
from mcmcsampler.monte_carlo_samplers.numpy_samplers import WolffSampler
from mcmcsampler.observables import NumpyObservables

physical_model = IsingModel()
initial_configuration = physical_model.initialize_random_configuration((3, 3))
engine = WolffSampler(configuration=initial_configuration,
                      lattice=SquareLattice())

for temperature in [0.1, 1., 10.]:
    metadata = {'temperature': temperature}
    start = timer()
    # burn-in
    _ = engine.sample(total_number_iterations=10 ** 4, temperature=temperature)
    samples = engine.sample(total_number_iterations=10 ** 4,
                            temperature=temperature)
    magnetization = [
        NumpyObservables.compute_average_absolute_magnetization(sample)
        for sample in samples]
    mean_magnetization = np.mean(magnetization)
    sd_magnetization = np.sqrt(np.var(magnetization))
    metadata.update({'mean_magnetization': mean_magnetization,
                     'sd_magnetization': sd_magnetization})
    end = timer()
    metadata.update({
        'computation_time': timedelta(seconds=end - start).total_seconds()})
    metadata.update(engine.metadata_dict)
    engine.write_json_into_file(metadata, file_name='raw_data.json')
```

## Multiple process

Let's consider the same computation using multiple processes.

```python
from datetime import timedelta
from timeit import default_timer as timer
import numpy as np
from mcmcsampler.physical_models.numpy_models import IsingModel
from mcmcsampler.lattices import SquareLattice
from mcmcsampler.monte_carlo_samplers.numpy_samplers import WolffSampler
from mcmcsampler.multi_process_engine import MultiProcessEngine
from mcmcsampler.observables import NumpyObservables

number_of_workers = 4
physical_model = IsingModel()
initial_configurations = [physical_model.initialize_random_configuration((3, 3)) for _ in range(number_of_workers)]
engine = MultiProcessEngine(physical_model=IsingModel(),
                            lattice=SquareLattice(),
                            initial_configurations=initial_configurations,
                            sampling_algorithm_class=WolffSampler,
                            number_of_workers=number_of_workers)

for temperature in [0.1, 1., 10.]:
    metadata = {'temperature': temperature}
    start = timer()
    _ = engine.execute_method_multiprocess(method_name='sample', total_number_iterations=10 ** 4,
                                           temperature=temperature)
    samples = engine.execute_method_multiprocess(method_name='sample', total_number_iterations=10 ** 4,
                                                 temperature=temperature)
    samples = engine.concatenate_multiprocess_outputs(samples)
    magnetization = [NumpyObservables.compute_average_absolute_magnetization(sample) for sample in samples]
    mean_magnetization = np.mean(magnetization)
    sd_magnetization = np.sqrt(np.var(magnetization))
    metadata.update({'mean_magnetization': mean_magnetization, 'sd_magnetization': sd_magnetization})
    end = timer()
    metadata.update({'computation_time': timedelta(seconds=end - start).total_seconds()})
    metadata.update(engine.metadata_dict)
    engine.write_json_into_file(metadata, file_name='raw_data.json')
```

## Multiple process with swap memory

The above script can induce memory problems like

```shell
RuntimeError: unable to mmap 464 bytes from file </torch_87658_715006036_1020>: Cannot allocate memory (12)
```

Using ```SwapMultiProcessEngine``` should solve the problem:

```python
from datetime import timedelta
from timeit import default_timer as timer
import numpy as np
from mcmcsampler.physical_models.numpy_models import IsingModel
from mcmcsampler.lattices import SquareLattice
from mcmcsampler.monte_carlo_samplers.numpy_samplers import WolffSampler
from mcmcsampler.multi_process_engine import SwapMultiProcessEngine
from mcmcsampler.observables import NumpyObservables

number_of_workers = 4
physical_model = IsingModel()
initial_configurations = [physical_model.initialize_random_configuration((3, 3)) for _ in range(number_of_workers)]
engine = SwapMultiProcessEngine(physical_model=IsingModel(),
                                lattice=SquareLattice(),
                                initial_configurations=initial_configurations,
                                sampling_algorithm_class=WolffSampler,
                                number_of_workers=number_of_workers)

for temperature in [0.1, 1., 10.]:
    metadata = {'temperature': temperature}
    start = timer()
    _ = engine.execute_method_multiprocess(method_name='sample_with_swap_memory',
                                           total_number_iterations=10 ** 4, temperature=temperature)
    samples = engine.execute_method_multiprocess(method_name='sample_with_swap_memory',
                                                 total_number_iterations=10 ** 4,
                                                 temperature=temperature)
    samples = engine.concatenate_multiprocess_outputs(samples)
    magnetization = [NumpyObservables.compute_average_absolute_magnetization(sample) for sample in samples]
    mean_magnetization = np.mean(magnetization)
    sd_magnetization = np.sqrt(np.var(magnetization))
    metadata.update({'mean_magnetization': mean_magnetization, 'sd_magnetization': sd_magnetization})
    end = timer()
    metadata.update({'computation_time': timedelta(seconds=end - start).total_seconds()})
    metadata.update(engine.metadata_dict)
    engine.write_json_into_file(metadata, file_name='raw_data.json')
```

## Post-process

Based on the raw data json file, it is possible to extract useful data files such as in the following:

```python
import pandas as pd

columns = ['temperature', 'mean_magnetization', 'sd_magnetization']
df = pd.read_json('raw_data.json')
df.to_csv('post_processed_data.csv',
          columns=columns,
          index=False,
          header=True,
          mode='w',
          sep='\t')
```
