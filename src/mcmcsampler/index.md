# mcmcsampler

## MCMC

Markov chain Monte Carlo (MCMC) methods allow sampling from a probability
distribution.
They are particularly suited for distributions that are defined up to a
normalising factor.
MCMC plays a fundamental role in:

* **Statistical mechanics**: Boltzmann-Gibbs probability sampling
* **Bayesian inference**: Bayes’ theorem, posterior probability sampling

Here is a list of concepts used in the ```mcmcsampler``` library that are useful
in both configurations:

| Statistical mechanics | Bayesian inference                           |
|:----------------------|----------------------------------------------|
| configuration         | statistical parameters                       |
| total energy          | unnormalized negative log posterior          |
| chemical potential    | Gaussian prior's variance, L2 regularization |
| temperature           | empirical Bayes method                       |
| Local sampling        | Gibbs sampling                               |

## Library features

In order to sample a probability distribution, it is required to define several
variables listed in the table below.

| Variable                                    | Statistical mechanics   | Bayesian inference                    |
|---------------------------------------------|:------------------------|---------------------------------------|
| initial_configuration                       | physical configuration  | parameters values                     |
| [monte_carlo_sampler](monte_carlo_samplers) | MCMC sampling algorithm | MCMC sampling algorithm               |
| [physical_model](physical_models)           | Hamiltonian / action    | unnormalized log posterior definition |
| [lattice](lattices)                         | physical lattice        | parameters geometry                   |

These variables are typically instances of a class contained in the library.
```mcmcsampler``` is designed to make it easy for a user to define a new class.
Creating a new inherited class allows to sample a new probability distribution
or sample using a different MCMC algorithm.

```mermaid
classDiagram
    MonteCarloSampler *-- PhysicalModel: composition
    MonteCarloSampler *-- Lattice: composition
    MonteCarloSampler : +configuration
    MonteCarloSampler : +physical_model
    MonteCarloSampler : +lattice
    MonteCarloSampler : +acceptance
    MonteCarloSampler: +sample()
    class PhysicalModel{
      +chemical_potential
      +compute_total_energy()
    }
    class Lattice{
      +compute_random_coordinate()
    }
```

*Example:*

```python
from mcmcsampler.lattices import SquareLattice
from mcmcsampler.monte_carlo_samplers.numpy_samplers import WolffSampler
from mcmcsampler.physical_models.numpy_models import IsingModel

configuration_shape = (3, 3)
physical_model = IsingModel()
initial_configuration = physical_model.initialize_random_configuration(
    configuration_shape=configuration_shape)
lattice = SquareLattice()

monte_carlo_sampler = WolffSampler(configuration=initial_configuration,
                                   physical_model=physical_model,
                                   lattice=lattice)
print(monte_carlo_sampler.metadata_dict)
monte_carlo_sampler.sample()
```

## Source code architecture

```mermaid
graph TD
  lattices-->observables
  lattices-->physical_models
  lattices-->monte_carlo_samplers
  physical_models-->monte_carlo_samplers
  monte_carlo_samplers-->multi_process_engine
  monte_carlo_samplers-->id1[(samples)]
  multi_process_engine-->id1[(samples)]
  observables-->id1[(samples)]
  observables-->monte_carlo_samplers
```

- **monte_carlo_samplers**: Sampling algorithms like Metropolis-Hasting.
- **physical_models**: Ising model, phi4, neural networks.
- **observables**: this module computes standard observables like magnetization.
 - **lattices**: lattice definition: geometry, neighbouring sites, boundary
  conditions...
- **multi_process_engine**: toolbox for multi process computations.
