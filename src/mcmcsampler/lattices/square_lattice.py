import functools
from typing import List

import numpy as np
from numba import jit

from .abstract_lattice import AbstractLattice


class SquareLattice(AbstractLattice):
    @staticmethod
    def compute_random_coordinate(configuration_shape: tuple, indices: list) -> tuple:
        """

        Args:
            configuration_shape:
            indices:

        Returns:
            random_coordinate:
        """
        random_index = np.random.choice(configuration_shape)
        random_coordinate = indices[random_index]
        return random_coordinate

    @staticmethod
    @jit(nopython=True)
    def get_indices(configuration_shape: tuple) -> list:
        """

        Args:
            configuration_shape:

        Returns:

        """
        return list(np.ndindex(*np.ones(configuration_shape).shape))

    @staticmethod
    def apply_periodic_boundary_conditions(configuration_shape: tuple, index: tuple) -> tuple:
        """
        Args:
            configuration_shape: configuration size
            index: coordinates
        Returns:
            index: coordinates with pbc
        """
        return tuple(((i % s + s) % s for (i, s) in zip(index, configuration_shape)))

    @staticmethod
    @functools.lru_cache()
    def compute_closest_neighbors(configuration_shape: tuple, coordinates: tuple) -> frozenset:
        """

        Args:
            configuration_shape: configuration_shape
            coordinates: coordinates

        Returns:
            closest_neighbors: closest_neighbors
        """
        next_neighbors = SquareLattice._compute_all_neighbors(configuration_shape, coordinates, step=1)
        previous_neighbors = SquareLattice._compute_all_neighbors(configuration_shape, coordinates, step=-1)
        closest_neighbors = frozenset(next_neighbors + previous_neighbors)
        assert len(closest_neighbors) == len(configuration_shape) * 2
        return closest_neighbors

    @staticmethod
    def _compute_all_neighbors(configuration_shape, coordinates: tuple, step: int) -> List[tuple]:
        neighbors = [coordinates for _ in coordinates]
        for index, neighbor in enumerate(neighbors):
            updated_neighbor = SquareLattice.compute_neighbor_according_to_step(index, neighbor, step)
            neighbors[index] = SquareLattice.apply_periodic_boundary_conditions(configuration_shape, updated_neighbor)
        return neighbors

    @staticmethod
    def compute_neighbor_according_to_step(index: int, neighbor: tuple, step: int) -> tuple:
        updated_neighbor_list = list(neighbor)
        updated_neighbor_list[index] = updated_neighbor_list[index] + step
        updated_neighbor = tuple(updated_neighbor_list)
        return updated_neighbor

    @staticmethod
    @functools.lru_cache()
    def compute_distance(configuration_shape: tuple, first_coordinates: tuple, second_coordinates: tuple) -> float:
        """
        Args:
            configuration_shape: configuration_shape
            first_coordinates: first_coordinates
            second_coordinates: second_coordinates

        Returns:
            distance: distance
        """
        euclidean_distance = []
        for fist_coordinate, second_coordinate, system_size in zip(
            first_coordinates, second_coordinates, configuration_shape
        ):
            single_coordinate_distance = abs(fist_coordinate - second_coordinate)
            if single_coordinate_distance > system_size / 2:
                single_coordinate_distance = system_size - single_coordinate_distance
            euclidean_distance.append(single_coordinate_distance)
        return np.linalg.norm(euclidean_distance)
