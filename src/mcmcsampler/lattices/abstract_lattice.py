import json
from abc import ABC


class AbstractLattice(ABC):
    def __repr__(self):
        dico = {"lattice": type(self).__name__}
        return json.dumps(dico)
