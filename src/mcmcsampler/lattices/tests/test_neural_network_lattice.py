from collections import OrderedDict

import numpy as np
import torch
from torch import nn

from mcmcsampler.lattices import NeuralNetworkLattice


class MLP(nn.Module):
    def __init__(self, input_dim, hidden_dim, output_dim):
        super().__init__()
        self.layers = nn.Sequential(
            nn.Linear(input_dim, hidden_dim),
            nn.ReLU(),
            nn.Linear(hidden_dim, output_dim),
        )

    def forward(self, input_data):
        prediction = self.layers(input_data)
        return prediction


class TestNeuralNetworkLattice:
    @staticmethod
    def test_model_is_expected_state_dict():
        torch.manual_seed(seed=0)
        model = MLP(1, 1, 1).state_dict()
        expected_state_dict = OrderedDict(
            [
                ("layers.0.weight", torch.Tensor([[-0.0075]])),
                ("layers.0.bias", torch.Tensor([0.5364])),
                ("layers.2.weight", torch.Tensor([[-0.8230]])),
                ("layers.2.bias", torch.Tensor([-0.7359])),
            ]
        )
        for ((state_1_name, state_1_value), (state_2_name, state_2_value)) in zip(
            model.items(), expected_state_dict.items()
        ):
            assert state_1_name == state_2_name
            assert torch.isclose(state_1_value, state_2_value, atol=1e-4)

    @staticmethod
    def test_get_weights_names_and_relative_sizes():
        model = MLP(1, 1, 1).state_dict()
        weights_list = NeuralNetworkLattice._get_weights_names_and_relative_sizes(model)
        layer_name, weights_relative_size = weights_list
        assert layer_name == [
            "layers.0.weight",
            "layers.0.bias",
            "layers.2.weight",
            "layers.2.bias",
        ]
        np.testing.assert_equal(weights_relative_size, [0.25, 0.25, 0.25, 0.25])

    @staticmethod
    def test_select_random_weight_layer():
        layers_list = [
            "layers.0.weight",
            "layers.0.bias",
            "layers.2.weight",
            "layers.2.bias",
        ]
        probabilities = [0.25, 0.25, 0.25, 0.25]
        random_list = []
        for _ in range(100):
            random_layer_name = NeuralNetworkLattice._select_random_weight_layer(layers_list, probabilities)
            random_list.append(random_layer_name)
        random_list = np.asarray(random_list)
        assert len(np.unique(random_list)) == 4

    @staticmethod
    def test_compute_random_weight_index():
        model = MLP(1, 10, 1).state_dict()
        random_list = []
        for _ in range(100):
            random_index = NeuralNetworkLattice._compute_random_weight_index(model, "layers.2.weight")
            random_list.append(random_index)
        random_list = np.asarray(random_list)
        assert len(np.unique(random_list)) == 10

    @staticmethod
    def test_compute_random_coordinate():
        model = MLP(1, 1, 1).state_dict()
        random_list = []
        for _ in range(100):
            (
                random_layer,
                random_index,
            ) = NeuralNetworkLattice.compute_random_coordinate(model)
            random_list.append((random_layer, *random_index))
        random_list = np.asarray(random_list, dtype=object)
        assert len(np.unique(random_list)) == 4
