import numpy as np
import pytest

from mcmcsampler.lattices import SquareLattice


@pytest.mark.parametrize(
    "index, expected",
    [
        ((9,), (9,)),
        ((10,), (0,)),
        ((-1,), (9,)),
    ],
)
def test_1d_periodic_boundary_conditions(index, expected):
    assert SquareLattice.apply_periodic_boundary_conditions((10,), index) == expected


@pytest.mark.parametrize(
    "index, expected",
    [
        ((9, 9), (9, 9)),
        ((10, 9), (0, 9)),
        ((9, 10), (9, 0)),
        ((-1, -1), (9, 9)),
    ],
)
def test_2d_periodic_boundary_conditions(index, expected):
    assert SquareLattice.apply_periodic_boundary_conditions((10, 10), index) == expected


def test_create_1d_neighbors():
    coordinates = (5,)
    assert SquareLattice.compute_closest_neighbors((10,), coordinates) == frozenset([(4,), (6,)])


@pytest.mark.parametrize(
    "coordinates, configuration_shape, neighbors",
    [
        ((5, 5), (10, 10), frozenset([(4, 5), (6, 5), (5, 4), (5, 6)])),
        ((0,), (3,), frozenset({(2,), (1,)})),
        ((0, 0), (3, 3), frozenset({(0, 1), (2, 0), (1, 0), (0, 2)})),
    ],
)
def test_create_2d_neighbors(coordinates, configuration_shape, neighbors):
    assert SquareLattice.compute_closest_neighbors(configuration_shape, coordinates) == neighbors


@pytest.mark.parametrize(
    "configuration_shape, first_coordinate, second_coordinate, expected",
    [
        ((1,), (1,), (1,), 0.0),
        ((5,), (1,), (4,), 2.0),
        ((5,), (0,), (4,), 1.0),
        ((5, 5), (0, 0), (1, 1), np.sqrt(2)),
        ((5, 5), (0, 0), (4, 4), np.sqrt(2)),
    ],
)
def test_compute_distance(configuration_shape, first_coordinate, second_coordinate, expected):
    assert SquareLattice.compute_distance(configuration_shape, first_coordinate, second_coordinate) == expected


def test_get_indices():
    indices = SquareLattice.get_indices((2, 2))
    assert indices == [(0, 0), (0, 1), (1, 0), (1, 1)]
