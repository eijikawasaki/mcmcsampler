```mermaid
classDiagram
    AbstractLattice <|-- NeuralNetworkLattice
    AbstractLattice <|-- SquareLattice
    class NeuralNetworkLattice{
      +compute_random_coordinate()
    }
    class SquareLattice{
      +compute_random_coordinate()
      +compute_distance()
    }
```

::: mcmcsampler.lattices.SquareLattice
    handler: python
    rendering:
      show_root_heading: true
      show_source: false
      members_order: 'source'
      show_signature: false

::: mcmcsampler.lattices.NeuralNetworkLattice
    handler: python
    rendering:
      show_root_heading: true
      show_source: false
      members_order: 'source'
      show_signature: false
