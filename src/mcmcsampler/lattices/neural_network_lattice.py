from typing import Tuple

import numpy as np

from .abstract_lattice import AbstractLattice


class NeuralNetworkLattice(AbstractLattice):
    @staticmethod
    def compute_random_coordinate(
        configuration: dict,
    ) -> Tuple[str, tuple]:
        """
        Args:
            configuration: torch NN

        Returns:
            random_layer: random_layer
            random_index: random_index
        """
        (
            layers_list,
            probabilities,
        ) = NeuralNetworkLattice._get_weights_names_and_relative_sizes(configuration)
        random_layer_name = NeuralNetworkLattice._select_random_weight_layer(layers_list, probabilities)
        random_weight_index = NeuralNetworkLattice._compute_random_weight_index(configuration, random_layer_name)
        return random_layer_name, random_weight_index

    @staticmethod
    def _select_random_weight_layer(names_list: list, probabilities: list) -> str:
        random_layer_index = np.random.choice(a=len(names_list), size=1, p=probabilities, replace=False)
        random_layer_name = names_list[int(random_layer_index)]
        return random_layer_name

    @staticmethod
    def _compute_random_weight_index(configuration: dict, target_name: str) -> tuple:
        target_shape = NeuralNetworkLattice._get_target_shape(configuration, target_name)
        random_index = tuple(np.random.randint(low=0, high=size) for size in target_shape)
        return random_index

    @staticmethod
    def _get_weights_names_and_relative_sizes(
        configuration: dict,
    ) -> Tuple[list, np.array]:
        """
        Args:
            configuration: torch NN

        Returns:
            names_list: names_list
            probabilities: probabilities
        """
        names_list = list(configuration.keys())
        number_of_parameters_list = [layer_content.numpy().size for layer_content in configuration.values()]
        number_of_parameters_list = np.asarray(number_of_parameters_list)
        probabilities = number_of_parameters_list / np.sum(number_of_parameters_list)
        return names_list, probabilities

    @staticmethod
    def _get_target_shape(models_parameters: dict, target_name: str) -> tuple:
        target_content = models_parameters.get(target_name)
        assert target_content is not None
        target_shape = target_content.shape
        return target_shape
