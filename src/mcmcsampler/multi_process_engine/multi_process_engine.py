import copy
import json
from typing import Any, Callable, List, Optional, Type

import numpy as np
import torch.multiprocessing as multiprocessing
from torch.multiprocessing import Pool

from ..lattices import AbstractLattice
from ..metadata_handler import MetadataHandler
from ..monte_carlo_samplers import AbstractMonteCarloSampler
from ..physical_models import AbstractPhysicalModel
from .single_simulation_engine import SingleSimulationEngine


class MultiProcessEngine(SingleSimulationEngine, MetadataHandler):
    """
    https://pytorch.org/docs/stable/multiprocessing.html
    https://pytorch.org/docs/stable/notes/multiprocessing.html
    """

    def __init__(
        self,
        sampling_algorithm_class: Type[AbstractMonteCarloSampler],
        initial_configurations: list,
        physical_model: AbstractPhysicalModel,
        lattice: AbstractLattice,
        number_of_processes: Optional[int] = None,
        is_progress_bar_disable: bool = False,
    ):
        """
        Args:
            sampling_algorithm_class: MCMC sampling algorithm class
            initial_configurations: initial configuration
            physical_model: physical configuration instance
            lattice: lattice instance
            number_of_processes: number of processes
        """
        self.workers_list: List[AbstractMonteCarloSampler] = [
            MultiProcessEngine.create_sampling_algorithm_instance(
                sampling_algorithm_class,
                initial_configuration,
                physical_model=physical_model,
                lattice=lattice,
                is_progress_bar_disable=is_progress_bar_disable,
            )
            for initial_configuration in initial_configurations
        ]
        self.number_of_processes = number_of_processes if number_of_processes is not None else len(self.workers_list)

    def __repr__(self) -> str:
        dico = json.loads(self.workers_list[0].__repr__())
        dico.update({"number_of_workers": len(self.workers_list)})
        return json.dumps(dico)

    def execute_method_multiprocess(self, method_name: str = "sample", **kwargs: Any) -> np.array:
        """
        Ressources:
            https://stackoverflow.com/questions/8533318/multiprocessing-pool-when-to-use-apply-apply-async-or-map

        Args:
            method_name: 'configuration', 'train', 'sample_generative_model', 'build_train_set',
            'train_generative_model_over_monte_carlo_samples'
            **kwargs: kwargs for method_name
        Returns:
            output: numpy array of the results
        """
        pool = multiprocessing.get_context("spawn").Pool(processes=self.number_of_processes)
        arguments = [(getattr(engine, method_name), kwargs) for engine in self.workers_list]
        results = pool.starmap(MultiProcessEngine._execute_method_single_worker, arguments)
        pool.close()
        pool.join()
        results = np.asarray(results, dtype=object)
        sampling_algorithms, output = results[:, 0], results[:, 1]
        self.workers_list = sampling_algorithms
        return output

    @staticmethod
    def _execute_method_single_worker(sampling_algorithm_method, kwargs):
        sampling_algorithm = sampling_algorithm_method.__self__
        seed = MultiProcessEngine._compute_worker_specific_seed(sampling_algorithm)
        MultiProcessEngine.set_seeds(seed)
        output = sampling_algorithm_method(**kwargs)
        return sampling_algorithm, output

    @staticmethod
    def concatenate_multiprocess_outputs(outputs: list, do_shuffle: bool = False) -> np.ndarray:
        """
        Args:
            outputs: list of single process outputs
            do_shuffle: shuffles the  concatenated output

        Returns:
            concatenate_output: concatenated outputs
        """
        # TODO: write test
        concatenate_output = []
        for output in outputs:
            concatenate_output.extend(output)
            del output
        concatenate_output = np.asarray(concatenate_output)
        if do_shuffle:
            np.random.shuffle(concatenate_output)
        return concatenate_output

    def set_physical_model_multiprocess(self, physical_model: AbstractPhysicalModel) -> None:
        """
        Warning:
            Workers are static copies, updating the physical configuration reference without using this method will thus
            leave the workers' physical parameters unchanged.

        Args:
            physical_model:
        """
        for worker in self.workers_list:
            worker.physical_model = copy.deepcopy(physical_model)

    def execute_function_multiprocess(self, function: Callable, argument: np.array) -> list:
        """
        Method designed to parallelize computations over samples. This solves a bug in the multiprocess pytorch module.

        Args:
            function: target function
            argument: to be fed to the target function

        Returns:
            results: outcome of function(argument)
        """
        with Pool(self.number_of_processes) as pool:
            results = pool.map(function, argument)
        return results
