```mermaid
classDiagram
    SingleSimulationEngine <|-- MultiProcessEngine
    MultiProcessEngine <|-- SwapMultiProcessEngine
    class SingleSimulationEngine{
        +create_sampling_algorithm_instance()
    }
    class MultiProcessEngine{
        +sampling_algorithm_class
        +physical_model
        +lattice
        +execute_method_multiprocess()
        +concatenate_multiprocess_outputs()
    }
    class SwapMultiProcessEngine{
        +execute_method_multiprocess()
    }
```
!!! warning "Configurations as distinct objects."

    ```initial_configurations``` should be variables with no link in memory.
    Avoid configurations copies using ```from copy.deepcopy``` as it may produce unpredictable effects.

::: mcmcsampler.multi_process_engine.MultiProcessEngine
    handler: python
    rendering:
      show_root_heading: true
      show_source: false
      members_order: 'source'
      show_signature: false

::: mcmcsampler.multi_process_engine.SingleSimulationEngine
    handler: python
    rendering:
      show_root_heading: true
      show_source: false
      members_order: 'source'
      show_signature: false

::: mcmcsampler.multi_process_engine.SwapMultiProcessEngine
    handler: python
    rendering:
      show_root_heading: true
      show_source: false
      members_order: 'source'
      show_signature: false

# Pytorch multiprocessing

Pytorch has its own multiprocessing wrapper around the
native ```multiprocessing``` module.

See: https://pytorch.org/docs/stable/multiprocessing.html

The file ```multi_process_engine.py``` is set
to ```import torch.multiprocessing```
instead of ```import multiprocessing```.
It is also useful to set a different sharing strategy:

```python
import torch.multiprocessing

torch.multiprocessing.set_sharing_strategy('file_system')
```

to avoid errors like

```python
raise RuntimeError('received %d items of ancdata' % ...)
```

In this latter case be careful about memory leaks,
see: https://pytorch.org/docs/stable/multiprocessing.html#file-system-file-system

# Additional tips

> **Avoid calling torch.nn.Module outside multiprocess.**
