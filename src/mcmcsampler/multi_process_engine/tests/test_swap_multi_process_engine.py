import numpy as np
import pytest
import torch
from torch.utils.data import DataLoader, TensorDataset

from mcmcsampler.lattices import NeuralNetworkLattice
from mcmcsampler.monte_carlo_samplers.torch_samplers import NeuralNetworkRandomWalkSampler
from mcmcsampler.multi_process_engine import SwapMultiProcessEngine
from mcmcsampler.physical_models.torch_models import NeuralNetworkModel
from mcmcsampler.tests.mlp import MLPTrainer


class TestSwapMultiProcessEngine:
    @staticmethod
    @pytest.mark.skip(reason="CI OSError: [Errno 28] No space left on device")
    def test_too_many_big_samples_raises_error_and_requires_swap_memory():
        """
        Error raises depending on the hardware...
        """
        module = MLPTrainer(input_dim=1, hidden_dim=tuple(10 for _ in range(10)), output_dim=1)
        initial_configuration = module.state_dict()
        train_input_data = torch.zeros((10, 1))
        train_output_data = torch.zeros((10, 1))
        dataset = TensorDataset(train_input_data, train_output_data)
        loader = DataLoader(dataset)
        nn_model = NeuralNetworkModel(loader, module)
        nn_lattice = NeuralNetworkLattice()
        engine = SwapMultiProcessEngine(
            physical_model=nn_model,
            lattice=nn_lattice,
            initial_configurations=[initial_configuration for _ in range(2)],
            sampling_algorithm_class=NeuralNetworkRandomWalkSampler,
            is_progress_bar_disable=True,
        )
        with pytest.raises(Exception):
            engine.execute_method_multiprocess(
                "sample",
                total_number_iterations=10**4,
                record_step=1,
            )
        samples = engine.execute_method_multiprocess(
            "sample_with_swap_memory",
            total_number_iterations=10**4,
            record_step=1,
        )
        assert np.shape(samples) == (2, 10**4)
