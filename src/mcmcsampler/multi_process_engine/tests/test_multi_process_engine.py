import numpy as np
import pytest

from mcmcsampler.lattices import SquareLattice
from mcmcsampler.monte_carlo_samplers.numpy_samplers import GlobalEnergySampler, LocalEnergySampler
from mcmcsampler.multi_process_engine import MultiProcessEngine
from mcmcsampler.observables import NumpyObservables
from mcmcsampler.physical_models.numpy_models import Phi4Model


class TestMultiProcessEngine:
    @staticmethod
    def test_multi_process_configurations_are_not_linked():
        phi4_model = Phi4Model()
        square_lattice = SquareLattice()
        number_of_workers = 2
        initial_configuration = phi4_model.initialize_random_configuration(configuration_shape=(3,))
        initial_configurations = [np.copy(initial_configuration) for _ in range(number_of_workers)]
        engine = MultiProcessEngine(
            physical_model=phi4_model,
            lattice=square_lattice,
            initial_configurations=initial_configurations,
            sampling_algorithm_class=GlobalEnergySampler,
        )
        assert engine.workers_list[0].configuration[0] == engine.workers_list[1].configuration[0]
        assert engine.workers_list[0].configuration[0] != 0
        engine.workers_list[0].configuration[0] = 0.0
        assert engine.workers_list[0].configuration[0] == 0
        assert engine.workers_list[1].configuration[0] != 0

    @staticmethod
    def test_multiprocess_sample_is_accepted_and_rejected():
        square_lattice = SquareLattice()
        phi4_model = Phi4Model(lattice=square_lattice)
        initial_configuration = phi4_model.initialize_random_configuration(configuration_shape=(3,))
        number_of_workers = 10
        initial_configurations = [initial_configuration for _ in range(number_of_workers)]
        engine = MultiProcessEngine(
            physical_model=phi4_model,
            lattice=square_lattice,
            initial_configurations=initial_configurations,
            sampling_algorithm_class=GlobalEnergySampler,
        )
        for worker in engine.workers_list:
            np.testing.assert_allclose(worker.configuration, initial_configuration)
        samples_multiprocess = engine.execute_method_multiprocess(total_number_iterations=1)

        configuration_is_updated = False
        for sample in samples_multiprocess:
            if not np.allclose(sample, initial_configuration):
                configuration_is_updated = True
        assert configuration_is_updated

        is_update_rejected = False
        for sample in samples_multiprocess:
            if np.allclose(sample, initial_configuration):
                is_update_rejected = True
        assert is_update_rejected

    @staticmethod
    def test_multiprocess_sample_all_configurations_differently():
        square_lattice = SquareLattice()
        phi4_model = Phi4Model(lattice=square_lattice)
        initial_configuration = phi4_model.initialize_random_configuration(configuration_shape=(3,))
        number_of_workers = 10
        initial_configurations = [initial_configuration for _ in range(number_of_workers)]
        engine = MultiProcessEngine(
            physical_model=phi4_model,
            lattice=square_lattice,
            initial_configurations=initial_configurations,
            sampling_algorithm_class=GlobalEnergySampler,
        )
        for worker in engine.workers_list:
            np.testing.assert_allclose(worker.configuration, initial_configuration)
        _ = engine.execute_method_multiprocess(
            method_name="sample",
            total_number_iterations=10**2,
        )

        for worker in engine.workers_list:
            with pytest.raises(AssertionError):
                np.testing.assert_allclose(worker.configuration, initial_configuration)

    @staticmethod
    def test_sample_multiprocess_is_same_as_single_sample():
        phi4_model = Phi4Model(chemical_potential=-5.0, interaction_strength=1.0, space_dimensions=2)
        square_lattice = SquareLattice()
        number_of_workers = 4
        initial_configurations = [
            phi4_model.initialize_random_configuration(configuration_shape=(3,)) for _ in range(number_of_workers)
        ]
        multi_process_engine = MultiProcessEngine(
            physical_model=phi4_model,
            lattice=square_lattice,
            initial_configurations=initial_configurations,
            sampling_algorithm_class=LocalEnergySampler,
        )
        single_process_engine = LocalEnergySampler(
            physical_model=phi4_model,
            lattice=square_lattice,
            configuration=initial_configurations[0],
            is_progress_bar_disable=True,
        )
        for chemical_potential_mu in (5.0, 0.0, -5.0):
            single_process_engine.physical_model.chemical_potential_mu = chemical_potential_mu
            phi4_model.chemical_potential_mu = chemical_potential_mu
            multi_process_engine.set_physical_model_multiprocess(physical_model=phi4_model)

            for _ in range(5):
                samples = single_process_engine.sample(total_number_iterations=10**2)
            samples = single_process_engine.sample(total_number_iterations=10**4)
            samples = np.asarray(samples)
            single_average_density = NumpyObservables.compute_average_density(samples)
            # print('single', single_average_density)

            for _ in range(5):
                samples_multiprocess = multi_process_engine.execute_method_multiprocess(
                    method_name="sample",
                    total_number_iterations=10**2,
                )
            samples_multiprocess = multi_process_engine.execute_method_multiprocess(
                method_name="sample",
                total_number_iterations=10**3,
            )
            average_density = []
            for samples in samples_multiprocess:
                samples = np.asarray(samples)
                average_density.append(NumpyObservables.compute_average_density(samples))
            multi_average_density = np.mean(average_density)
            # print('multi', multi_average_density)
            assert (
                abs(single_average_density - multi_average_density) / (single_average_density + multi_average_density)
                < 1e-1
            )
