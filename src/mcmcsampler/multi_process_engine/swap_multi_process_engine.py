import os
from typing import Any

import numpy as np
import torch
import torch.multiprocessing as multiprocessing

from .multi_process_engine import MultiProcessEngine


class SwapMultiProcessEngine(MultiProcessEngine):
    def execute_method_multiprocess(self, method_name: str = "sample_with_swap_memory", **kwargs: Any) -> np.array:
        """
        Pickles output of single workers and retrieve them after the multiprocess pool is closed.
        Based on pytorch ```save``` and ```load``` methods:
        https://pytorch.org/tutorials/beginner/saving_loading_models.html

        Args:
            method_name: 'configuration', 'train', 'sample_generative_model', 'build_train_set'
            **kwargs: kwargs for method_name

        Returns:
            output: numpy array of the results
        """
        pool = multiprocessing.Pool(processes=self.number_of_processes)
        arguments = [(getattr(engine, method_name), kwargs) for engine in self.workers_list]
        sampling_algorithms = pool.starmap(SwapMultiProcessEngine._execute_method_single_worker, arguments)
        pool.close()
        pool.join()
        self.workers_list = sampling_algorithms
        output = [torch.load(worker.temporary_file_name) for worker in self.workers_list]
        for worker in self.workers_list:
            os.remove(worker.temporary_file_name)
        return output

    @staticmethod
    def _execute_method_single_worker(sampling_algorithm_method, kwargs):
        seed = SwapMultiProcessEngine._compute_worker_specific_seed(sampling_algorithm_method.__self__)
        SwapMultiProcessEngine.set_seeds(seed)
        sampling_algorithm = sampling_algorithm_method(**kwargs)
        return sampling_algorithm
