import random
from typing import Type, Union

import numpy as np
import torch

from ..lattices import AbstractLattice
from ..monte_carlo_samplers import AbstractMonteCarloSampler
from ..physical_models import AbstractPhysicalModel


class SingleSimulationEngine:
    @staticmethod
    def create_sampling_algorithm_instance(
        sampling_algorithm_class: Type[AbstractMonteCarloSampler],
        initial_configuration: object,
        physical_model: AbstractPhysicalModel,
        lattice: AbstractLattice,
        is_progress_bar_disable: bool,
    ) -> Union[AbstractMonteCarloSampler]:
        """
        Args:
            sampling_algorithm_class: MCMC sampling algorithm class
            initial_configuration: initial configuration
            physical_model: physical_model
            lattice: lattice,
            is_progress_bar_disable:

        Returns:
            sampling_algorithm: sampling_algorithm
        """
        sampling_algorithm = sampling_algorithm_class(
            configuration=initial_configuration,
            physical_model=physical_model,
            lattice=lattice,
            is_progress_bar_disable=is_progress_bar_disable,
        )
        return sampling_algorithm

    @staticmethod
    def set_seeds(seed):
        np.random.seed(seed)
        torch.manual_seed(seed)
        random.seed(seed)

    @staticmethod
    def _compute_worker_specific_seed(sampling_algorithm) -> int:
        seed = hash(sampling_algorithm)
        seed = int(str(seed)[-5:])
        return seed
