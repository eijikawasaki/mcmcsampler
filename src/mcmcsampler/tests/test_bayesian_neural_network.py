import numpy as np
import torch
from torch import nn
from torch.utils.data import DataLoader, TensorDataset
from tqdm import trange

from mcmcsampler.lattices import NeuralNetworkLattice
from mcmcsampler.monte_carlo_samplers.torch_samplers import NeuralNetworkRandomWalkSampler
from mcmcsampler.physical_models.torch_models import NeuralNetworkModel


class MLPTrainer(nn.Module):
    def __init__(self):
        super().__init__()
        self.loss_func = nn.MSELoss()
        self.model = MLP()

    def compute_negative_log_likelihood(self, input_data, output_data):
        y_pred = self.model(input_data)
        loss = self.loss_func(y_pred, output_data)
        return loss

    def do_training(self, data_loader, number_of_epochs, weight_decay):
        optimizer = torch.optim.Adam(self.parameters(), lr=1e-2, weight_decay=weight_decay)
        progress_bar = trange(number_of_epochs)
        self.model.train()
        for epoch in progress_bar:
            self.train()
            for data in data_loader:
                x_input, y_output = data
                optimizer.zero_grad()
                output = self.model(x_input)
                loss = self.loss_func(output, y_output)
                loss.backward()
                optimizer.step()
                progress_bar.set_postfix({"loss: ": loss})

    def forward(self, input_data):
        return self.model(input_data)


class MLP(nn.Module):
    def __init__(self):
        super().__init__()
        self.layers = nn.Sequential(nn.Linear(1, 1, bias=True))

    def forward(self, input_data):
        prediction = self.layers(input_data)
        return prediction


class TestBayesianNeuralNetwork:
    @staticmethod
    def test_mcmc_mle_is_gradient_descent_mle():
        weight_decay = 10.0
        number_of_epochs = 10**3
        train_input_data = torch.randn((10, 1, 1))
        train_output_data = torch.randn((10, 1, 1))
        dataset = TensorDataset(train_input_data, train_output_data)
        data_loader = DataLoader(dataset=dataset, batch_size=len(train_input_data), shuffle=True)
        # Gradient Descent
        gradient_descent_mle = MLPTrainer()
        gradient_descent_mle.do_training(
            data_loader=data_loader, number_of_epochs=number_of_epochs, weight_decay=weight_decay
        )
        expected_state_dict = gradient_descent_mle.state_dict()
        # MCMC MLE
        mcmc_mle = MLPTrainer()
        configuration = mcmc_mle.state_dict()
        physical_model = NeuralNetworkModel(data_loader=data_loader, module=mcmc_mle, weight_decay=weight_decay)
        lattice = NeuralNetworkLattice()
        mc_sampler = NeuralNetworkRandomWalkSampler(
            configuration=configuration, physical_model=physical_model, lattice=lattice
        )
        samples = mc_sampler.sample(
            temperature=1e-10,
            total_number_iterations=10**4,
            record_step=10**4 // 10,
            random_walk_step_size=1e-3,
        )
        for parameter_name in configuration:
            assert abs(float(samples[-1].get(parameter_name)) - float(expected_state_dict.get(parameter_name)) < 1e-4)

        gradient_descent_loss = gradient_descent_mle.compute_negative_log_likelihood(
            train_input_data, train_output_data
        )
        mcmc_loss = physical_model.compute_total_energy(samples[-1])
        assert abs(float(gradient_descent_loss) - float(mcmc_loss)) / abs(mcmc_loss) < 0.5

        predictor = MLPTrainer()
        predictor.load_state_dict(samples[-1])
        mcmc_predictions = predictor(train_input_data)
        mcmc_predictions = mcmc_predictions.detach().numpy()
        mle_predictions = gradient_descent_mle(train_input_data)
        mle_predictions = mle_predictions.detach().numpy()
        assert np.allclose(mle_predictions, mcmc_predictions, atol=1e-1)
