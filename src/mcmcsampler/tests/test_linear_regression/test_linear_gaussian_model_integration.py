import numpy as np
import numpy_exact_linear_regressor

from mcmcsampler.monte_carlo_samplers.numpy_samplers import GlobalNumpySampler
from mcmcsampler.physical_models.numpy_models import LinearGaussianModel


class TestLinearGaussianModelIntegration:
    @staticmethod
    def test_mcmc_posterior_sampling_match_exact_solution():
        true_weights = np.asarray([-0.3, 0.5])
        aleatoric_std = 0.02
        number_of_samples = 3
        chemical_potential = 0.25

        data_samples = list(numpy_exact_linear_regressor.sample_data(number_of_samples, true_weights, aleatoric_std))
        full_xs = [np.array([1, i]) for i in np.linspace(-3.0, 3.0, 100)]
        xs = np.asarray(full_xs)[:, 1]

        # MCMC
        physical_model = LinearGaussianModel(data_samples, aleatoric_std, chemical_potential)
        samples = TestLinearGaussianModelIntegration.compute_samples(physical_model, true_weights)
        mus, stds = TestLinearGaussianModelIntegration.compute_predictions(full_xs, physical_model, samples)

        # Exact
        model = numpy_exact_linear_regressor.BayesianLinearRegressor(
            n_features=2, weigth_decay=chemical_potential, aleatoric_std=aleatoric_std
        )
        for xi, yi in data_samples:
            xi = np.asarray([1, xi])
            model.learn(xi, yi)
        predictions = np.asarray([model.predict(np.array([1, xi])) for xi in xs])
        exact_mus, exact_stds = predictions[:, 0], predictions[:, 1]

        for expected_mu, empirical_mu in zip(exact_mus, mus):
            assert abs(expected_mu - empirical_mu) < 0.1

        for expected_std, empirical_std in zip(exact_stds, stds):
            assert abs(expected_std - empirical_std) / expected_std < 0.5

    @staticmethod
    def compute_predictions(full_xs, physical_model, samples):
        mus = []
        stds = []
        for x in full_xs:
            mean_mu = 0.0
            y_samples = []
            for sample in samples:
                single_mean = np.dot(sample, x)
                mean_mu += single_mean
                y_samples.append(np.random.normal(single_mean, physical_model.aleatoric_std, 1000))
            stds.append(np.std(y_samples))
            mean_mu /= len(samples)
            mus.append(mean_mu)
        mus = np.asarray(mus)
        stds = np.asarray(stds)
        return mus, stds

    @staticmethod
    def compute_samples(physical_model, true_weights):
        random_walk_step_size = 0.01
        engine = GlobalNumpySampler(
            configuration=physical_model.initialize_random_configuration(configuration_shape=(true_weights.size,)),
            physical_model=physical_model,
        )
        engine.sample(
            total_number_iterations=10**3,
            random_walk_step_size=random_walk_step_size,
            record_step=10**3,
            temperature=1e-5,
        )
        samples = engine.sample(
            total_number_iterations=10**4,
            random_walk_step_size=random_walk_step_size,
            record_step=10**2,
            temperature=1.0,
        )
        return samples
