import torch
from torch import nn
from tqdm import trange


class LinearRegressorTrainer(nn.Module):
    def __init__(self, aleatoric_std):
        super().__init__()
        self.model = LinearRegressor(
            input_dim=1,
            output_dim=1,
        )
        self.aleatoric_std = aleatoric_std

    def compute_negative_log_likelihood(self, input_data, output_data):
        y_pred = self.model(input_data)
        loss = self.loss_func(y_pred, output_data)
        return loss

    def do_training(self, data_loader, number_of_epochs):
        optimizer = torch.optim.Adam(self.model.parameters())
        progress_bar = trange(number_of_epochs)
        self.model.train()
        for epoch in progress_bar:
            self.train()
            for data in data_loader:
                x_input, y_output = data
                optimizer.zero_grad()
                output = self.model(x_input)
                loss = self.loss_func(output, y_output)
                loss.backward()
                optimizer.step()
                progress_bar.set_postfix({"loss: ": loss})

    def predict(self, input_data):
        y_pred = self.model(input_data)
        return y_pred.detach().numpy().flatten()

    def compute_negative_log_likelihood_with_gradient(self, input_data, output_data, weight_decay: float):
        y_pred = self.model(input_data)
        loss = self.loss_func(y_pred, output_data)
        loss += weight_decay * sum(p.pow(2.0).sum() for p in self.model.parameters())
        return loss

    def loss_func(self, y_pred, output_data):
        mse_loss = nn.MSELoss(reduction="sum")
        return 0.5 * mse_loss(y_pred, output_data) / self.aleatoric_std**2


class LinearRegressor(nn.Module):
    def __init__(self, input_dim, output_dim):
        super().__init__()
        self.layers = nn.Sequential(nn.Linear(input_dim, output_dim))

    def forward(self, input_data):
        prediction = self.layers(input_data)
        return prediction
