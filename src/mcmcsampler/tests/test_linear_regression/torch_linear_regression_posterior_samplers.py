from mcmcsampler.lattices import NeuralNetworkLattice
from mcmcsampler.monte_carlo_samplers.torch_samplers import NeuralNetworkLangevinSampler, NeuralNetworkRandomWalkSampler
from mcmcsampler.multi_process_engine import MultiProcessEngine, SwapMultiProcessEngine
from mcmcsampler.physical_models.torch_models import NeuralNetworkModel
from mcmcsampler.tests.test_linear_regression.torch_linear_regressor import LinearRegressorTrainer


def compute_random_walk_samples(aleatoric_std, chemical_potential, data_loader):
    module = LinearRegressorTrainer(aleatoric_std=aleatoric_std)
    module.do_training(data_loader=data_loader, number_of_epochs=1000)
    physical_model = NeuralNetworkModel(data_loader=data_loader, module=module, weight_decay=chemical_potential)
    lattice = NeuralNetworkLattice()
    engine = NeuralNetworkRandomWalkSampler(
        configuration=physical_model.get_module_configuration_copy(), physical_model=physical_model, lattice=lattice
    )
    samples = engine.sample(record_step=10**2, total_number_iterations=10**4, random_walk_step_size=0.05)
    return samples, module


def compute_langevin_samples(aleatoric_std, chemical_potential, data_loader):
    random_walk_step_size = 1e-4
    module = LinearRegressorTrainer(aleatoric_std=aleatoric_std)
    module.do_training(data_loader=data_loader, number_of_epochs=1000)
    physical_model = NeuralNetworkModel(data_loader=data_loader, module=module, weight_decay=chemical_potential)
    lattice = NeuralNetworkLattice()
    engine = NeuralNetworkLangevinSampler(
        configuration=physical_model.get_module_configuration_copy(), physical_model=physical_model, lattice=lattice
    )
    samples = engine.sample(
        record_step=10**2, total_number_iterations=10**4, random_walk_step_size=random_walk_step_size
    )
    return samples, module


def compute_multiprocess_random_walk_samples(aleatoric_std, chemical_potential, data_loader):
    number_of_workers = 2
    module = LinearRegressorTrainer(aleatoric_std=aleatoric_std)
    module.do_training(data_loader=data_loader, number_of_epochs=1000)
    physical_model = NeuralNetworkModel(data_loader=data_loader, module=module, weight_decay=chemical_potential)
    lattice = NeuralNetworkLattice()
    engine = MultiProcessEngine(
        sampling_algorithm_class=NeuralNetworkRandomWalkSampler,
        initial_configurations=[physical_model.get_module_configuration_copy() for _ in range(number_of_workers)],
        physical_model=physical_model,
        lattice=lattice,
    )
    samples = engine.execute_method_multiprocess(
        record_step=10**2, total_number_iterations=10**4, random_walk_step_size=0.05
    )
    samples = engine.concatenate_multiprocess_outputs(samples)
    return samples, module


def compute_multiprocess_random_walk_samples_with_swap_memory(aleatoric_std, chemical_potential, data_loader):
    number_of_workers = 2
    module = LinearRegressorTrainer(aleatoric_std=aleatoric_std)
    module.do_training(data_loader=data_loader, number_of_epochs=1000)
    physical_model = NeuralNetworkModel(data_loader=data_loader, module=module, weight_decay=chemical_potential)
    lattice = NeuralNetworkLattice()
    engine = SwapMultiProcessEngine(
        sampling_algorithm_class=NeuralNetworkRandomWalkSampler,
        initial_configurations=[physical_model.get_module_configuration_copy() for _ in range(number_of_workers)],
        physical_model=physical_model,
        lattice=lattice,
    )
    samples = engine.execute_method_multiprocess(
        record_step=10**2, total_number_iterations=10**4, random_walk_step_size=0.05
    )
    samples = engine.concatenate_multiprocess_outputs(samples)
    return samples, module
