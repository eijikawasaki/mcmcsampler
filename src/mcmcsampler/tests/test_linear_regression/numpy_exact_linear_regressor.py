import numpy as np


class BayesianLinearRegressor:
    def __init__(self, n_features, weigth_decay, aleatoric_std):
        self.mean = np.zeros(n_features)
        self.cov_inv = 2.0 * weigth_decay * np.identity(n_features)
        self.aleatoric_std = aleatoric_std

    def learn(self, x_input, y_output):
        cov_inv = self.cov_inv + np.outer(x_input, x_input) / self.aleatoric_std**2
        cov = np.linalg.inv(cov_inv)
        mean = cov @ (self.cov_inv @ self.mean + y_output * x_input / self.aleatoric_std**2)
        self.cov_inv = cov_inv
        self.mean = mean
        return self

    def predict(self, x_input):
        y_pred_mean = x_input @ self.mean
        w_cov = np.linalg.inv(self.cov_inv)
        y_pred_var = self.aleatoric_std**2 + x_input @ w_cov @ x_input.T
        y_pred_std = np.sqrt(y_pred_var)
        return y_pred_mean, y_pred_std


def sample_data(number_of_samples, true_weights, aleatoric_std):
    for _ in range(number_of_samples):
        x_data = np.random.uniform(-1, 1)
        y_data = np.dot(true_weights, (1, x_data)) + np.random.normal(scale=aleatoric_std)
        yield x_data, y_data
