import numpy as np
import numpy_exact_linear_regressor
import pytest
import torch
from torch.utils.data import DataLoader, TensorDataset
from torch_linear_regression_posterior_samplers import (
    compute_langevin_samples,
    compute_multiprocess_random_walk_samples,
    compute_multiprocess_random_walk_samples_with_swap_memory,
    compute_random_walk_samples,
)

from mcmcsampler.multi_process_engine import MultiProcessEngine


class TestTorchLinearRegressionIntegration:
    @staticmethod
    @pytest.mark.parametrize(
        "sample",
        [
            compute_random_walk_samples,
            compute_langevin_samples,
            compute_multiprocess_random_walk_samples,
            compute_multiprocess_random_walk_samples_with_swap_memory,
        ],
    )
    def test_neural_mcmc_posterior_sampling_match_exact_solution(sample):
        MultiProcessEngine.set_seeds(seed=42)
        true_weights = np.asarray([-0.3, 0.5])
        aleatoric_std = 0.02
        number_of_samples = 3
        chemical_potential = 0.25

        data_samples = list(numpy_exact_linear_regressor.sample_data(number_of_samples, true_weights, aleatoric_std))

        _data_samples = np.asarray(data_samples).astype(np.float32)
        _data_samples = _data_samples.reshape((number_of_samples, 2, 1))
        train_input_data, train_output_data = torch.tensor(_data_samples[:, 0]), torch.tensor(_data_samples[:, 1])
        dataset = TensorDataset(train_input_data, train_output_data)
        data_loader = DataLoader(dataset, batch_size=len(train_input_data))

        # MCMC
        samples, module = sample(aleatoric_std, chemical_potential, data_loader)
        xs = np.linspace(-5.0, 5.0, 100).astype(np.float32)
        mus, stds = TestTorchLinearRegressionIntegration.compute_predictions(xs, aleatoric_std, samples, module)

        # Exact
        model = numpy_exact_linear_regressor.BayesianLinearRegressor(
            n_features=2, weigth_decay=chemical_potential, aleatoric_std=aleatoric_std
        )
        for xi, yi in data_samples:
            xi = np.asarray([1, xi])
            model.learn(xi, yi)
        full_xs = [np.array([1, x]) for x in xs]
        xs = np.asarray(full_xs)[:, 1]
        predictions = np.asarray([model.predict(np.array([1, xi])) for xi in xs])
        exact_mus, exact_stds = predictions[:, 0], predictions[:, 1]

        for expected_mu, empirical_mu in zip(exact_mus, mus):
            assert abs(expected_mu - empirical_mu) < 0.1

        for expected_std, empirical_std in zip(exact_stds, stds):
            assert abs(expected_std - empirical_std) / expected_std < 0.5

    @staticmethod
    def compute_predictions(full_xs, aleatoric_std, samples, module):
        mus = []
        stds = []
        for x in full_xs:
            mean_mu = 0.0
            y_samples = []
            for sample in samples:
                module.load_state_dict(sample)
                single_mean = module.predict(torch.from_numpy(x.reshape(1, 1)))
                mean_mu += single_mean
                y_samples.append(np.random.normal(single_mean, aleatoric_std, 1000))
            stds.append(np.std(y_samples))
            mean_mu /= len(samples)
            mus.append(mean_mu)
        mus = np.asarray(mus)
        stds = np.asarray(stds)
        mus = mus.flatten()
        return mus, stds
