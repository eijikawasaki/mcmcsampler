import numpy as np
import pytest
import torch
from torch.utils.data import DataLoader, TensorDataset

from mcmcsampler.lattices import SquareLattice
from mcmcsampler.monte_carlo_samplers.numpy_samplers import GlobalEnergySampler
from mcmcsampler.monte_carlo_samplers.torch_samplers import NeuralNetworkLangevinSampler
from mcmcsampler.multi_process_engine import MultiProcessEngine
from mcmcsampler.physical_models.numpy_models import Phi4Model
from mcmcsampler.physical_models.torch_models import NeuralNetworkModel
from mcmcsampler.tests.mlp import MLPTrainer


class TestPrint:
    @staticmethod
    def test_print():
        module = MLPTrainer(input_dim=1, hidden_dim=tuple(10 for _ in range(10)), output_dim=1)
        train_input_data = torch.zeros((10, 1))
        train_output_data = torch.zeros((10, 1))
        dataset = TensorDataset(train_input_data, train_output_data)
        loader = DataLoader(dataset)
        nn_model = NeuralNetworkModel(data_loader=loader, module=module)
        initial_configuration = nn_model.get_module_configuration_copy()
        engine = NeuralNetworkLangevinSampler(configuration=initial_configuration, physical_model=nn_model)
        print(str(engine))
        assert str(engine) == (
            '{"simulation": "NeuralNetworkLangevinSampler", "weight_decay": 0.1, '
            '"inverse_temperature": 1.0, '
            '"configuration_architecture": ["model.layers.0.0.weight", "model.layers.0.0.bias", '
            '"model.layers.1.0.weight", "model.layers.1.0.bias", "model.layers.2.0.weight", '
            '"model.layers.2.0.bias", "model.layers.3.0.weight", "model.layers.3.0.bias", '
            '"model.layers.4.0.weight", "model.layers.4.0.bias", "model.layers.5.0.weight", '
            '"model.layers.5.0.bias", "model.layers.6.0.weight", "model.layers.6.0.bias", '
            '"model.layers.7.0.weight", "model.layers.7.0.bias", "model.layers.8.0.weight", '
            '"model.layers.8.0.bias", "model.layers.9.0.weight", "model.layers.9.0.bias", '
            '"model.layers.10.weight", "model.layers.10.bias"]}'
        )

    @staticmethod
    @pytest.mark.parametrize(
        "physical_model, lattice, initial_configuration, sampling_algorithm_class, expected_output",
        [
            (
                Phi4Model(),
                SquareLattice(),
                np.zeros(0),
                GlobalEnergySampler,
                (
                    '{"simulation": "GlobalEnergySampler", "chemical_potential": 1.0, '
                    '"interaction_strength": 0.0, "space_dimensions": 2, "magnetic_configuration": 0.0, '
                    '"lattice": "SquareLattice", "configuration_shape": [0]}'
                ),
            ),
        ],
    )
    def test_print_2(
        physical_model,
        lattice,
        initial_configuration,
        sampling_algorithm_class,
        expected_output,
    ):
        engine = sampling_algorithm_class(
            physical_model=physical_model, lattice=lattice, configuration=initial_configuration
        )
        assert str(engine) == expected_output

    @staticmethod
    def test_print_multiprocess():
        phi4_model = Phi4Model(chemical_potential=-5.0, interaction_strength=1.0, space_dimensions=2)
        square_lattice = SquareLattice()
        number_of_workers = 4
        initial_configurations = [
            phi4_model.initialize_random_configuration(configuration_shape=(3,)) for _ in range(number_of_workers)
        ]
        multi_process_engine = MultiProcessEngine(
            physical_model=phi4_model,
            lattice=square_lattice,
            initial_configurations=initial_configurations,
            sampling_algorithm_class=GlobalEnergySampler,
        )
        assert (
            str(multi_process_engine) == '{"simulation": "GlobalEnergySampler",'
            ' "chemical_potential": -5.0, "interaction_strength": 1.0,'
            ' "space_dimensions": 2, "magnetic_configuration": 0.0,'
            ' "lattice": "SquareLattice", "configuration_shape": [3], "number_of_workers": 4}'
        )
