import math

import pytest

from mcmcsampler.lattices import SquareLattice
from mcmcsampler.observables import NumpyObservables
from mcmcsampler.physical_models.numpy_models import IsingModel


class TestIsingIntegration:
    @staticmethod
    @pytest.mark.parametrize(
        "configuration", [(IsingModel.initialize_random_configuration(configuration_shape=(int(1e6),)))]
    )
    def test_get_total_magnetization(configuration):
        assert round(NumpyObservables.compute_average_magnetization(configuration), ndigits=2) == 0.0

    @staticmethod
    def test_2d_total_magnetization():
        configuration = IsingModel.initialize_ones_configuration(configuration_shape=(100, 100))
        assert NumpyObservables.compute_average_magnetization(configuration) == 1.0

    @staticmethod
    @pytest.fixture
    def configuration_lattice_1d():
        configuration_shape = (10,)
        lattice = SquareLattice
        return IsingModel.initialize_random_configuration(configuration_shape=configuration_shape), lattice

    @staticmethod
    @pytest.fixture
    def lattice_2d():
        configuration_shape = (10, 10)
        lattice = SquareLattice
        return IsingModel.initialize_random_configuration(configuration_shape=configuration_shape), lattice

    @staticmethod
    def test_compute_1d_local_energy(configuration_lattice_1d: tuple):
        configuration, lattice = configuration_lattice_1d
        configuration_shape = configuration.shape
        i = 5
        local_configuration = configuration[i]
        local_energy = local_configuration * configuration[i + 1] + local_configuration * configuration[i - 1]
        local_energy *= -1.0
        coordinates = (i,)
        neighbors = lattice.compute_closest_neighbors(configuration_shape, coordinates)
        neighbors_configurations = NumpyObservables.get_neighbors_configurations(configuration, neighbors)
        local_configuration = NumpyObservables.get_local_configuration(configuration, coordinates)
        assert math.isclose(
            IsingModel.compute_local_energy(local_configuration, neighbors_configurations), local_energy
        )

    @staticmethod
    def test_compute_2d_local_energy(lattice_2d: tuple):
        configuration, lattice = lattice_2d
        configuration_shape = configuration.shape
        i, j = 5, 5
        local_configuration = configuration[i][j]
        local_energy = (
            local_configuration * configuration[i + 1][j]
            + local_configuration * configuration[i][j + 1]
            + local_configuration * configuration[i - 1][j]
            + local_configuration * configuration[i][j - 1]
        )
        local_energy *= -1.0
        coordinates = (i, j)
        neighbors = lattice.compute_closest_neighbors(configuration_shape, coordinates)
        neighbors_configurations = NumpyObservables.get_neighbors_configurations(configuration, neighbors)
        local_configuration = NumpyObservables.get_local_configuration(configuration, coordinates)
        assert math.isclose(
            IsingModel.compute_local_energy(local_configuration, neighbors_configurations), local_energy
        )

    @staticmethod
    def test_compute_2d_pbc_local_energy(lattice_2d):
        configuration, lattice = lattice_2d
        configuration_shape = configuration.shape
        i, j = 9, 9
        local_configuration = configuration[i][j]
        local_energy = (
            local_configuration * configuration[9][0]
            + local_configuration * configuration[9][8]
            + local_configuration * configuration[0][9]
            + local_configuration * configuration[8][9]
        )
        local_energy *= -1.0
        coordinates = (i, j)
        neighbors = lattice.compute_closest_neighbors(configuration_shape, coordinates)
        neighbors_configurations = NumpyObservables.get_neighbors_configurations(configuration, neighbors)
        local_configuration = NumpyObservables.get_local_configuration(configuration, coordinates)
        assert math.isclose(
            IsingModel.compute_local_energy(local_configuration, neighbors_configurations), local_energy
        )

    @staticmethod
    def test_compute_total_energy(lattice_2d):
        configuration, lattice = lattice_2d
        configuration_shape = configuration.shape
        (L, M) = configuration_shape
        expected_energy = 0.0
        for i in range(L):
            for j in range(M):
                local_configuration = configuration[i][j]
                (x, y) = lattice.apply_periodic_boundary_conditions(configuration_shape, (i + 1, j))
                local_energy = local_configuration * configuration[x][y]
                expected_energy += local_energy
                (x, y) = lattice.apply_periodic_boundary_conditions(configuration_shape, (i, j + 1))
                local_energy = local_configuration * configuration[x][y]
                expected_energy += local_energy
        expected_energy *= -1.0
        total_energy = NumpyObservables.compute_total_energy(
            SquareLattice(), configuration, IsingModel.compute_local_energy
        )
        assert math.isclose(total_energy, expected_energy)
