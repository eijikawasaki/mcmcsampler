import torch
from torch import nn


class MLPTrainer(nn.Module):
    def __init__(self, input_dim, hidden_dim, output_dim):
        super().__init__()
        self.loss_func = nn.MSELoss()
        self.model = MLP(
            input_dim=input_dim,
            hidden_layers_dimensions=hidden_dim,
            output_dim=output_dim,
        )

    def do_training(self, input_data, output_data, epochs):
        optimizer = torch.optim.Adam(self.model.parameters())
        self.model.train()
        for epoch in range(epochs):
            optimizer.zero_grad()
            y_pred = self.model(input_data)
            loss = self.loss_func(y_pred, output_data)
            loss.backward()
            optimizer.step()
        self.model.eval()

    def forward(self, input_data):
        return self.model(input_data)

    def predict(self, input_data):
        y_pred = self.model(input_data)
        return y_pred.detach().numpy().flatten()

    def compute_negative_log_likelihood(self, input_data, output_data, weight_decay: float = 0.0):
        y_pred = self.model(input_data)
        loss = self.loss_func(y_pred, output_data)
        loss += weight_decay * sum(p.pow(2.0).sum() for p in self.model.parameters())
        return loss


class MLP(nn.Module):
    def __init__(self, input_dim, hidden_layers_dimensions, output_dim, is_bias=True):
        super().__init__()
        layers = [MLP.create_sequential_layer(input_dim, hidden_layers_dimensions[0], is_bias)]
        for idx in range(len(hidden_layers_dimensions) - 1):
            layers.append(
                MLP.create_sequential_layer(
                    hidden_layers_dimensions[idx],
                    hidden_layers_dimensions[idx + 1],
                    is_bias,
                )
            )
        layers.append(nn.Linear(hidden_layers_dimensions[-1], output_dim, bias=is_bias))

        self.layers = nn.Sequential(*layers)

    def forward(self, input_data):
        prediction = self.layers(input_data)
        return prediction

    @staticmethod
    def create_sequential_layer(
        current_layer_dimensions: int, next_layer_dimensions: int, is_bias: bool
    ) -> nn.Sequential:
        return nn.Sequential(
            nn.Linear(current_layer_dimensions, next_layer_dimensions, bias=is_bias),
            nn.LeakyReLU(),
        )
