# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres
to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.0] - 11-01-2024

### Fixed

- ```MultiProcessEngine.execute_method_multiprocess``` now works
  with ```pytorch``` neural networks models as well

### Added

- ```TorchSurrogateModel```
- ```NeuralNetworkMiniBatchModel```
- ```NeuralNetworkPenaltySampler```

### Changed

- torch ```configuration``` is now a ```dict``` from ```state_dict``` instead of
  a ```torch.nn.Module```

## [0.0.3] - 19/06/2023

### Added

- ```compute_random_coordinate``` method added to both ```SquareLattice```
  and ```NeuralNetworkLattice```

### Changed

- ```sizes``` renamed into ```configuration_size```
- ```_update_single_step``` renamed into ```_sample_single_step```
- class diagram added to the documentation

## [0.0.2] - 22-03-2023

### Fixed

- Documentation publication using GitLab pages

### Changed

- ```NeuralNetworkLangevinSampler``` refactor (gradient computation update)
